//
//  AppContent.m
//  truck
//
//  Created by 洪湃 on 16/11/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "AppContent.h"

@implementation AppContent


-(void)initView{
    [self setBackgroundColor:CGray];
    
    NoticeAdd(self, @selector(noticeSelectDownMenu:), KNoticeName_MapSelectPlace, nil);
    
    appDownMenu = [AppDownMenu instanceByFrame:R(0, App_Size.height-SS(AppButtomHeight), App_Size.width,SS(AppButtomHeight))];
    [self addSubview:appDownMenu];
    
    
}

#pragma mark -- 点击导航栏
-(void)noticeSelectDownMenu:(NSNotification *)notice{
    int index = [[notice object][@"selectindex"] intValue];
    if(lastclick == index){
        return;
    }
    
    [lastView removeFromSuperview];
    
    CGRect framecontent = R(0, 0, App_Size.width, App_Size.height-SS(AppButtomHeight));
    if (index==1) {//显示推荐
        if (userTask==nil) {
            userTask = [UserTask instanceByFrame:framecontent];
        }
        lastView = userTask;
    }else if(index==2){
        if (userMessage==nil) {
            userMessage = [UserMessage instanceByFrame:framecontent];
        }
        lastView = userMessage;
    }else if(index==3){
        if (userCenter==nil) {
            userCenter = [UserCenter instanceByFrame:framecontent];
        }
        lastView = userCenter;
    }
    [self addSubview:lastView];
    
    lastclick = index;
}

-(void)dealloc{
    track();
}

@end
