//
//  EEButtonGroup.h
//  Sonialvision
//
//  Created by 洪湃 on 14-8-6.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EEButtonGroupDelegate <NSObject>

-(void)EEButtonGroup_SelectIndex:(int)index group:(id)btnGroup;

@end



@interface EEButtonGroup : UIView{
}

@property(nonatomic,weak)id pdelegate;
@property(nonatomic,strong)NSArray *sourceArray;
@property(nonatomic,strong)UITapGestureRecognizer *tapGesture;
@property(nonatomic)BOOL isTransClick;


-(void)tap:(UIGestureRecognizer *)gesture;

- (id)initWithSourceArray:(NSArray *)listArray positionArray:(NSArray *)posArray normalColor:(UIColor *)nColor selectColor:(UIColor *)sColor fontSize:(int)size isTransparentArea:(BOOL)isTrans;

-(void)setSelectIndex:(int)index;



-(void)onlySelectImage:(int)index;

@end


@interface ImageButton : UIImageView

@property(nonatomic,strong)NSString *normalImageName;
@property(nonatomic,strong)NSString *selectImageName;
@property(nonatomic,weak)UILabel *titleLabel;

@property(nonatomic,strong)UIColor *normalColor;
@property(nonatomic,strong)UIColor *selectColor;

@property(nonatomic)BOOL isSelected;


-(void)setButtonSelect:(BOOL)select;








@end