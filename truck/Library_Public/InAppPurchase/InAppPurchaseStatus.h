//
//  InAppPurchaseStatus.h
//  iDaily
//
//  Created by zhe on 11/04/03.
//  Copyright 2011 Rakuraku Technologies, Inc. All rights reserved.
//

#define kStatusOK                                 0
#define kStatusNG                                 999999
#define kStatusAPIServerUnavailable               999991
#define kStatusIncorrectJSON                      21000
#define kStatusMalformedReceipt                   21002
#define kStatusCanNotAuthenticateReceipt          21003
#define kStatusWrongSharedSecret                  21004
#define kStatusReceiptServerUnavailable           21005
#define kStatusSubscriptionExpired                21006

#define kServerBadRequestErrorCode                400
#define kServerRuntimeErrorCode                   500

#define kIAPErrorDomain                           @"IAPErrorDomain"
