//
//  ApData.h
//  OuRuiDA
//
//  Created by 洪湃 on 15/12/17.
//  Copyright © 2015年 洪湃 454077256. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    AppProductType_xuetang_aishuke = 0,
    AppProductType_xuetang_pulagte,
    AppProductType_xuetang_XTY_beijia,
    AppProductType_xuetang_XTY_outjie,
    AppProductType_xuetang_YDS_zhentou,
    AppProductType_xuetang_YDS_zhusheqi
}AppProductType;


typedef enum{
    UserInfor_udid,
    UserInfor_username,
}UserInfor;


typedef enum{
    TaskState_doing,
    TaskState_compelte,
    TaskState_waiting,
    TaskState_unknown
}TaskState;


@interface AppData : NSObject{
    
}

@property(nonatomic)AppProductType appproductType;

+(AppData * )shareInstance;
+(NSMutableDictionary *)eeData;
+(void)save;

+(void)setData:(NSString *)topKey key:(NSString *)keyString value:(id)valueString;
+(id)getData:(NSString *)topKey key:(NSString *)keyString;



+(Boolean)checkLogin;
+(NSString *)getUser:(UserInfor)tag;
+(NSDictionary *)getUserInfor;

+(TaskState)taskState:(NSDictionary *)data;
@end
