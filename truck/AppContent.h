//
//  AppContent.h
//  truck
//
//  Created by 洪湃 on 16/11/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"
#import "AppDownMenu.h"
#import "UserTask.h"
#import "UserCenter.h"
#import "UserMessage.h"

@interface AppContent : EEView{
    AppDownMenu *appDownMenu;
    
    UserTask *userTask;
    UserCenter *userCenter;
    UserMessage *userMessage;
    
    EEView *lastView;
    int lastclick;

}

@end
