//
//  UnitCpu.h
//  Lohas
//
//  Created by 洪湃 on 15-3-24.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

typedef void(^UnitComplete) (id postResult);
typedef void(^UnitFailed) (id postResult);
typedef void(^UnitBegin) ();

@interface UnitCpu : NSObject

@property(nonatomic,strong)id delegate;

@property(nonatomic,strong)NSDictionary *configData;
@property(nonatomic,strong)NSDictionary *postResultData;
@property(nonatomic ,strong)NSString *uIndex;


- (void)start:(NSDictionary *)pConfigData  begin:(UnitBegin)blockB complete:(UnitComplete)blockC failed:(UnitFailed)blockD;

- (void)doComplete:(id)data;
- (void)doFailed:(id)data;
-(void)disponse;

@end
