//
//  EEImageBrowser.h
//  a0minicomp
//
//  Created by 洪湃 on 16/5/18.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface EEImageBrowser : EEView<UIScrollViewDelegate,EGOImageViewDelegate>{
    EGOImageView *contentImageView;
    UITapGestureRecognizer *reloadTapGesture;

    UIScrollView *scrollView;
}

@end
