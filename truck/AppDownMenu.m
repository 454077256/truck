
//
//  AppMenu.m
//  NEJM
//
//  Created by 洪湃 on 15/11/13.
//  Copyright © 2015年 洪湃 454077256. All rights reserved.
//

#import "AppDownMenu.h"

@implementation AppDownMenu

+(id)instanceByFrame:(CGRect)frame{
    AppDownMenu *instance = [[AppDownMenu alloc] initWithFrame:frame];
//    [instance setBackgroundColor:CRed];
    [instance initView];

    return instance;
}

-(void)initView{
    
//    [self addSubview:[Global createViewByFrame:R(0,0,self.width,0.5) color:CHex(0xA5A5A4)]];
    
//    float array = [0.1,0.3,0.5,0.7,0.9];
//    UIImage *bgimg = [Global loadImg:@"bg_menubar.png"];
//    [self setBackgroundColor:[UIColor colorWithPatternImage:bgimg]];
    
    [self setBackgroundColor:CWhite];
    
    [self addSubview:[Global createViewByFrame:R(0, 0, self.width, S(0.5)) color:CHex(0xEFEFEF)]];
    //float startBili = 0.125;
    for (int i=1; i<=3; i++) {
        NSString *normalName = [NSString stringWithFormat:@"assets/%ds.png",i];
        NSString *selectName = [NSString stringWithFormat:@"assets/%dh.png",i];
        
        /*
        float bli = startBili;
        if (i==1) {
            bli = bli;
            
        }else if(i==2){
            bli = (1-bli-bli)/3 + bli;
        }else if(i==3){
            bli = (1-bli-bli)*2/3 + bli;
        }else if(i==4){
            bli = 1-bli;
        }*/
        float bli  =0.25*i;
        
        [self addSubview:tmpButtom = [Global createBtn_center:ccp(bli*App_Size.width, self.height*0.5) name:normalName down:selectName target:self action:@selector(clickBtn:)]];
        tmpButtom.tag = i;
        
        if (i==1) {
            bli = 0.15;
            curSelectBtn = tmpButtom;
        }
        /*
        EEButton *labeBtn = [Global createBtnColor:R(0, 0, SS(108), SS(130)) bgcolor:CClear title:labs[i-1] titleNormal:CBlack titleSelect:CBlack size:S(10) delegate:self action:@selector(clickTextBtn:)];
        labeBtn.tag = 100+i;
        [self addSubview:labeBtn];
        
        labeBtn.center = ccp(tmpButtom.center.x, self.height*0.5);*/
        
    }
    
    [self clickBtn:curSelectBtn];
    
}

-(void)clickBtn:(EEButton *)sender{
    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        EEButton *btn = (EEButton *)obj;
        if ([btn isKindOfClass:[EEButton class]]) {
            [btn setSelected:NO];
        }
    }];
    
    NSInteger index = sender.tag;
    
    NoticePost(KNoticeName_MapSelectPlace, @{@"selectindex":[NSNumber numberWithInteger:index]});
    
    [sender setSelected:YES];
}

-(void)clickTextBtn:(EEButton *)sender{
    NSInteger tagClick = sender.tag-100;
    EEButton *btn = [self viewWithTag:tagClick];
    [self clickBtn:btn];
    
}

-(void)noticeSelectIndex:(NSNotification *)notice{
    int index = [notice.object intValue];
    EEButton *btn = [self viewWithTag:index];
    [self clickBtn:btn];
}


@end
