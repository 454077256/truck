//
//  LocationCell.m
//  Lohas
//
//  Created by 洪湃 on 16/4/16.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "LocationCell.h"

@implementation LocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refreshTitle:(NSString *)_title detail:(NSString *)detail{
    [labelTitle removeFromSuperview];
    [labelDetail removeFromSuperview];
    
    if (detail==nil ||detail.length<=0) {
        
        labelTitle = [Global createLabel:R(SS(32), 0, App_Size.width-SS(40), SS(100)) label:_title lines:0 fontSize:S(16) fontName:nil textcolor:CBlack align:(NSTextAlignmentLeft)];
        [self addSubview:labelTitle];
        
    }else{
        
        labelTitle = [Global createLabel:R(SS(32), SS(12), App_Size.width-SS(40), SS(46)) label:_title lines:0 fontSize:S(16) fontName:nil textcolor:CBlack align:(NSTextAlignmentLeft)];
        [self addSubview:labelTitle];
        
        labelDetail = [Global createLabel:R(SS(32), SS(58), App_Size.width-SS(40), SS(35)) label:detail lines:0 fontSize:S(12) fontName:nil textcolor:CGray align:(NSTextAlignmentLeft)];
        [self addSubview:labelDetail];
    }
    
}

@end
