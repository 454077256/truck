//
//  RootControllerPhone.m
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃. All rights reserved.
//

#import "RootControllerPhone.h"


static RootControllerPhone *_rootViewController = NULL;

@interface RootControllerPhone ()

@end

@implementation RootControllerPhone


+ (RootControllerPhone*) sharedRootViewController
{
    if (!_rootViewController) {
        _rootViewController = [[RootControllerPhone alloc] init];
        [Global sharedGlobal].rootController = _rootViewController;
    }
    return _rootViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // 检测是否已经登录
//    if ([AppData checkLogin]) {
//        appContent = [AppUIFactory open:@"AppContent" frame:frameUI data:nil parent:RootView action:(ActionType_none)];
//    }else{
        [AppUIFactory open:@"AppLogin" frame:frameUI data:nil parent:RootView action:(ActionType_none)];
//    }
    
    [Global statusBarShow:YES];
    
    NoticeAdd(self, @selector(notice_userloginsuccess:), kNotice_userloginsuccess, nil);
    NoticeAdd(self, @selector(notice_userloginOut:), knotice_userLoginOut, nil);
}

-(void)notice_userloginsuccess:(NSNotification *)notice{
    if(appContent==nil){
        appContent = [AppUIFactory open:@"AppContent" frame:frameUI data:nil parent:RootView action:(ActionType_none)];
    }
}


-(void)notice_userloginOut:(NSNotification *)notice{
    [AppUIFactory open:@"AppLogin" frame:frameUI data:nil parent:RootView action:(ActionType_none)];

    [appContent removeFromSuperview];
    appContent = nil;
}




@end
