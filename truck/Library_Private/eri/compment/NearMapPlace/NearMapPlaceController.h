//
//  NearMapPlaceController.h
//  Lohas
//
//  Created by 洪湃 on 16/4/16.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>


@interface NearMapPlaceController : UIViewController<MAMapViewDelegate,AMapSearchDelegate,UITableViewDataSource,UITableViewDelegate>{
    MAMapView *_mapView;
    CLLocationCoordinate2D lastCoordinate2d;
    MAPointAnnotation *pointAnnotation;
    AMapSearchAPI *_search;
    
    BOOL isUserMoveMap;
}


@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSArray *dataSource;
@property(nonatomic,assign)NSInteger currentSelectLocationIndex;
@end
