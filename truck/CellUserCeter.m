//
//  CellUserCeter.m
//  carDcom
//
//  Created by 洪湃 on 16/11/22.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "CellUserCeter.h"

@implementation CellUserCeter

-(void)loadData:(NSDictionary *)config size:(CGSize)size indexPath:(NSIndexPath *)indexpath{
    self.eeData = config;
    cellSize = size;
    
    NSString *icoName = @"";
    NSInteger row = indexpath.row;
    NSInteger section = indexpath.section;
    
    /*
    if(section==0 && row==0){
        icoName = @"assets/userico1.png";
    }else if(section==0 && row==1){
        icoName = @"assets/userico2.png";
    }else if(section==1 && row==0){
        icoName = @"assets/userico3.png";
    }else if(section==1 && row==1){
        icoName = @"assets/userico4.png";
    }else if(section==2 && row==0){
        icoName = @"assets/userico5.png";
    }
    
    [self addSubview:[Global createImage:icoName center:ccp(SS(44), size.height*0.5)]];*/
    
    lab_title = [Global createLabel:R(SS(48), 0, SS(200), size.height) label:config[@"title"] lines:0 fontSize:S(12) fontName:nil textcolor:CBlack align:(NSTextAlignmentLeft)];
    [self addSubview:lab_title] ;
    if(indexpath.section==0 && indexpath.row ==0){
        [self setUserInformation];
    }
    
}


-(void)setUserInformation{
    
    UIImageView *userimg = [Global createCircleImage:@"assets/useravatar.png" url:nil size:CGSizeMake(SS(88), SS(88)) center:ccp(SS(86), cellSize.height*0.5)];
    [self addSubview:userimg];
    
    [lab_title removeFromSuperview];
    
    EELabel *userName = [Global createLabelFit:CGSizeMake(100, SS(36)) point:ccp(SS(140), SS(32)) label:@"杨宗纬" lines:0 fontSize:S(16) fontName:nil textcolor:CBlack align:(NSTextAlignmentLeft) resultSize:nil];
    [self addSubview:userName];
    
    EELabel *userLevel = [Global createLabel:R(userName.x+userName.width+SS(6), userName.y, SS(100), SS(40)) label:@"LV6" lines:0 fontSize:S(13) fontName:nil textcolor:CWhite align:(NSTextAlignmentCenter)];
    [Global addBorder:userLevel borderWidth:1 color:CHex(0xFC9827) radius:S(6)];
    [userLevel setBackgroundColor:CHex(0xFC9827)];
    [self addSubview:userLevel];
    userLevel.clipsToBounds = YES;
    
    EELabel *userPhone = [Global createLabel:R(userName.x, SS(78), SS(200), SS(40)) label:@"13899878223" lines:0 fontSize:S(14) fontName:nil textcolor:CHex(0x444444) align:(NSTextAlignmentLeft)];
    [self addSubview:userPhone];
}


-(void)dealloc{
}

@end
