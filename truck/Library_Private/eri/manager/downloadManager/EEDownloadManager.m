//
//  EEDownloadManager.m
//  OuRuiDA
//
//  Created by 洪湃 on 16/6/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "EEDownloadManager.h"

static int maxDownloadThread = 2;

static EEDownloadManager *instance;

@implementation EEDownloadManager

#define EventArrays ([EEDownloadManager shareInstance].eventsArray)


+(EEDownloadManager*) shareInstance
{
    if (!instance) {
        instance = [[EEDownloadManager alloc] init];
        instance.eventsArray = [NSMutableArray array];
    }
    return instance;
}

/*
 * weburl:下载的完整网络路径
 * savepath:保存本地的完整路径
 * data:设置到EEOneEvent里面的数据
 * isToFirst:是否优先下载，放置到下载队列的第一个，会暂停另外一个正在下载的进程
 **/
+(void)addOneDownloadTask_webpath:(NSString *)webFilePath savePath:(NSString *)savePath delegate:(id)delegate data:(NSDictionary *)data isToFirst:(BOOL)isToFirst progressView:(LDProgressView *)progress{
    
    EEOneEvent *oneEvent = [EEDownloadManager getOneEventByWebURL:webFilePath];
    
    if(oneEvent==nil){
        oneEvent = [EEOneEvent instanceWithWebFilePath:webFilePath savePath:savePath configData:data progresView:progress];
        oneEvent.hashCode = [webFilePath hash];
    }else{
        [EventArrays removeObject:oneEvent];
    }
    
    if(isToFirst){
        [EventArrays insertObject:oneEvent atIndex:0];
    }else{
        [EventArrays addObject:oneEvent];
    }
    
    oneEvent.progressView = progress;//这个有可能在外部移除
    
    oneEvent.delegate = delegate;
    
    [EEDownloadManager printInfor];
    
    [self startQueue];
    //
}

+(void)startQueue{
    //启动队列
    for (int i=0; i<[[EEDownloadManager shareInstance].eventsArray count]; i++) {
        EEOneEvent *curEvent = EventArrays[i];
        if (i<maxDownloadThread) {
            [curEvent start];
        }else{
            [curEvent stop];
        }
    }
}


+(void)removeOneDownloadTaskByWebURL:(NSString *)webURL{
    
    NSUInteger webURLhashCode = [webURL hash];
    
    [[EEDownloadManager shareInstance].eventsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        EEOneEvent *oneEvent = (EEOneEvent *)obj;
        if (oneEvent.hashCode == webURLhashCode) {
            
            [oneEvent disponse];
            [[EEDownloadManager shareInstance].eventsArray removeObject:oneEvent];
            
            *stop = YES;
        }
    }];

}

+(void)removeOneDownloadTaskByOneEvent:(EEOneEvent *)oneEvent{
    //处理销毁逻辑
    [oneEvent disponse];
    
    //删除
    [[EEDownloadManager shareInstance].eventsArray removeObject:oneEvent];
}



+(Boolean)checkTaskIsExist:(NSString *)webURL {
    NSUInteger webURLhashCode = [webURL hash];
    
    __block Boolean isExistInTaskList = NO;
    [[EEDownloadManager shareInstance].eventsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        EEOneEvent *oneEvent = (EEOneEvent *)obj;
        if (oneEvent.hashCode == webURLhashCode) {
            trace(@"hashcode:%lu 该任务已经在下载队列中，不需要重新添加",(unsigned long)webURLhashCode);
            isExistInTaskList = YES;
            
            *stop = YES;
        }
    }];
    
    return isExistInTaskList;
}


+(EEOneEvent *)getOneEventByWebURL:(NSString *)webURL {
    NSUInteger webURLhashCode = [webURL hash];
    for (int i=0; i<[[EEDownloadManager shareInstance].eventsArray count]; i++) {
        EEOneEvent *oneEvent = [EEDownloadManager shareInstance].eventsArray[i];
        if (oneEvent.hashCode == webURLhashCode) {
            return oneEvent;
        }
    }
    return nil;
}


+(void)printInfor{
    trace(@"队列还剩下:个%dtask",[EventArrays count]);
}






@end
