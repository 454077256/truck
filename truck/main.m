    //
//  main.m
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
