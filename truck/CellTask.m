//
//  CellTask.m
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "CellTask.h"

@implementation CellTask

-(void)loadData:(NSDictionary *)config size:(CGSize)size indexPath:(NSIndexPath *)indexpath{
    self.eeData = config;
    [self setBackgroundColor:CClear];
    
    [self addSubview:[Global createImage:@"assets/cellbg.png" point:ccp(SS(14), 0)]];
    
    UIColor *tColor = CHex(0x565656);
    
    [self addGreenBox:@"装货" position:ccp(SS(28), SS(64))];
    
    //状态
    TaskState state = [AppData taskState:self.eeData];
    NSString *filename = @"";
    if(state==TaskState_waiting){
        filename = @"assets/processwill.png";
    }else if(state==TaskState_doing){
        filename = @"assets/processing.png";
    }else if(state==TaskState_compelte){
        filename = @"assets/processok.png";
    }
    [self addSubview:[Global createImage:filename center:ccp(SS(566), SS(208))]];

    
    //装货时间
    NSArray *workOrderItems= self.eeData[@"workOrderItems"];
    
    if ([workOrderItems count]>0) {
        NSString *loadTime = NotNull(workOrderItems[0][@"workOrder"][@"returnDate"]) ;
        if (loadTime.length>5) {
            loadTime = [loadTime substringFromIndex:5];
        }
        
        NSString *titleZhuanghuo = [NSString stringWithFormat:@"装货时间:%@",loadTime];
        [self addSubview:[Global createLabel:R(SS(28), SS(18), SS(570), SS(34)) label:titleZhuanghuo lines:0 fontSize:S(13) fontName:nil textcolor:tColor align:(NSTextAlignmentLeft)]];
    }
    
    //箱号
    [self addSubview:[Global createLabel:R(SS(310), SS(18), SS(56), SS(34)) label:@"箱号:" lines:0 fontSize:S(13) fontName:nil textcolor:tColor align:(NSTextAlignmentLeft)]];
    
    NSString *targetCode = @"";
    for (int i=0; i<[workOrderItems count]; i++) {
        if(i>0){
            targetCode = [NSString stringWithFormat:@"%@\n",targetCode];
        }
        NSDictionary *item = workOrderItems[0];
        NSString *cntrNum       = NotNull(item[@"cntrNum"]);
        NSString *checkDigit    = NotNull(item[@"checkDigit"]);
        NSString *cntrType      = NotNull(item[@"cntrType"]);
        if(cntrType.length>0){
            cntrType = [NSString stringWithFormat:@"/%@",cntrType];
        }
        targetCode = [NSString stringWithFormat:@"%@%@%@%@",targetCode,cntrNum,checkDigit,cntrType];
    }
    
    EELabel *labXianghao =  [Global createLabelFit:CGSizeMake(SS(264), 0) point:ccp(SS(370),0) label:targetCode lines:0 fontSize:S(12) fontName:nil textcolor:tColor align:(NSTextAlignmentLeft) resultSize:nil];
    labXianghao.layer.anchorPoint = ccp(0, 0.5);
    labXianghao.center = ccp(SS(370), SS(36));
    
    [self addSubview:labXianghao];
    
    //装货
    NSString *zHcontent = @"";
    if([workOrderItems count]>0){
        NSDictionary *item = workOrderItems[0];
        NSString *doorProvince  = NotNull(item[@"workOrder"][@"doorProvince"]);
        NSString *doorCity      = NotNull(item[@"workOrder"][@"doorCity"]);
        NSString *doorCounty    = NotNull(item[@"workOrder"][@"doorCounty"]);
        NSString *doorTown      = NotNull(item[@"workOrder"][@"doorTown"]);
        NSString *doorAddress   = NotNull(item[@"workOrder"][@"doorAddress"]);
        
        zHcontent = [NSString stringWithFormat:@"%@%@%@%@%@",doorProvince,doorCity,doorCounty,doorTown,doorAddress];
    }
    
    EELabel *labezhuanghuo = [Global createLabelFit:CGSizeMake(SS(520), 0) point:ccp(SS(100), SS(70)) label:zHcontent lines:0 fontSize:S(13) fontName:nil textcolor:tColor align:(NSTextAlignmentLeft) resultSize:nil];
    [self addSubview:labezhuanghuo];
    
    
    //港口
    
    [self addGreenBox:@"港口" position:ccp(SS(28), nextY(labezhuanghuo, SS(16)))];
    NSString *Gkcontent = @"";
    if([workOrderItems count]>0){
        NSDictionary *item = workOrderItems[0];
        NSString *obIb  = NotNull(item[@"workOrder"][@"obIb"]);
        
        if([[obIb lowercaseString] isEqualToString:@"i"]){
            NSString *obIb  = NotNull(item[@"workOrder"][@"podNameZh"]);
            Gkcontent = [NSString stringWithFormat:@"<span style='color:#565656;font-size:1.5em;'>%@ &nbsp;&nbsp;<span style='background-color:#4982CE;color:white;font-size:1.0em;'>%@</span></span>",obIb,@"进口"];
        }else if([[obIb lowercaseString] isEqualToString:@"o"]){
            NSString *obIb  = NotNull(item[@"workOrder"][@"polNameZh"]);
            Gkcontent = [NSString stringWithFormat:@"<span style='color:#565656;font-size:1.5em;'>%@ &nbsp;&nbsp;<span style='background-color:#4982CE;color:white;font-size:1.0em;'>%@</span></span>",obIb,@"出口"];
        }
        
    }

    EELabel *labGangkou = [Global createLabelFitHTML:CGSizeMake(SS(280), 0) point:ccp(SS(100), nextY(labezhuanghuo, SS(20))) label:Gkcontent numLine:0 resultSize:nil];
    [self addSubview:labGangkou];
    
    //obIb==I podNameZh = Qinzhou;//进口
    //obIb==O polNameZh = "\U53a6\U95e8";//出口
    
    //truckFleetName  licencePlate
    //车队
    [self addGreenBox:@"车队" position:ccp(SS(28), nextY(labGangkou, SS(22)))];
    NSString *cDcontent = @"";
    if([workOrderItems count]>0){
        NSDictionary *item = workOrderItems[0];
        NSString *truckFleetName = NotNull( item[@"workOrder"][@"truckFleetName"]);
        NSString *licencePlate = NotNull( item[@"licencePlate"]);
        if(licencePlate.length>1){
            licencePlate = [NSString stringWithFormat:@"(%@)",licencePlate];
        }
        
        cDcontent = [NSString stringWithFormat:@"%@%@",truckFleetName,licencePlate];
    }
    
    [self addSubview:[Global createLabelFit:CGSizeMake(SS(520), 0) point:ccp(SS(100), nextY(labGangkou, SS(30))) label:cDcontent lines:0 fontSize:S(13) fontName:nil textcolor:tColor align:(NSTextAlignmentLeft) resultSize:nil]] ;

    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


-(void)addGreenBox:(NSString *)title position:(CGPoint)point{
    EELabel *labelText = [Global createLabel:R(point.x, point.y, SS(68), SS(42)) label:title lines:0 fontSize:S(13) fontName:nil textcolor:CWhite align:(NSTextAlignmentCenter)];
    [labelText setBackgroundColor:CHex(0x28985E)];
    [Global addBorder:labelText borderWidth:0.1 color:CHex(0x28985E) radius:S(4)];
    labelText.clipsToBounds = YES;
    [self addSubview:labelText];
}



@end
