//
//  InAppStore.h
//  OneTap
//
//  Created by xuzhe on 09/12/11.
//  Copyright 2009 Rakuraku Technologies, Inc.. All rights reserved.
//

/* how to use this class:
 //////////////////////////////////////////////////////
 ------------------------------------------------------
 * When checking purchased product status *
 ------------------------------------------------------
 NSInteger status = [InAppStoreServerConnector checkProductStatus:kProductIdentifier expireDate:&expireDate];
 if (status == kStatusOK) {
    //You have already purchased this item. And no need to Renew it.
 } else if (status == kProductStatusNGOutOfDate) {
    //The item you purchased is out of date. Renew it now?
 } else if (status == kProductStatusNGNotBought) {
    //You never purchased this item on this device. You need to purchase it again.
 } else {
    //The server is not reachable or in trouble, please try again later.
 }
 //////////////////////////////////////////////////////
 
 //////////////////////////////////////////////////////
 ------------------------------------------------------
 * Check if this device can make Payments *
 ------------------------------------------------------
 if ([<#Inited InAppStore> canMakePayments]) {
    // can make payments
 } else {
    // can NOT make payments
 }
 //////////////////////////////////////////////////////
 
 //////////////////////////////////////////////////////
 ------------------------------------------------------
 * How to purchase a product *
 ------------------------------------------------------
 [<#Inited InAppStore> addPayment:kProductIdentifier];
 
 ------------------------------------------------------
 And wait for the methods below being called:
 ------------------------------------------------------
 - (BOOL)provideContent:(NSString *)productIdentifier expireDate:(NSDate *)expireDate;  // succeed
 - (BOOL)provideRestoredContent:(NSString *)productIdentifier expireDate:(NSDate *)expireDate;  // succeed, if there is no provideRestoredContent, we just invoke provideContent instead
 - (void)paymentFailed:(NSString *)productIdentifier;   // failed
 - (void)paymentCancelled:(NSString *)productIdentifier;    // cancelled
 //////////////////////////////////////////////////////
 
 //////////////////////////////////////////////////////
 ------------------------------------------------------
 * How to get product Infomations (such as name, price, etc.) *
 ------------------------------------------------------
 [<#Inited InAppStore> requestProductsData:<#Product Indentifier Set>];
 
 ------------------------------------------------------
 And wait for the method below being called:
 ------------------------------------------------------
 - (void)showProductInfo:(NSArray *)products invalidProductIdentifiers:(NSArray *)invalidProductIdentifiers;
 //////////////////////////////////////////////////////
*/

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "InAppPurchaseObserver.h"

@protocol InAppPurchaseManagerDelegate <NSObject>
@optional
- (BOOL)provideContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate;  // succeed
- (BOOL)provideRestoredContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate;  // succeed
- (void)paymentFailed:(SKPaymentTransaction *)transaction;   // failed
- (void)paymentCancelled:(SKPaymentTransaction *)transaction;    // cancelled
- (void)restoreFinished;
- (void)restoreFailed:(NSError *)error;
- (void)verifyReceiptFailed:(SKPaymentTransaction *)transaction withError:(NSError *)error;
- (void)showProductInfo:(NSArray *)products invalidProductIdentifiers:(NSArray *)invalidProductIdentifiers;   // got productInfo
@end

@interface InAppPurchaseManager : NSObject <SKProductsRequestDelegate, InAppPurchaseObserverDelegate>

@property (unsafe_unretained, nonatomic) id<InAppPurchaseManagerDelegate> delegate;

+ (BOOL)canMakePayments;
- (void)requestProductsData:(NSSet *)productIdSet;
- (void)addPayment:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;
+ (InAppPurchaseManager *)sharedInAppPurchaseManager;

@end
