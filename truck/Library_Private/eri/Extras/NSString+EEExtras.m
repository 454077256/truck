//
//  NSString+EEExtras.m
//  Libaray
//
//  Created by pai hong on 12-10-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NSString+EEExtras.h"

@implementation NSString (EEExtras)




#pragma mark -
#pragma mark -----URL 编码 解码----

//urlEncode 对url就行编码
- (NSString *)ee_URLEncodedString 
{
    NSString *result = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (CFStringRef)self,
                                                                           NULL,
																		   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                           kCFStringEncodingUTF8);
	return result;
}
//urlEncode 对url进行解码 
- (NSString*)ee_URLDecodedString
{
	NSString *result = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
																						   (CFStringRef)self,
																						   CFSTR(""),
																						   kCFStringEncodingUTF8);
	return result;	
}


#pragma mark - xml 种的特殊符号 -
- (NSString *)ee_stringByDecodingXMLEntities {
    
    NSUInteger myLength = [self length];
    NSUInteger ampIndex = [self rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return self;
    }
    // Make result string with some extra capacity.
    
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:self];
    
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            return result;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
             else if ([scanner scanString:@"&lt;" intoString:NULL])
             [result appendString:@"<"];
             else if ([scanner scanString:@"&gt;" intoString:NULL])
             [result appendString:@">"];
             else if ([scanner scanString:@"&#" intoString:NULL]) {
                 BOOL gotNumber;
                 unsigned charCode;
                 NSString *xForHex = @"";
                 
                 // Is it hex or decimal?
                 if ([scanner scanString:@"x" intoString:&xForHex]) {
                     gotNumber = [scanner scanHexInt:&charCode];
                 }
                 else {
                     gotNumber = [scanner scanInt:(int*)&charCode];
                 }
                 
                 if (gotNumber) {
                     [result appendFormat:@"%C", (unichar)charCode];
                     
                     [scanner scanString:@";" intoString:NULL];
                 }
                 else {
                     NSString *unknownEntity = @"";
                     
                     [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                     
                     
                     [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
                     
                     //[scanner scanUpToString:@";" intoString:&unknownEntity];
                     //[result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                     NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
                     
                 }
                 
             }
             else {
                 NSString *amp;
                 
                 [scanner scanString:@"&" intoString:&amp];      //an isolated & symbol
                 [result appendString:amp];
                 
                 
             }
             
             }
             while (![scanner isAtEnd]);
             
             return result;
             }


-(NSString *)ee_trim{
    NSMutableString *resultFileName = [NSMutableString stringWithString:self];
    CFStringTrimWhitespace((CFMutableStringRef)resultFileName);
    return resultFileName;
}



//十六进制 to 字符串
+ (NSString *)ee_stringFromHexString:(NSString *)hexString { //
    char *myBuffer = (char *)malloc((int)[hexString length] / 2 + 1);
    bzero(myBuffer, [hexString length] / 2 + 1);
    for (int i = 0; i < [hexString length] - 1; i += 2) {
        unsigned int anInt;
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 2] = (char)anInt;
    }
    NSString *unicodeString = [NSString stringWithCString:myBuffer encoding:4];
    NSLog(@"------字符串=======%@",unicodeString);
    return unicodeString;
}
//字符串 to 十六进制
+ (NSString *)ee_hexStringFromString:(NSString *)string{
    NSData *myD = [string dataUsingEncoding:NSUTF8StringEncoding];
    Byte *bytes = (Byte *)[myD bytes];
    //下面是Byte 转换为16进制。
    NSString *hexStr=@"";
    for(int i=0;i<[myD length];i++)
    {
        NSString *newHexStr = [NSString stringWithFormat:@"%x",bytes[i]&0xff];///16进制数
        if([newHexStr length]==1)
            hexStr = [NSString stringWithFormat:@"%@0%@",hexStr,newHexStr];
        else 
            hexStr = [NSString stringWithFormat:@"%@%@",hexStr,newHexStr]; 
    } 
    return hexStr; 
}

//用正则表达式替换字符
-(NSString *)ee_replaceStringByPattern:(NSString *)rege replacestr:(NSString *)letter {
    
    NSRegularExpression *re = [NSRegularExpression regularExpressionWithPattern:rege
                                                                        options:0
                                                                          error:NULL];
    return  [re stringByReplacingMatchesInString:self
                                           options:0
                                             range:NSMakeRange(0, [self length])
                                      withTemplate:letter];
}

@end




