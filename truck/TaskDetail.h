//
//  TaskDetail.h
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface TaskDetail : EEView{
    UIScrollView *contentDetail;
    
    EEButton *processBtn1;
    EEButton *processBtn2;
    EEButton *processBtn3;
    EEButton *processBtn4;
    EEButton *processBtn5;
}

@end
