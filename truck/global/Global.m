//
//  Global.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "Global.h"
#import "XMLDictionary.h"
#import "NSDate+convenience.h"
#import "Reachability.h"
#import "UIImage+Extras.h"

static Global *_global = nil;

@implementation Global

id tmpObject	= NULL;

EEImageView *tmpImgView	= NULL;
EEButton *tmpButtom	= NULL;
EELabel *tmpLabel	= NULL;
EEView *tmpView	= NULL;
EETextFiled *tmpTextFiled	= NULL;




+ (Global*) sharedGlobal
{
    if (!_global) {
        _global = [[Global alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) ;
        NSString *document = [paths objectAtIndex:0];
        
        _global.unitDictionary = [[NSMutableDictionary alloc] init];
        //屏幕尺寸
        _global.screenSize = [[UIScreen mainScreen] currentMode].size;
        
        int deviceHeight = [Global sharedGlobal].screenSize.height;
        if (IS_IPAD) {//说明为ipad
            _global.appSize = CGSizeMake(1024, 768);
        }else if(deviceHeight==2208){//说明为6p
            _global.appSize = CGSizeMake(_global.screenSize.width/3, _global.screenSize.height/3);
        }else{
            _global.appSize = CGSizeMake(_global.screenSize.width*0.5, _global.screenSize.height*0.5);
        }
        
        //系统版本
        _global.version = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        //机型变量
        if (deviceHeight==2208) {//1242*2208
            _global.phoneType = MMPhoneType6p;
        }else if(deviceHeight==1334){//750*1334
            _global.phoneType =  MMPhoneType6;
        }else if(deviceHeight==1136){//640*1136
            _global.phoneType =  MMPhoneType5;
        }else if(deviceHeight==960){//640*960
            _global.phoneType =  MMPhoneType4;
        }
        
        trace(@"\n----------------App 启动参数------------");
        NSLog(@"\n-----> app安装路径:%@\
              \n-----> 屏幕尺寸: %@\
              \n-----> 系统版本: %f",
              document,
              [NSValue valueWithCGSize:_global.screenSize],
              _global.version);
        
    }
    return _global;
}

//[[Global sharedGlobal] ]



-(void) log:(NSString *)s level:(int)lev
{
    
    if (lev < logLevelTrace) {
        return;
    }
    
    //控制台输出
    trace(@"--log-- %@",s);
    
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Documents/log.log",NSHomeDirectory()];
    
    //    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
    BOOL isExit = [[NSFileManager defaultManager] fileExistsAtPath:logFilePath isDirectory:NO];
    if (!isExit) {
        NSLog(@"%@",@"文件不存在");
        NSString *begin = [NSString stringWithFormat:@"--------->log开始记录:\r"];
        [begin writeToFile:logFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    if ([[[NSFileManager defaultManager] attributesOfItemAtPath:logFilePath error:nil] fileSize] > 5242880) {
        //    if ([[[NSFileManager defaultManager] attributesOfItemAtPath:logFilePath error:nil] fileSize] > 100) {
        NSString *newlogFilePath = [NSString stringWithFormat:@"%@/Documents/log%@.log",NSHomeDirectory(),[NSDate date]];
        
        [[NSFileManager defaultManager] moveItemAtPath:logFilePath
                                                toPath:newlogFilePath error:NULL];
        NSString *begin = [NSString stringWithFormat:@"开始了:\r"];
        [begin writeToFile:logFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    NSFileHandle  *outFile;
    NSData *buffer;
    
    outFile = [NSFileHandle fileHandleForWritingAtPath:logFilePath];
    
    if(outFile == nil)
    {
        NSLog(@"Open of file for writing failed");
    }
    
    //找到并定位到outFile的末尾位置(在此后追加文件)
    [outFile seekToEndOfFile];
    
    //读取inFile并且将其内容写到outFile中
    NSString *bs = [NSString stringWithFormat:@"%@ %@\n",[NSDate ee_getDateString],s];
    buffer = [bs dataUsingEncoding:NSUTF8StringEncoding];
    
    [outFile writeData:buffer];
    
    //关闭读写文件
    [outFile closeFile];
    
}


+(id)loadplist:(NSString *)path{
    NSString *fliePath = [[NSBundle mainBundle] pathForResource:path ofType:nil];
    NSDictionary *result = [NSDictionary dictionaryWithContentsOfFile:fliePath];
    return result;
}

+(id)loadxml:(NSString *)path{
    NSString *fliePath = [[NSBundle mainBundle] pathForResource:path ofType:nil];
    NSDictionary *result = [NSDictionary dictionaryWithXMLFile:fliePath];
    return result;
}

+(EEView *)createViewEmpty{
    EEView *eeview = [[EEView alloc] init];
    return eeview;
}

+(EEView *)createViewByFrame:(CGRect)frame color:(UIColor *)col{
    EEView *eeview = [[EEView alloc] initWithFrame:frame];
    [eeview setBackgroundColor:col];
    return eeview;
}

#pragma mark -- UI Image
+(EEImageView *)createImage:(CGRect )frame fname:(NSString *)filePath{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    [imgview setFrame:frame];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath frame:(CGRect)frame{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    [imgview setFrame:frame];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath point:(CGPoint)point{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    [imgview setFrame:CGRectMake(point.x, point.y, img.size.width, img.size.height)];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath center:(CGPoint)point{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    imgview.center = point;
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath fitWidth:(int)fwidth point:(CGPoint)point height:(NSNumber *)resultHeight{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    
    float fheight = img.size.height*fwidth/img.size.width;
    resultHeight = [NSNumber numberWithFloat:fheight];
    
    [imgview setFrame:CGRectMake(point.x, point.y, fwidth, fheight)];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath fitSize:(CGSize)fsize center:(CGPoint)point{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    
    CGSize targetSize = [Global fitSize:img.size inSize:fsize];
    
    [imgview setFrame:CGRectMake(0, 0, targetSize.width, targetSize.height)];
    
    imgview.center = point;
    
    return imgview;
}


+(EGOImageView *)createImageEGO:(NSString *)fileURL frame:(CGRect)_frame{
    //
    EGOImageView *egoImageView = [[EGOImageView alloc] initWithFrame:_frame];
    [egoImageView setContentMode:UIViewContentModeScaleAspectFit];
    NSURL *url = [NSURL URLWithString:fileURL];
    
    //    [egoImageView setBackgroundColor:RGB(250, 247, 242)];
    //    egoImageView.delegate = self;
    [egoImageView setImageURL:url];
    return egoImageView;
}


+(EGOImageView *)createImageEGO:(NSString *)fileURL size:(CGSize)_size center:(CGPoint)point preimage:(UIImage *)_preimage{
    //
    EGOImageView *egoImageView = [[EGOImageView alloc] initWithFrame:R(0, 0, _size.width, _size.height)];
    [egoImageView setPlaceholderImage:_preimage];
    [egoImageView setContentMode:UIViewContentModeScaleAspectFit];
    //    [egoImageView setPlaceholderImage:[Global loadImg:@"ass/imagePlaceHolder.png"]];
    NSURL *url = [NSURL URLWithString:fileURL];
    egoImageView.center = point;
    
    //    [egoImageView setBackgroundColor:RGB(250, 247, 242)];
    //    egoImageView.delegate = self;
    [egoImageView setImageURL:url];
    return egoImageView;
}

+(EGOImageView *)createImageEGO:(NSString *)fileURL size:(CGSize)_size center:(CGPoint)point{
    //
    EGOImageView *egoImageView = [[EGOImageView alloc] initWithFrame:R(0, 0, _size.width, _size.height)];
    [egoImageView setContentMode:UIViewContentModeScaleAspectFit];
    //    [egoImageView setPlaceholderImage:[Global loadImg:@"ass/imagePlaceHolder.png"]];
    NSURL *url = [NSURL URLWithString:fileURL];
    egoImageView.center = point;
    
    //    [egoImageView setBackgroundColor:RGB(250, 247, 242)];
    //    egoImageView.delegate = self;
    [egoImageView setImageURL:url];
    return egoImageView;
}

#pragma mark -- 建立圆形图片 --
+(UIImageView *)createCircleImage:(NSString *)filename url:(NSString *)url size:(CGSize)_size center:(CGPoint)_center{
    if (url.length>1) {
        EGOImageView *icoImg = [Global createImageEGO:url size:_size center:_center];
        icoImg.layer.cornerRadius = _size.width*0.5;
        icoImg.clipsToBounds = YES;
        icoImg.center = _center;
        return icoImg;
    }else{
        
        UIImageView *icoImg = [Global createImage:R(0, 0, _size.width, _size.height) fname:filename];
        icoImg.layer.cornerRadius = _size.width*0.5;
        icoImg.clipsToBounds = YES;
        icoImg.center = _center;
        return icoImg;
    }
    return nil;
}

//Global createCircleImage:nil url:@"" size: center

#pragma mark -- UI UILabel
+ (EELabel  *) createLabel:(CGRect)frame label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)size fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align ;
{
    if (num == 1) {
        if (frame.size.height<size*1.4) {
            frame.size.height = size*1.4;
            frame.origin.y -= 0.2*size;
        }
    }
    
    EELabel *lbl = [[EELabel alloc] initWithFrame:frame];
    
    lbl.text = text;
    lbl.numberOfLines = num;
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    //CGFloat* colors = ;
    //
    //    lbl.font = [UIFont systemFontOfSize:size];
    lbl.font = [UIFont fontWithName:fontName size:size];
    
    lbl.alpha = CGColorGetAlpha(textcolor.CGColor) ;
    lbl.adjustsFontSizeToFitWidth = TRUE;
    //lbl.minimumFontSize = size*0.5;
    lbl.minimumScaleFactor = 0.5f;
    lbl.backgroundColor = CClear;
    [lbl setTextAlignment:align];
    [lbl setTextColor:textcolor];
    lbl.opaque = FALSE;
    return lbl;
}


+ (EELabel  *) createLabelFit:(CGSize)size point:(CGPoint)point label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)fontSize fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align resultSize:(CGSize *)resultSize
{
    EELabel *lbl = [[EELabel alloc] init];
    
    
    
    lbl.text = text;
    lbl.numberOfLines = num;
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    
    lbl.font = [UIFont fontWithName:fontName size:fontSize];
    lbl.font = [UIFont systemFontOfSize:fontSize];
    
    lbl.alpha = CGColorGetAlpha(textcolor.CGColor) ;
    lbl.adjustsFontSizeToFitWidth = TRUE;
    
    [lbl setTextAlignment:align];
    [lbl setTextColor:textcolor];
    
    CGSize resultSize1 = [lbl sizeThatFits:size];
    resultSize = &resultSize1;
    
    [lbl setFrame:CGRectMake(point.x, point.y, resultSize1.width, resultSize1.height)];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:(IS_IPAD?10:4)];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    lbl.attributedText = attributedString;
    [lbl sizeToFit];
    
    return lbl;
}

+ (EELabel  *) createLabelFit:(CGSize)size point:(CGPoint)point label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)fontSize fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align resultSize:(CGSize *)resultSize isHtml:(BOOL)ishtml
{
    EELabel *lbl = [[EELabel alloc] init];
    
    
    
    lbl.text = text;
    lbl.numberOfLines = num;
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    
    lbl.font = [UIFont fontWithName:fontName size:fontSize];
    lbl.font = [UIFont systemFontOfSize:fontSize];
    
    lbl.alpha = CGColorGetAlpha(textcolor.CGColor) ;
    lbl.adjustsFontSizeToFitWidth = TRUE;
    
    [lbl setTextAlignment:align];
    [lbl setTextColor:textcolor];
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    if (ishtml) {
        attributedString = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        lbl.attributedText = attributedString;
        
    }else{
        
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:(IS_IPAD?10:4)];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
        lbl.attributedText = attributedString;
        [lbl sizeToFit];
    }
    
    CGSize resultSize1 = [lbl sizeThatFits:size];
    resultSize = &resultSize1;
    
    [lbl setFrame:CGRectMake(point.x, point.y, resultSize1.width, resultSize1.height)];
    
    return lbl;
}

+ (EELabel  *) createLabelFitHTML:(CGSize)size point:(CGPoint)point label:(NSString*)text numLine:(int)numline resultSize:(CGSize *)resultSize
{
    EELabel *lbl = [[EELabel alloc] init];
    
    //title = [NSString stringWithFormat:@"<span style='font-size:%d;font-weight:bold;'>%@</span> <span style='font-size:%d; '>利用人乳头</span><img src=file://%@ width='40px' /> ",S(12),title,S(12),ico];
    
    
    lbl.numberOfLines = numline;
    lbl.text = text;\
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    
    //lbl.adjustsFontSizeToFitWidth = TRUE;
    
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    attributedString = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    lbl.attributedText = attributedString;
    
    
    CGSize resultSize1 = [lbl sizeThatFits:size];
    resultSize = &resultSize1;
    
    [lbl setFrame:CGRectMake(point.x, point.y, resultSize1.width, resultSize1.height)];
    
    return lbl;
}

#pragma mark -- UI Button
+(EEButton *)createBtnAlpha:(CGRect)frame clearColor:(BOOL)isClearColor delegate:(id)target action:(SEL)act{
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    if (isClearColor) {
        [button setBackgroundColor:CClear];
    }else{
        [button setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:1]];
    }
    button.alpha = 0.2;
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

+(EEButton *)createBtnColor:(CGRect)frame bgcolor:(UIColor *)bgcolor title:(NSString *)title titleNormal:(UIColor *)_col titleSelect:(UIColor *)_colS size:(int)size delegate:(id)target action:(SEL)act{
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    
    [button setBackgroundColor:bgcolor];
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:_col forState:UIControlStateNormal];
    [button setTitleColor:_colS forState:UIControlStateSelected];
    [button setTitleColor:_colS forState:UIControlStateHighlighted];
    
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    
    
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

+(EEButton *)createBtn_center:(CGPoint)center name:(NSString *)nName down:(NSString *)nDown  target:(id)target action:(SEL)act{
    
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgDown = [Global loadImg:nDown];
    
    EEButton *button = [[EEButton alloc] initWithFrame:CGRectMake(0, 0, imgNormal.size.width, imgNormal.size.height)];
    [button setImage:imgNormal forState:UIControlStateNormal];
    if (imgDown!=nil) {
        [button setImage:imgDown forState:UIControlStateHighlighted];
        [button setImage:imgDown forState:UIControlStateSelected];
    }
    button.center = center;
    
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    return button;
}


+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)dName target:(id)target action:(SEL)act{
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgSelect = [Global loadImg:dName];
    
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    [button setImage:imgNormal forState:UIControlStateNormal];
    [button setImage:imgSelect forState:UIControlStateSelected];
    [button setImage:imgSelect forState:UIControlStateHighlighted];
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName target:(id)target action:(SEL)act{
    return [Global createBtn:frame normal:nName down:nil target:target action:act];
}

+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)nDown title:(NSString *)_title col:(UIColor *)_col colS:(UIColor *)_colS size:(int)size target:(id)target  action:(SEL)act{
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgDown = [Global loadImg:nDown];
    
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    [button setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [button setBackgroundImage:imgDown forState:UIControlStateHighlighted];
    [button setBackgroundImage:imgDown forState:UIControlStateSelected];
    
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitle:_title forState:UIControlStateNormal];
    
    [button setTitleColor:_col forState:UIControlStateNormal];
    [button setTitleColor:_colS forState:UIControlStateSelected];
    [button setTitleColor:_colS forState:UIControlStateHighlighted];
    
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    
    return button;
}




+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector{
    
    return [Global createFitButton:contentEdge center:_center original:NullPoint imageName:imgName imageEdge:imgEdge title:text size:_size delegate:_delegate action:selector];
}

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector{
    
    return [Global createFitButton:contentEdge center:NullPoint original:_original imageName:imgName imageEdge:imgEdge title:text size:_size delegate:_delegate action:selector];
}

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector{
    
    
    UIImage *img = [Global loadImg:imgName];
    img = [img resizableImageWithCapInsets:imgEdge];
    
    EEButton *button = [[EEButton alloc] init];
    
    button.titleLabel.font = [UIFont systemFontOfSize:_size];
    
    [button setTitle:text forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button setBackgroundImage:nil forState:UIControlStateHighlighted];
    [button setBackgroundImage:nil forState:UIControlStateSelected];
    
    [button.titleLabel sizeToFit];
    
    //[button setFrame:CGRectMake(0, 0, 200, 21)];
    
    [button sizeToFit];
    if (contentEdge.top==0 && contentEdge.bottom==0) { //为 0 的话，说明纵向不拉伸，那么就是图片的高度
        [button setFrame:CGRectMake(0,0, button.titleLabel.width+contentEdge.left+contentEdge.right, img.size.height)];
    }else if(contentEdge.right==0 && contentEdge.left==0){
        [button setFrame:CGRectMake(0,0, img.size.width, button.titleLabel.height+contentEdge.top+contentEdge.bottom)];
    }else{
        [button setFrame:CGRectMake(0,0, button.titleLabel.width+contentEdge.left+contentEdge.right, button.titleLabel.height+contentEdge.top+contentEdge.bottom)];
    }
    [button addTarget:_delegate action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (!CGPointEqualToPoint(NullPoint, _original)) {
        [button setFrame:CGRectMake(_original.x,_original.y, button.width, button.height)];
    }
    
    if (!CGPointEqualToPoint(NullPoint, _center)) {
        button.center = _center;
    }
    
    return button;
}

#pragma mark -- UI button block

-(void)clickUIButton:(id)sender{
    EEButton *control = (EEButton *)sender;
    if (control.block_1)
    {
        control.block_1(sender);
    }
}
+(EEButton *)createBlockBtnAlpha:(CGRect)frame clearColor:(BOOL)isClearColor action:(eeBlock1)block1 {
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    if (isClearColor) {
        [button setBackgroundColor:CClear];
    }else{
        [button setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:1]];
    }
    button.alpha = 0.2;
    button.block_1 = block1;
    [button addTarget:[Global sharedGlobal] action:@selector(clickUIButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


+(EEButton *)createBlockBtnColor:(CGRect)frame bgcolor:(UIColor *)bgcolor title:(NSString *)title titleNormal:(UIColor *)_col titleSelect:(UIColor *)_colS size:(int)size  action:(eeBlock1)block1{
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    
    [button setBackgroundColor:bgcolor];
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:_col forState:UIControlStateNormal];
    [button setTitleColor:_colS forState:UIControlStateSelected];
    [button setTitleColor:_colS forState:UIControlStateHighlighted];
    
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    button.block_1 = block1;
    
    
    [button addTarget:[Global sharedGlobal] action:@selector(clickUIButton:)  forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

+(EEButton *)createBlockBtn_center:(CGPoint)center name:(NSString *)nName down:(NSString *)nDown  action:(eeBlock1)block1{
    
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgDown = [Global loadImg:nDown];
    
    EEButton *button = [[EEButton alloc] initWithFrame:CGRectMake(0, 0, imgNormal.size.width, imgNormal.size.height)];
    [button setImage:imgNormal forState:UIControlStateNormal];
    if (imgDown!=nil) {
        [button setImage:imgDown forState:UIControlStateHighlighted];
        [button setImage:imgDown forState:UIControlStateSelected];
    }
    button.center = center;
    button.block_1 = block1;
    
    [button addTarget:[Global sharedGlobal] action:@selector(clickUIButton:)  forControlEvents:UIControlEventTouchUpInside];
    return button;
}


+(EEButton *)createBlockBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)dName  action:(eeBlock1)block1{
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgSelect = [Global loadImg:dName];
    
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    button.block_1 = block1;
    [button setImage:imgNormal forState:UIControlStateNormal];
    [button setImage:imgSelect forState:UIControlStateSelected];
    [button setImage:imgSelect forState:UIControlStateHighlighted];
    [button addTarget:[Global sharedGlobal] action:@selector(clickUIButton:)  forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


+(EEButton *)createBlockBtn:(CGRect)frame normal:(NSString *)nName  action:(eeBlock1)block1{
    return [Global createBlockBtn:frame normal:nName down:nil action:block1];
}

+(EEButton *)createBlockBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)nDown title:(NSString *)_title col:(UIColor *)_col colS:(UIColor *)_colS size:(int)size  action:(eeBlock1)block1{
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgDown = [Global loadImg:nDown];
    
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    button.block_1 = block1;
    [button setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [button setBackgroundImage:imgDown forState:UIControlStateHighlighted];
    [button setBackgroundImage:imgDown forState:UIControlStateSelected];
    
    [button addTarget:[Global sharedGlobal] action:@selector(clickUIButton:)  forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitle:_title forState:UIControlStateNormal];
    
    [button setTitleColor:_col forState:UIControlStateNormal];
    [button setTitleColor:_colS forState:UIControlStateSelected];
    [button setTitleColor:_colS forState:UIControlStateHighlighted];
    
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    
    return button;
}




+(EEButton *)createBlockFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size  action:(eeBlock1)block1{
    
    return [Global createBlockFitButton:contentEdge center:_center original:NullPoint imageName:imgName imageEdge:imgEdge title:text size:_size  action:block1];
}

+(EEButton *)createBlockFitButton:(UIEdgeInsets)contentEdge original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size  action:(eeBlock1)block1{
    
    return [Global createBlockFitButton:contentEdge center:NullPoint original:_original imageName:imgName imageEdge:imgEdge title:text size:_size action:block1];
}

+(EEButton *)createBlockFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size  action:(eeBlock1)block1{
    
    
    UIImage *img = [Global loadImg:imgName];
    img = [img resizableImageWithCapInsets:imgEdge];
    
    EEButton *button = [[EEButton alloc] init];
    button.block_1 = block1;
    
    button.titleLabel.font = [UIFont systemFontOfSize:_size];
    
    [button setTitle:text forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button setBackgroundImage:nil forState:UIControlStateHighlighted];
    [button setBackgroundImage:nil forState:UIControlStateSelected];
    
    [button.titleLabel sizeToFit];
    
    //[button setFrame:CGRectMake(0, 0, 200, 21)];
    
    [button sizeToFit];
    if (contentEdge.top==0 && contentEdge.bottom==0) { //为 0 的话，说明纵向不拉伸，那么就是图片的高度
        [button setFrame:CGRectMake(0,0, button.titleLabel.width+contentEdge.left+contentEdge.right, img.size.height)];
    }else if(contentEdge.right==0 && contentEdge.left==0){
        [button setFrame:CGRectMake(0,0, img.size.width, button.titleLabel.height+contentEdge.top+contentEdge.bottom)];
    }else{
        [button setFrame:CGRectMake(0,0, button.titleLabel.width+contentEdge.left+contentEdge.right, button.titleLabel.height+contentEdge.top+contentEdge.bottom)];
    }
    [button addTarget:[Global sharedGlobal] action:@selector(clickUIButton:)  forControlEvents:UIControlEventTouchUpInside];
    
    if (!CGPointEqualToPoint(NullPoint, _original)) {
        [button setFrame:CGRectMake(_original.x,_original.y, button.width, button.height)];
    }
    
    if (!CGPointEqualToPoint(NullPoint, _center)) {
        button.center = _center;
    }
    
    return button;
}

#pragma mark -- UI TextFiled
+(EETextFiled *)createTextFiled:(CGRect )frame size:(int)size color:(UIColor *)color{
    EETextFiled *filed = [[EETextFiled alloc] initWithFrame:frame];
    [filed setFont:[UIFont systemFontOfSize:size]];
    [filed setTextColor:color];
    return filed;
}


+(EETextView *)createTextView:(CGRect )frame size:(int)fontsize color:(UIColor *)color txt:(NSString *)texts{
    EETextView *content = [[EETextView alloc] initWithFrame:frame];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 4;// 字体的行间距
    paragraphStyle.alignment = NSTextAlignmentJustified;
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:fontsize],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 NSForegroundColorAttributeName:color
                                 };
    content.attributedText = [[NSAttributedString alloc] initWithString:texts attributes:attributes];
    
    [content setFrame:R(content.x, content.y, content.width, content.contentSize.height)];
    content.userInteractionEnabled = NO;
    
    return content;
}


#pragma mark -- UI UIControl
+(EEControl *)createControl:(CGRect)frame delegate:(id)delegate action:(SEL)selector color:(UIColor *)color  isHighLight:(BOOL)isHigh{
    EEControl *control =  [[EEControl alloc] initWithFrame:frame];
    [control addTarget:delegate action:selector forControlEvents:(UIControlEventTouchUpInside)];
    [control setBackgroundColor:color];
    return control;
}
//
////block形式  延迟执行
//-(void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay{
//    block = [[block copy] autorelease];
//    [self performSelector:@selector(runPerformBlock:) withObject:block afterDelay:delay];
//}
//
//-(void)runPerformBlock:(void(^)(void))block{
//    block();
//    block = nil;
//}
//
+(EEControl *)createControlBlock:(CGRect)frame action:(eeBlock1)block1 color:(UIColor *)color isHighLight:(BOOL)isHigh{
    EEControl *control =  [[EEControl alloc] initWithFrame:frame];
    control.block_1 = block1;
    [control addTarget:[Global sharedGlobal] action:@selector(clickUIControl:) forControlEvents:(UIControlEventTouchUpInside)];
    [control setBackgroundColor:color];
    control.isShowDarkHighLight = isHigh;
    return control;
}

-(void)clickUIControl:(id)sender{
    EEControl *control = (EEControl *)sender;
    if (control.block_1)
    {
        control.block_1(sender);
    }
}


#pragma mark -- UI UIScrollView
+(EEScrollView *)createScroll:(CGRect)frame delegate:(id)_delegate content:(UIView *)_content pageEnable:(BOOL)pageEnable zoom:(BOOL)isZoom{
    EEScrollView *contentScroll = [[EEScrollView alloc] initWithFrame:frame];
    contentScroll.delegate = _delegate;
    contentScroll.pagingEnabled = pageEnable;
    [contentScroll setShowsHorizontalScrollIndicator:NO];
    if (isZoom) {
        contentScroll.multipleTouchEnabled = YES;
        contentScroll.minimumZoomScale = 1;
        contentScroll.maximumZoomScale = 2;
        contentScroll.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    [contentScroll addSubview:_content];
    [contentScroll setContentSize:CGSizeMake(_content.width, _content.height)];
    
    return contentScroll;
}
////下面的绝对不能少
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//    return contentImageView;
//}

#pragma mark -- view 操作
+(void)addShadowToView:(UIView *)pview tag:(MMShadow)tag {
    //加入阴影
    if (tag == MMShadowLeft) {
        UIImage *yingy = [Global loadImg:@"img/bgyingyinleft.png"];
        UIView *vv = [Global createViewByFrame:R(-yingy.size.width, 0, yingy.size.width, pview.height) color:[UIColor colorWithPatternImage:yingy]];
        [pview addSubview:vv];
    }else if(tag==MMShadowRight){
        UIImage *yingy = [Global loadImg:@"img/bgyingyinright.png"];
        UIView *vv = [Global createViewByFrame:R(pview.width, 0, yingy.size.width, pview.height) color:[UIColor colorWithPatternImage:yingy]];
        [pview addSubview:vv];
    }else if(tag==MMShadowUp){
        UIImage *yingy = [Global loadImg:@"img/bgyingyinup.png"];
        UIView *vv = [Global createViewByFrame:R(0, -yingy.size.height, pview.size.width, yingy.size.height) color:[UIColor colorWithPatternImage:yingy]];
        [pview addSubview:vv];
    }else if(tag==MMShadowDown){
        UIImage *yingy = [Global loadImg:@"img/bgyingyindown.png"];
        UIView *vv = [Global createViewByFrame:R(0, pview.size.height, pview.size.width, yingy.size.height) color:[UIColor colorWithPatternImage:yingy]];
        [pview addSubview:vv];
    }else if(tag==MMShadowALL){
        pview.layer.shadowColor = CBlack.CGColor;
        pview.layer.shadowRadius = 5;
        pview.layer.shadowOpacity = 0.8;
        pview.layer.shadowOffset = CGSizeMake(0, 0);//shadowOffset阴影偏移,x向右偏移4，y向下偏移4，默认(0, -3),这个跟shadowRadius配合使用
    }
    
    pview.clipsToBounds = NO;
}

+(void)addBorder:(UIView *)v borderWidth:(CGFloat)bw color:(UIColor *)col radius:(int)r{
    v.layer.borderWidth =1;
    v.layer.borderColor = col.CGColor;
    v.layer.cornerRadius = r;
}

+(void)addDashBorder:(UIView *)instance color:(UIColor *)_color{
    
    CAShapeLayer *border = [CAShapeLayer layer];
    
    border.strokeColor = _color.CGColor;
    
    border.fillColor = nil;
    
    border.path = [UIBezierPath bezierPathWithRect:instance.bounds].CGPath;
    
    border.frame = instance.bounds;
    
    border.lineWidth = 1.f;
    
    border.lineCap = @"butt";
    
    border.lineDashPattern = @[@4, @3];
    
    [instance.layer addSublayer:border];
    instance.layer.cornerRadius = S(4);
}

-(void)resetViewIntFrame:(UIView *)view{
    [view setFrame:R((int)view.x, (int)view.y, view.width, view.height)];
}

#pragma mark -- gif
+(UIWebView *)createGif:(CGRect)frame path:(NSString *)filepath{
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:R(0, 0, frame.size.width, frame.size.height)];
    webview.userInteractionEnabled  =NO;
    [webview.scrollView setShowsVerticalScrollIndicator:NO];
    [webview.scrollView setScrollEnabled:NO];
    
    NSData *gif = [NSData dataWithContentsOfFile:filepath];
    
    [webview loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
    return webview;
}



+(UIActivityIndicatorView *)createHud:(CGSize)size center:(CGPoint)center style:(UIActivityIndicatorViewStyle)style{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:R(0, 0, size.width, size.height)];//指定进度轮的大小
    [activity setActivityIndicatorViewStyle:style];//设置进度轮显示类型
    [activity startAnimating];
    activity.center = center;
    return activity;
}



+(UIImage *)loadImg:(NSString *)fname{
    if (fname==nil || fname.length<=0) {
        return nil;
    }
    
    float scaleT = 1.0;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[fname stringByDeletingPathExtension] ofType:[fname pathExtension]];
    float ss = [[UIScreen mainScreen] scale];
    if (ss != 2.0 && ss !=3.0 ) {//正对ipad2  如果x.png存在就读取，没有存在就不读取
        if (path==nil) {
            path = [[NSBundle mainBundle] pathForResource:([NSString stringWithFormat:@"%@@2x",[fname stringByDeletingPathExtension]]) ofType:[fname pathExtension]];
            scaleT = 2.0;
        }
        if ( [[NSFileManager defaultManager] fileExistsAtPath:path] ) {
            UIImage *img= [UIImage imageWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path]] CGImage] scale:scaleT orientation:UIImageOrientationUp];
            return img;
        }
    }
    NSString *path2x = nil;
    if (ss==2.0) {
        path2x = [[path stringByDeletingLastPathComponent]
                  stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                  [[path lastPathComponent] stringByDeletingPathExtension],
                                                  [path pathExtension]]];
        scaleT = 2.0;
    }else if(ss==3.0){
        path2x = [[path stringByDeletingLastPathComponent]
                  stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@3x.%@",
                                                  [[path lastPathComponent] stringByDeletingPathExtension],
                                                  [path pathExtension]]];
        scaleT = 3.0;
    }else if(ss==1.0){
        
        path2x = [[path stringByDeletingLastPathComponent]
                  stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                  [[path lastPathComponent] stringByDeletingPathExtension],
                                                  [path pathExtension]]];
        scaleT = 2.0;
    }
    if (path2x==nil) {
        if (ss==2.0) {
            path2x = path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@@2x",[fname stringByDeletingPathExtension]] ofType:[fname pathExtension]];
            scaleT = 2.0;
        }else if(ss==3.0) {
            path2x = path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@@3x",[fname stringByDeletingPathExtension]] ofType:[fname pathExtension]];
            scaleT = 3.0;
            if (path2x==nil) {
                path2x = path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@@2x",[fname stringByDeletingPathExtension]] ofType:[fname pathExtension]];
                scaleT = 2.0;
            }
        }
        
    }
    
    //下面的代码是让iphone6 、 iphone5去适应iphone6p的1242尺寸的素材
    //如果还为空的话，检测是否存在@3x的图片
    if (path2x==nil && ss!=3.0) {
        path2x = [[NSBundle mainBundle] pathForResource:([NSString stringWithFormat:@"%@@3x",[fname stringByDeletingPathExtension]]) ofType:[fname pathExtension]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path2x] ) {
            UIImage *img= [UIImage imageWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path2x]] CGImage] scale:1 orientation:UIImageOrientationUp];
            //按比例缩小
            int w = img.size.width;
            int h = img.size.height;
            if (M_PhoneType == MMPhoneType6) {
                w = w*0.60;
                h = h*0.60;
                
            }else if(M_PhoneType == MMPhoneType5){
                w = w*0.515;
                h = h*0.515;
                
            }
            
            img = [img imageScale_to_Width:w height:h];
            img = [UIImage imageWithCGImage:img.CGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
            return img;
        }
    }
    

    
    if ( [[NSFileManager defaultManager] fileExistsAtPath:path2x] ) {
        UIImage *img= [UIImage imageWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path2x]] CGImage] scale:scaleT orientation:UIImageOrientationUp];
        return img;
    }else{
        if ( [[NSFileManager defaultManager] fileExistsAtPath:path]) {
            UIImage *img= [UIImage imageWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path]] CGImage] scale:1.0 orientation:UIImageOrientationUp];
            return img;
        }else{
            traceException(@"%@ 文件不存在",fname);
        }
    }
    return nil;
}


+(UIImage *)loadImgCache:(NSString *)fname {
    UIImage *img = [UIImage imageNamed:fname];
    if (img==nil) {
        traceException(@"%@ 文件不存在",fname);
        return nil;
    }
    return img;
}


+(NSString *)assets:(NSString *)fileName{
    return  [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
}

#pragma mark -- 文件夹操作 --
//单个文件的大小
+ (long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}
//遍历文件夹获得文件夹大小，返回多少M
+ (float ) folderSizeAtPath:(NSString*) folderPath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [Global fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/(1024.0*1024.0);
}


//只有在UTF16的编码下才能正确显示中文，因此我们需要在展示文件之前将文件的编码改为UTF16
+(NSString *)converTxtEncodeFromWin2:(NSData *)winTxtFileData{
    
    //判断是UNICODE编码
    NSString *isUNICODE = [[NSString alloc] initWithData:winTxtFileData encoding:NSUTF8StringEncoding];
    //还是ANSI编码
    NSString *isANSI = [[NSString alloc] initWithData:winTxtFileData encoding:-2147482062];
    
    if (isUNICODE) {
        
        NSString *retStr = [[NSString alloc]initWithCString:[isUNICODE UTF8String] encoding:NSUTF8StringEncoding];
        
        NSData *data = [retStr dataUsingEncoding:NSUTF16StringEncoding];
        
        NSString *ss = [[NSString alloc] initWithData:data encoding:NSUTF16StringEncoding];
        
        return ss;
        
    }else if(isANSI){
        
        NSData *data = [isANSI dataUsingEncoding:NSUTF16StringEncoding];
        
        NSString *ss = [[NSString alloc] initWithData:data encoding:NSUTF16StringEncoding];
        return ss;
    }
    traceError(@"没有符合的编码");
    return nil;
}

+(NSData *)converTxtEncodeFromWin:(NSData *)winTxtFileData{
    
    //判断是UNICODE编码
    NSString *isUNICODE = [[NSString alloc] initWithData:winTxtFileData encoding:NSUTF8StringEncoding];
    //还是ANSI编码
    NSString *isANSI = [[NSString alloc] initWithData:winTxtFileData encoding:-2147482062];
    if (isUNICODE) {
        NSString *retStr = [[NSString alloc]initWithCString:[isUNICODE UTF8String] encoding:NSUTF8StringEncoding];
        NSData *data = [retStr dataUsingEncoding:NSUTF16StringEncoding];
        return data;
    }else if(isANSI){
        NSData *data = [isANSI dataUsingEncoding:NSUTF16StringEncoding];
        return data;
    }
    traceError(@"没有符合的编码");
    return nil;
}

+(NSMutableArray *)getAllFilesInFolder:(NSString *)folderPath{
    NSFileManager *filemanager = [NSFileManager defaultManager];
    NSDirectoryEnumerator *myDirectoryEnumerator=[filemanager enumeratorAtPath:folderPath];
    id path;
    NSMutableArray *list = [NSMutableArray array];
    while ((path=[myDirectoryEnumerator nextObject])!=nil) {
        NSString *filepath = [folderPath stringByAppendingPathComponent:path];
        
        BOOL isDictionary = NO;
        BOOL isExist = [filemanager fileExistsAtPath:filepath isDirectory:&isDictionary];//如果是文件夹的话，则不添加进来
        if (isExist && !isDictionary) {
            [list addObject:path];
        }else{
            trace(@"%@",path);
        }
        
    }
    return list;
}

#pragma mark -- 系统设定
+(void)setAppWeakupAllTime:(BOOL)isAllWeakup{
    [[UIApplication sharedApplication] setIdleTimerDisabled:isAllWeakup];
}

#pragma mark -- 工具方法
+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize
{
    CGFloat scale;
    CGSize newsize;
    
    if(thisSize.width<aSize.width && thisSize.height < aSize.height)
    {
        newsize = thisSize;
    }
    else
    {
        if(thisSize.width >= thisSize.height)
        {
            scale = aSize.width/thisSize.width;
            newsize.width = aSize.width;
            newsize.height = thisSize.height*scale;
        }
        else
        {
            scale = aSize.height/thisSize.height;
            newsize.height = aSize.height;
            newsize.width = thisSize.width*scale;
        }
    }
    return newsize;
}



//全局弹出信息窗口
-(void)alert:(NSString *)content {
    trace(@"%@",content);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:content delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    [alert show];
}
+(void)alertNoWeb{
    [[Global sharedGlobal] alert:@"没有网络"];
}
+(NSString *)getAppVersion{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+(NSString *)getIdentifier{
    NSString *identifier = [[NSBundle mainBundle] bundleIdentifier];
    return  identifier;
}


#pragma mark -- EGOImage

+(NSString *)egoPathConvert:(NSString *)filePath  style:(NSString *)style{
    NSString *pathKey = nil;
    if(style){
        pathKey = [NSString stringWithFormat:@"EGOImageLoader-%lu-%lu", (unsigned long)[filePath hash], (unsigned long)[style hash]];
    }else{
        pathKey = [NSString stringWithFormat:@"EGOImageLoader-%lu", (unsigned long)[filePath hash]];
    }
    
    NSString* cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *  _EGOCacheDirectory = [[cachesDirectory stringByAppendingPathComponent:[[NSProcessInfo processInfo] processName]] stringByAppendingPathComponent:@"EGOCache"];
    
    NSString *convertfilepath = [_EGOCacheDirectory stringByAppendingPathComponent:pathKey];
    
    return convertfilepath;
}

+(BOOL)fileIsInEGOCache:(NSString *)filePath style:(NSString *)style{
    
    NSString *convertFilePath = [Global egoPathConvert:filePath style:style];
    
    BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:convertFilePath];
    if (isExist) {
        
        return YES;
    }else {
        return NO;
    }
}


#pragma mark -- 数组操作
+(void)arrayRandom:(NSMutableArray *)pArray{
    for (int i = (int)(pArray.count-1); i > 0; i--)
    {
        [pArray exchangeObjectAtIndex:i withObjectAtIndex:(NSInteger)(arc4random_uniform(i+1))];
    }
}

+(void)arraySort:(NSMutableArray *)postArray key:(NSString *)sortKey ascending:(BOOL)isAscending{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:isAscending];
    NSArray *sortA = @[sortDescriptor];
    [postArray sortUsingDescriptors:sortA];
}

+(void)filterNullValue:(id)postData{
    if([postData isKindOfClass:[NSMutableDictionary class]]){
        NSArray *allKeys = [postData allKeys];
        for(int i=0;i<[allKeys count];i++){
            id curValue = postData[allKeys[i]];
            if(curValue==nil || curValue == [NSNull null]){
                postData[allKeys[i]] = @"";
            }else if([curValue isKindOfClass:[NSMutableArray class]]){
                [Global filterNullValue:curValue];
            }else if([curValue isKindOfClass:[NSMutableDictionary class]]){
                [Global filterNullValue:curValue];
            }
        }
    }else if([postData isKindOfClass:[NSMutableArray class]]){
        for (int i=0; i<[postData count]; i++) {
            id curData = postData[i];
            if([curData isKindOfClass:[NSString class]]){
            }else{
                [Global filterNullValue:curData];
            }
        }
    }
    
}
#pragma mark -- 颜色 color
+(UIColor*)colorWithHex:(uint)value{
    float rc = (value & 0xFF0000) >> 16;
    float gc = (value & 0xFF00) >> 8;
    float bc = (value & 0xFF);
    return [UIColor colorWithRed:rc/255.0 green:gc/255.0 blue:bc/255.0 alpha:1.0];
}

#pragma mark ---日期操作
+(NSDate *)getDateByString:(NSString *)dateString formatString:(NSString *)forString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    if (forString==nil) {
        [formatter setDateFormat:@"yyyy年MM月dd日"];
    }else{
        [formatter setDateFormat:forString];
    }
    return [formatter dateFromString:dateString];
}
+(NSString *)getDateString:(NSDate *)date formatString:(NSString *)forString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    if (forString==nil) {
        [formatter setDateFormat:@"yyyy年MM月dd日"];
    }else{
        [formatter setDateFormat:forString];
    }
    //    NSDate *date=[formatter dateFromString:date];
    return [formatter stringFromDate:date];
}

+(NSTimeInterval)getTwoDateDistance:(NSDate *)d1 :(NSDate *)d2{
    return  abs([d1 timeIntervalSinceDate:d2]) ;
}
//
//l getDateByString:lastLoginTime formatString:@"yyyy-MM-dd HH:mm:ss"];
//int count = -[dd timeIntervalSinceNow];


+(NSString *) compareCurrentTime:(NSDate*) compareDate
//
{
    NSTimeInterval  timeInterval = [compareDate timeIntervalSinceNow];
    timeInterval = -timeInterval;
    long temp = 0;
    NSString *result;
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    }
    else if((temp = timeInterval/60) <60){
        result = [NSString stringWithFormat:@"%ld分前",temp];
    }
    
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%ld小前",temp];
    }
    
    else if((temp = temp/24) <30){
        result = [NSString stringWithFormat:@"%ld天前",temp];
    }
    
    else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%ld月前",temp];
    }
    else{
        temp = temp/12;
        result = [NSString stringWithFormat:@"%ld年前",temp];
    }
    
    return  result;
}



#pragma mark ---状态栏

+(void)statusBarShow:(BOOL)isShow{
    if (!isShow && IS_IPAD) {
        return;
    }
    [[UIApplication sharedApplication] setStatusBarHidden:!isShow withAnimation:UIStatusBarAnimationFade];
}

+(CGRect)uisize_isHasStatusSpace:(BOOL)isHasStatusSpace{
    int statusHeight = 20;
    
    float sysVersion = [Global sharedGlobal].version;
    int deviceWidth  = [Global sharedGlobal].screenSize.width;
    int deviceHeight = [Global sharedGlobal].screenSize.height;
    if (IS_IPAD) {//说明为ipad
        deviceWidth     = 768;
        deviceHeight    = 1024;
    }else if(deviceHeight==2208){
        deviceWidth     = deviceWidth/3;
        deviceHeight    = deviceHeight/3;
    }else{
        deviceWidth     = deviceWidth*0.5;
        deviceHeight     = deviceHeight*0.5;
    }
    
    if (isHasStatusSpace && sysVersion<7.0) {//7.0以下 有状态栏
        return R(0, 0, deviceWidth, deviceHeight-statusHeight);
    }else if(isHasStatusSpace && sysVersion>=7.0){
        return R(0, statusHeight, deviceWidth, deviceHeight-statusHeight);
    }else{
        return R(0, 0, deviceWidth, deviceHeight);
    }
    
}

#pragma mark -- 机型适配

+(int)uiYstartIOS678{
    if ([Global sharedGlobal].version>=7.0) {
        return 20;
    }else{
        return 0;
    }
}

-(float)pad_phone:(float)x1 :(float)x2{
    return  IS_IPAD?x1:x2;
}

-(float)p5_6_6p:(float)x1 :(float)x2 :(float)x3{
    if (M_PhoneType == MMPhoneType6p) {
        return x3;
    }
    if (M_PhoneType == MMPhoneType6) {
        return x2;
    }
    return x1;
}


+(float)multiHeight:(float)value{//1.3364
    if(M_PhoneType == MMPhoneType6p) {
        return value*1.3;
    }else if(M_PhoneType == MMPhoneType6){
        return value*1.17;
    }else if(M_PhoneType == MMPhoneType6p_FS){
        return value*1.17;
    }else if(M_PhoneType == MMPhoneTypePad){
        return value*1;
    }else{
        return value;
    }
}


+(float)multiHeightPhone:(float)phone WithPad:(float)pad{
    if(M_PhoneType == MMPhoneType6p) {
        return phone*1.3;
    }else if(M_PhoneType == MMPhoneType6){
        return phone*1.17;
    }else if(M_PhoneType == MMPhoneType6p_FS){
        return phone*1.17;
    }else if(M_PhoneType == MMPhoneTypePad){
        return pad;
    }else{
        return phone;
    }
}

+(float)multiHeightAA:(float)p5 :(float)p6 :(float)p6p :(float)pad{
    if (M_PhoneType==MMPhoneType5) {
        return p5 *0.5;
    }else if(M_PhoneType == MMPhoneType6){
        return p6 *0.5;
    }else  if(M_PhoneType == MMPhoneType6p) {
        return p6p*0.333;
    }else if(M_PhoneType == MMPhoneType6p_FS){
        return p6p*1.17*0.333;
    }else if(M_PhoneType == MMPhoneTypePad){
        return pad *0.5;
    }else{
        return p5 *0.5;
    }
}

#pragma mark --  网络
+(MMNetworkStatus)netWorkState{
    Reachability * reach = [Reachability reachabilityForInternetConnection];
    if ([reach currentReachabilityStatus] == NotReachable) {
        return MMNetworkStatusNO;
    }else if([reach currentReachabilityStatus] == ReachableViaWiFi){
        return MMNetworkStatusWiFi;
    }else{
        return MMNetworkStatus3g;
    }
}

#pragma mark -- 字符串检测

+(BOOL)isImageFileName:(NSString *)oneString{//判断一个字符串是否是 图片的文件名称
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@".?\\.(png|jpg|PNG|JPG|jpeg|JPEG)$" options:0 error:&error];
    NSTextCheckingResult *firtmakr = [regex firstMatchInString:oneString options:0 range:NSMakeRange(0, [oneString length])];
    
    if (firtmakr) {//说明是图片
        return YES;
    }else{
        return NO;
    }
}
#pragma mark -- NSDictionary 操作
+(void)setObject:(NSMutableDictionary *)dic value:(NSString *)_value key:(NSString *)_key{
    if (_value==nil || _value.length<=0) {
        return;
    }
    [dic setObject:_value forKey:_key];
}



#pragma mark -- EGO 本地缓存
+(NSString *)egoCacheFolder{
    NSString* cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return [[cachesDirectory stringByAppendingPathComponent:[[NSProcessInfo processInfo] processName]] stringByAppendingPathComponent:@"EGOCache"];
}

+(NSString *)egoKey:(NSString *)url{
    return [NSString stringWithFormat:@"EGOImageLoader-%u", [[url description] hash]];
}

-(void)egoSaveLocalImageData:(UIImage *)localImage url:(NSString *)webpath{
    
    NSString *key = [Global egoKey:webpath];
    
    NSString *completePath = [[Global egoCacheFolder] stringByAppendingPathComponent:key];
    
    NSData *data = UIImagePNGRepresentation(localImage);
    
    BOOL success = [data writeToFile:completePath atomically:YES];
    if (success) {
        trace(@"保存ego图片成功 %@",webpath);
    }else{
        trace(@"保存ego图片失败!!!!! %@",webpath);
    }
}


@end
