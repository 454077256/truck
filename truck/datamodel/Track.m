//
//  Track.m
//  Lohas
//
//  Created by 洪湃 on 15-4-25.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "Track.h"
#import "BaiduMobStat.h"

@implementation Track

+(void)postDaily:(NSString *)strContent{
    [[BaiduMobStat defaultStat] logEvent:@"103" eventLabel:strContent];
    
    [Flurry logEvent:@"每日推荐" withParameters:@{@"target":strContent}];
}
+(void)postSeason:(NSString *)strContent{
    [[BaiduMobStat defaultStat] logEvent:@"104" eventLabel:strContent];
    
    [Flurry logEvent:@"节气令" withParameters:@{@"target":strContent}];
}
+(void)postOpenMagazine:(NSString *)strContent{
    [[BaiduMobStat defaultStat] logEvent:@"105" eventLabel:strContent];
    
    [Flurry logEvent:@"打开杂志" withParameters:@{@"target":strContent}];
}
+(void)postDownLoadMagazine:(NSString *)strContent{
    [[BaiduMobStat defaultStat] logEvent:@"108" eventLabel:strContent];
    
    [Flurry logEvent:@"下载杂志" withParameters:@{@"target":strContent}];
}

+(void)postHaoKanVideo{
    [[BaiduMobStat defaultStat] logEvent:@"106" eventLabel:@"好看视频"];
    
    [Flurry logEvent:@"打开好看视频"];
}
+(void)postRiZiDian{
    [[BaiduMobStat defaultStat] logEvent:@"107" eventLabel:@"日子店"];
    
    [Flurry logEvent:@"打开日资店"];
}
+(void)postSlider:(NSString *)title{
    //检测是iphone 还是ipad
    [[BaiduMobStat defaultStat] logEvent:@"109" eventLabel:title];
    
    [Flurry logEvent:@"点击轮播图" withParameters:@{@"target":title}];
}
+(void)postWeakupApp{
    [[BaiduMobStat defaultStat] logEvent:@"110" eventLabel:@"唤醒程序"];
    
    [Flurry logEvent:@"唤醒程序"];
    
}



@end
