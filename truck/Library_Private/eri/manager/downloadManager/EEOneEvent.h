//
//  EEOneEvent.h
//  OuRuiDA
//
//  Created by 洪湃 on 16/6/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "LDProgressView.h"



@protocol EEOneEventDelegate <NSObject>

-(void)EEOneEventDownloadSuccess:(id)oneEvent data:(NSDictionary *)eeData;
-(void)EEOneEventDownloadFailed:(id)oneEvent data:(NSDictionary *)eeData;

@end


@interface EEOneEvent : NSObject{
    
    ASIFormDataRequest *dataRequest;
    
    BOOL isStarting;
}

@property(nonatomic ,weak)id<EEOneEventDelegate> delegate;

@property(nonatomic)NSUInteger hashCode;
@property(nonatomic,strong)NSDictionary *eeData;
@property(nonatomic,copy)NSString *webFilePath;
@property(nonatomic,copy)NSString *localFilePath;
@property(nonatomic,strong)LDProgressView *progressView;


+(EEOneEvent *)instanceWithWebFilePath:(NSString *)webpath savePath:(NSString *)localSavePath configData:(NSDictionary *)pdata progresView:(LDProgressView *)progressView;
-(void)start;
-(void)stop;
-(void)disponse;

    
@end
