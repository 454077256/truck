//
//  EETopBar.h
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface EETopBar : EEView{
    
}

@property(nonatomic,strong)EEButton *rightButton;
@property(nonatomic,strong)EEButton *leftButton;
@property(nonatomic,strong)EELabel *centerLabel;

+(id)instanceBy_normalLeft:(NSString *)normalLeft selectLeft:(NSString *)selectLeft
                leftAction:(SEL)leftAction leftDelegate:(id)leftDelegate
               normalRight:(NSString *)normalRight selectRight:(NSString *)selectRight
               rightAction:(SEL)rightAction rightDelegate:(id)rightDelegate
                    center:(NSString *)centerName
              isIOS7Lagger:(BOOL)isios7lagger;

+(id)instanceBy_normalLeft:(NSString *)normalLeft selectLeft:(NSString *)selectLeft
                leftAction:(SEL)leftAction leftDelegate:(id)leftDelegate
               normalRight:(NSString *)normalRight selectRight:(NSString *)selectRight
               rightAction:(SEL)rightAction rightDelegate:(id)rightDelegate
                    center:(NSString *)centerName
              isIOS7Lagger:(BOOL)isios7lagger width:(float)fwidth;

-(void)setDownLiner:(UIColor *)color;

-(void)hiddeLeftButonIfIpad;
-(void)hiddeRightButonIfIpad;

-(void)changedLeftBtnPoint:(CGPoint)point anch:(CGPoint)anc;



@end
