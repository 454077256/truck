//
//  URequest.m
//  Lohas
//
//  Created by 洪湃 on 15-3-24.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "URequest.h"

@implementation URequest

-(void)fire{
    NSURL *url = [NSURL URLWithString:[self.configData objectForKey:@"url"]];
    webrequest = [WebRequestProxy request:url data:self.configData delegate:self success:@selector(reqeustResult:) failed:@selector(reqeustFailed:)];
    [webrequest startAsynchronous];
    
}



-(void)reqeustResult:(NSDictionary *)postData{
//    trace(@"%@",postData);
    
    BOOL isSuccess = [[postData objectForKey:@"success"] boolValue];
    if (isSuccess) {
        [self doComplete:postData];
    }else{
        
        NSString *sName = [postData objectForKey:@"error"];
        
        [self doFailed:sName];
    }
}

-(void)reqeustFailed:(ASIHTTPRequest *)request{
    [self doFailed:@"网络错误"];
}


#pragma mark --- 回收 ---
-(void)disponse{
    webrequest = nil;
    [super disponse];
}


- (void)dealloc
{
    track();
}

@end
