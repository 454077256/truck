//
//  AppTimer.m
//  Lohas
//
//  Created by 洪湃 on 15-3-16.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "AppTimer.h"

@implementation AppTimer


- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)createTimer{
    autoPlayerTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(rollPlayer) userInfo:nil repeats:YES];
    [autoPlayerTimer fire];
    
}

+(int)getClockNum{
    return clockNum;
}

+(void)setClockNum:(int)num{
    clockNum = num;
}

+(void)clearClockNum{
    clockNum = 0;
}

#pragma mark --
-(void)rollPlayer{
    
//    BOOL isScroll = [[[AppDataConfig appData] objectForKey:@"isSliderAutoScroll"] boolValue];
//    if(!isScroll){
//        return;
//    }
//    
//    
//    clockNum += 1;
//    
//    if (clockNum==4) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppClockNoticeNum" object:[NSNumber numberWithInt:clockNum]];
//        clockNum = 0;
//    }
}


@end
