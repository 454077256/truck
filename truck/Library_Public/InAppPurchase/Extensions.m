//
//  Extensions.m
//  iMW
//
//  Created by James Chen on 6/8/12.
//  Copyright (c) 2012 Rakuraku Technologies, Inc. All rights reserved.
//

#import "Extensions.h"
#import <QuartzCore/QuartzCore.h>
#import <CommonCrypto/CommonDigest.h>

#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>


@implementation UIImage (INRoundedCornerShadow)

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight){
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

+ (UIImage *)roundedCornerImageRect:(CGRect)rect width:(float)cw {
	
	int h = rect.size.height;
	int w = rect.size.width;
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
	
	CGContextBeginPath(context);
	CGRect imgRect = rect;
	imgRect.origin.x = 0;
	imgRect.origin.y = 0;
	
	addRoundedRectToPath(context, imgRect, cw, cw);
	CGContextClosePath(context);
	CGContextClip(context);
	UIColor *c = [UIColor blackColor];
	[c set];
	CGContextFillRect(context, imgRect);
	CGImageRef imageMasked = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
	UIImage *img = [UIImage imageWithCGImage:imageMasked];
	
	CGImageRelease(imageMasked);
	CGColorSpaceRelease(colorSpace);
	return img;
}


+ (UIImage *)imageWithRoundedCorners:(UIImage *)inputImage cornerHeight:(float)ch cornerWidth:(float)cw {
	
	int h = inputImage.size.height;
	int w = inputImage.size.width;
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
	
	CGContextBeginPath(context);
	CGRect imgRect = CGRectMake(0, 0, w, h);
	addRoundedRectToPath(context, imgRect, cw, ch);
	CGContextClosePath(context);
	CGContextClip(context);
	CGContextDrawImage(context, imgRect, inputImage.CGImage);
	CGImageRef imageMasked = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
	
	float shadowOffsetY = 1;
	CGContextRef context2 = CGBitmapContextCreate(NULL, w + 2, h + shadowOffsetY + 1, 8, 4 * (w + 2), colorSpace, kCGImageAlphaPremultipliedFirst);
	CGColorRef shadowColor = [[UIColor darkGrayColor] CGColor];
	CGContextSetShadowWithColor(context2, CGSizeMake(0, -shadowOffsetY), 2, shadowColor);
	imgRect = CGRectMake(1, shadowOffsetY + 1, w, h);
	CGContextDrawImage(context2, imgRect, imageMasked);
	CGImageRef imageShadowed = CGBitmapContextCreateImage(context2);
	
	UIImage *img = [UIImage imageWithCGImage:imageShadowed];
	
	CGContextRelease(context2);
	CGColorSpaceRelease(colorSpace);
	CGImageRelease(imageShadowed);
	CGImageRelease(imageMasked);
	
	return img;
}

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize ratio:(double)r offset:(CGPoint)offset{
	int h = newSize.height;
	int w = newSize.width;
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
	
	CGContextScaleCTM(context, r, r);
	
	CGRect rect = CGRectMake(offset.x, offset.y, image.size.width, image.size.height);
	CGContextDrawImage(context, rect, image.CGImage);
	CGImageRef imageTrimmed = CGBitmapContextCreateImage(context);
	
	
	UIImage *img = [UIImage imageWithCGImage:imageTrimmed];
	
	CGColorSpaceRelease(colorSpace);
	CGImageRelease(imageTrimmed);
	CGContextRelease(context);
	
	return img;
}

#pragma mark -
#pragma mark TakeScreenShot
+ (UIImage *)imageWithUIView:(UIView *)view
{
    CGSize imageSize = [view bounds].size;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    else
        UIGraphicsBeginImageContext(imageSize);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // -renderInContext: renders in the coordinate space of the layer,
    // so we must first apply the layer's geometry to the graphics context
    CGContextSaveGState(context);
    // Center the context around the window's anchor point
    CGContextTranslateCTM(context, CGRectGetMidX(view.bounds), CGRectGetMidY(view.bounds));
    // Apply the window's transform about the anchor point
    CGContextConcatCTM(context, [view transform]);
    // Offset by the portion of the bounds left of and above the anchor point
    CGContextTranslateCTM(context,
                          -[view bounds].size.width * [[view layer] anchorPoint].x,
                          -[view bounds].size.height * [[view layer] anchorPoint].y);
    
    // Render the layer hierarchy to the current context
    [[view layer] renderInContext:context];
    
    // Restore the context
    CGContextRestoreGState(context);
    
    // Retrieve the screenshot image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

@implementation UIColor (WebHexColor)

+ (UIColor*)convertWebRGBColor:(NSString*)webHexColor {
	unsigned hexInt = 0;
	NSScanner *scanner = [NSScanner scannerWithString:[webHexColor stringByReplacingOccurrencesOfString:@"#" withString:@""]];
	[scanner scanHexInt:&hexInt];
	return [UIColor colorWithRed:((hexInt>>16)&0xFF)/255.0 green:((hexInt>>8)&0xFF)/255.0 blue:((hexInt)&0xFF)/255.0 alpha:1];
}
@end


#pragma mark -
#pragma mark fontSizeAutoSelect
@implementation UIFont (FontSizeAutoSelect)
+ (UIFont *)fontSizeAutoSelect:(NSString *)string
					   useFont:(UIFont *)font
				   maxFontSize:(CGFloat)max
				  miniFontSize:(CGFloat)min
			 constrainedToSize:(CGSize)size
				 lineBreakMode:(UILineBreakMode)lineBreakMode {
	// auto select a fixed font size
	font = [UIFont fontWithName:font.fontName size:max];
	CGSize bigSize = CGSizeMake(size.width, size.height + 999.0);
	for (CGFloat f = max; f >= min; f -= 0.25) {
		font = [UIFont fontWithName:font.fontName size:f];
		CGSize allStringSize = [string sizeWithFont:font constrainedToSize:bigSize lineBreakMode:lineBreakMode];
		if (allStringSize.height <= size.height) {
			break;
		}
	}
	return font;
}
@end

#ifdef NS_BLOCKS_AVAILABLE
@implementation NSString (UnitConverter)
+ (NSString *)stringByConvertUnsignedLongLongNumber:(unsigned long long)number withBlock:(NSString *(^)(NSArray *array))block withBehaviors:(NSArray *)behaviors {
    NSUInteger count = [behaviors count];
    NSMutableArray *mutableBehaviors = [behaviors mutableCopy];
    for (NSUInteger i = count - 2; i < NSUIntegerMax; i--) {
        [mutableBehaviors replaceObjectAtIndex:i
                                    withObject:[NSNumber numberWithFloat:
                                                [[mutableBehaviors objectAtIndex:i + 1] unsignedLongLongValue] * [[mutableBehaviors objectAtIndex:i] unsignedLongLongValue]]];
    }
    
    unsigned long long mod = number;
    NSMutableArray *array = [NSMutableArray array];
    for (NSUInteger i = 0; i < count; i++) {
        unsigned long long currentUnit = [[mutableBehaviors objectAtIndex:i] unsignedLongLongValue];
        float currentUnitNumber = (float)mod / currentUnit;
        [array addObject:[NSNumber numberWithFloat:currentUnitNumber]];
        mod = mod % currentUnit;
    }
    [array addObject:[NSNumber numberWithUnsignedLongLong:mod]];
    return block(array);
}
@end
#endif


@implementation NSString (MD5)

- (NSString *)md5 {
	const char *cStr = [self UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	
	CC_MD5(cStr, strlen(cStr), result);
	
	NSMutableString *resultStr = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
	for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
		[resultStr appendFormat:@"%02x", result[i]];
	}
	return resultStr;
}

@end

@implementation NSDate (relative)

- (NSString *)relativeDescription {
	NSDate * now = [NSDate date];
	int diff = (int)([now timeIntervalSince1970] - [self timeIntervalSince1970]);
	if (diff < 60) {
		if (diff < 10) {
			return @"just now";
		} else {
			return [NSString stringWithFormat:@"%d seconds ago", diff];
		}
	} else {
		if ((diff > 60) && (diff < 3600)) {
			int minutes = diff / 60;
			return [NSString stringWithFormat:@"%d minutes ago", minutes];
		} else {
			if ((diff > 3600) && (diff < 86400)) {
				int hours = diff / 3600;
				int minutes = (diff % 3600) / 60;
				if (hours > 1) {
					return [NSString stringWithFormat:@"%d hours %d minutes ago", hours, minutes];
				} else {
					return [NSString stringWithFormat:@"%d hour %d minutes ago", hours, minutes];
				}
			} else {
				if ((diff > 86400) && (diff < 2678400)) {
					int days = diff / 86400;
					int hours = (diff % 86400) / 3600;
					if (days > 1) {
						if (hours > 1) {
							return [NSString stringWithFormat:@"%d days %d hours ago", days, hours];
						} else {
							return [NSString stringWithFormat:@"%d days %d hour ago", days, hours];
						}
					} else {
						if (hours > 1) {
							return [NSString stringWithFormat:@"%d day %d hours ago", days, hours];
						} else {
							return [NSString stringWithFormat:@"%d day %d hour ago", days, hours];
						}
					}
				} else {
					NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
					[formatter setDateStyle:NSDateFormatterMediumStyle];
					NSString * formatted = [formatter stringFromDate:self];
					return formatted;
				}
			}
		}
	}
}

-(NSString *)timeAgo {
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    if (deltaMinutes < 60) {
        return [NSString stringWithFormat:@"%dm", (int)deltaMinutes];
    } else if (deltaMinutes < (24 * 60)) {
        return [NSString stringWithFormat:@"%dh", (int)(deltaMinutes / 60)];
    } else {
        return [NSString stringWithFormat:@"%dd", (int)(deltaMinutes / 60 / 24)];
    }
}

- (NSString *)timeAgoInChinese {
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    if (deltaMinutes < 60) {
        return [NSString stringWithFormat:@"%d 分钟前", (int)deltaMinutes];
    } else if (deltaMinutes < (24 * 60)) {
        return [NSString stringWithFormat:@"%d 小时前", (int)(deltaMinutes / 60)];
    } else if (deltaMinutes < (24 * 60 * 30)) {
        return [NSString stringWithFormat:@"%d 天前", (int)(deltaMinutes / 60 / 24)];
    } else {
        return [NSString stringWithFormat:@"%d 个月前", (int)(deltaMinutes / 60 / 24 / 30)];
    }
}

@end

@implementation NSObject (RTPerform)

- (void)performSelectorOnMainThread:(SEL)aSelector withObject:(id)arg1 withObject:(id)arg2 waitUntilDone:(BOOL)wait {
    [self performSelectorOnMainThread:aSelector waitUntilDone:wait withObjects:arg1, arg2, nil];
}

- (void)performSelectorInBackground:(SEL)aSelector withObject:(id)arg1 withObject:(id)arg2 {
    [self performSelectorInBackground:aSelector withObjects:arg1, arg2, nil];
}

- (void)performSelectorOnMainThread:(SEL)aSelector waitUntilDone:(BOOL)wait withObjects:(id)arg, ... {
    if ([self respondsToSelector:aSelector]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:aSelector]];
        [invocation setTarget:self];
        [invocation setSelector:aSelector];
        
        va_list args;
        va_start(args, arg);
        
        NSInteger index = 2;
        for (id object = arg; object != nil; object = va_arg(args, id), index++) {
            [invocation setArgument:&object atIndex:index];
        }
        [invocation retainArguments];
        va_end(args);
        
		[invocation performSelectorOnMainThread:@selector(invoke) withObject:nil waitUntilDone:wait];
    }
}

- (void)performSelectorInBackground:(SEL)aSelector withObjects:(id)arg, ... {
    if ([self respondsToSelector:aSelector]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:aSelector]];
        [invocation setTarget:self];
        [invocation setSelector:aSelector];
        
        va_list args;
        va_start(args, arg);
        
        NSInteger index = 2;
        for (id object = arg; object != nil; object = va_arg(args, id), index++) {
            [invocation setArgument:&object atIndex:index];
        }
        [invocation retainArguments];
        va_end(args);
        
		[invocation performSelectorInBackground:@selector(invoke) withObject:nil];
    }
}

- (void)performSelector:(SEL)aSelector afterDelay:(NSTimeInterval)delay withObject:(id)arg,... {
    if ([self respondsToSelector:aSelector]) {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:aSelector]];
        [invocation setTarget:self];
        [invocation setSelector:aSelector];
        
        va_list args;
        va_start(args, arg);
        
        NSInteger index = 2;
        for (id object = arg; object != nil; object = va_arg(args, id), index++) {
            [invocation setArgument:&object atIndex:index];
        }
        [invocation retainArguments];
        va_end(args);
        
		[invocation performSelector:@selector(invoke) withObject:nil afterDelay:delay];
    }
    
}

+ (NSInvocation*)invocationWithTarget:(id)target
                             selector:(SEL)aSelector
                      retainArguments:(BOOL)retainArguments
                            arguments:(void *)args, ... {
    va_list ap;
    va_start(ap, args);
    NSMethodSignature* signature = [target methodSignatureForSelector:aSelector];
    NSInvocation* invocation = [NSInvocation invocationWithMethodSignature:signature];
    if (retainArguments) {
        [invocation retainArguments];
    }
    [invocation setTarget:target];
    [invocation setSelector:aSelector];
    for (int index = 2; index < [signature numberOfArguments]; index++) {
        [invocation setArgument:args atIndex:index];
        args = va_arg(ap, void *);
    }
    va_end(ap);
    return invocation;
}

@end

@implementation NSArray (Shuffle)

- (NSMutableArray *)shuffle {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[self count]];
    
    NSMutableArray *copy = [self mutableCopy];
    while ([copy count] > 0) {
        int index = arc4random() % [copy count];
        id obj = [copy objectAtIndex:index];
        [array addObject:obj];
        [copy removeObjectAtIndex:index];
    }
    
    return array;
}

@end


@interface UIDevice(Private)

- (NSString *) macaddress;

@end

@implementation UIDevice (IdentifierAddition)

////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private Methods

// Return the local MAC addy
// Courtesy of FreeBSD hackers email list
// Accidentally munged during previous update. Fixed thanks to erica sadun & mlamb.
- (NSString *)macaddress {
    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        free(buf);
        printf("Error: sysctl, take 2");
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public Methods

- (NSString *)uniqueGlobalDeviceIdentifier {
    NSString *macaddress = [[UIDevice currentDevice] macaddress];
    NSString *uniqueIdentifier = [macaddress md5];
    
    return uniqueIdentifier;
}

- (NSString *)uniqueDeviceIdentifier {
    NSString *macaddress = [[UIDevice currentDevice] macaddress];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    NSString *stringToHash = [NSString stringWithFormat:@"%@-%@", macaddress, bundleIdentifier];
    NSString *uniqueIdentifier = [stringToHash md5];
    
    return uniqueIdentifier;
}

@end