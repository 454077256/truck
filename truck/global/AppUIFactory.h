//
//  AppUIFactory.h
//  Lohas
//
//  Created by 洪湃 on 15-1-16.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUIFactory : NSObject

+(id)open:(NSString *)className frame:(CGRect)frame data:(NSDictionary *)config parent:(UIView *)parent action:(ActionType)action;

@end
