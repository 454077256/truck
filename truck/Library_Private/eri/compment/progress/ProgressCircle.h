//
//  ProgressCircle.h
//  Lohas
//
//  Created by 洪湃 on 15-3-7.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface ProgressCircle : EEView

@property(nonatomic)float progress;

-(void)refreshProgress:(int)cIndex total:(int)total;

@end
