//
//  EETools_System.h
//  Zume100
//
//  Created by 洪湃 on 14-5-24.
//
//

#import <Foundation/Foundation.h>

@interface EETools : NSObject


+ (long long ) freeDiskSpaceInBytes;
+(float)getTotalDiskSpaceInBytes ;



//得到资源文件夹里面的路径
+(NSString *)tools_getFilePathInAppBundle_filename:(NSString *)filname ext:(NSString *)type;
+(NSString *)tools_getFileLastestUpdateTime:(NSString *)filePath;
+(void)ee_deleteAllFlies_inOneFolder:(NSString *)folder;
+(long long)ee_fileSizeForPath:(NSString*) filePath;
+(long long)ee_fileSizeForDir:(NSString*)path;

+(CGSize)ee_getTextSize:(int)textWidth fontName:(NSString *)fontName fontSize:(CGFloat)fontSize text:(NSString *)text;

@end
