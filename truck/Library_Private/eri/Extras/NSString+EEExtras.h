//
//  NSString+EEExtras.h
//  Libaray
//
//  Created by pai hong on 12-10-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EEExtras)

- (NSString *)ee_URLEncodedString;
- (NSString *)ee_URLDecodedString;

-(NSString *)ee_stringByDecodingXMLEntities;
-(NSString *)ee_trim;

//十六进制 to 字符串
+ (NSString *)ee_stringFromHexString:(NSString *)hexString;
//字符串 to 十六进制
+ (NSString *)ee_hexStringFromString:(NSString *)string;


//用正则表达式替换字符
-(NSString *)ee_replaceStringByPattern:(NSString *)rege replacestr:(NSString *)letter ;

@end
