//
//  NSArray+Extras.h
//  epp
//
//  Created by 洪湃 on 14-9-26.
//  Copyright (c) 2014年 qq:454077256 phone:18621592830. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (external)


-(BOOL)isContainerString:(NSString *)searchStr;
-(int)getSerchstringIndex:(NSString *)searchStr;

@end
