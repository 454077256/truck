//
//  AppUIFactory.m
//  Lohas
//
//  Created by 洪湃 on 15-1-16.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "AppUIFactory.h"

@implementation AppUIFactory


+(id)open:(NSString *)className frame:(CGRect)frame data:(NSDictionary *)config parent:(UIView *)parent action:(ActionType)action{
    Class class = NSClassFromString(className);
    id ins = [[class alloc] initWithFrame:frame];
    ((EEView *)ins).eeData = config;
    [ins initView];
    [ins addTransition:action];
    [parent addSubview:ins];
    
    return ins;
}

@end
