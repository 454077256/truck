//
//  NSObject+Extras.h
//  Zume100
//
//  Created by hong pai on 14-3-29.
//
//

#import <Foundation/Foundation.h>

@interface NSObject(Extras )

-(void)performBlock:(void(^)(void))block afterDelay:(NSTimeInterval)delay;

@end
