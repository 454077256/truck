//
//  UserMessage.m
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "UserMessage.h"

@implementation UserMessage

-(void)initView{
    [self setBackgroundColor:CHex(0xEFEFEF)];
    //导航条
    EETopBar *topbar = [EETopBar instanceBy_normalLeft:nil
                                            selectLeft:nil
                                            leftAction:nil
                                          leftDelegate:nil
                                           normalRight:nil
                                           selectRight:nil
                                           rightAction:nil
                                         rightDelegate:self
                                                center:@"消息中心" isIOS7Lagger:YES];
    [topbar setBackgroundColor:CHex(0x1BA2E7)];
    [self addSubview:topbar];
    
    [self addSubview:[Global createLabel:R(0, SS(100), App_Size.width, SS(100)) label:@"页面开发中，敬请期待" lines:0 fontSize:S(14) fontName:nil textcolor:CGray align:(NSTextAlignmentLeft)]];
    
}


@end
