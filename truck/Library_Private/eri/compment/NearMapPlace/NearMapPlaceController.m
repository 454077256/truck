//
//  NearMapPlaceController.m
//  Lohas
//
//  Created by 洪湃 on 16/4/16.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "NearMapPlaceController.h"
#import "LocationCell.h"

@interface NearMapPlaceController ()

@end

@implementation NearMapPlaceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    
    //导航条
    
    EETopBar * topbar = [EETopBar instanceBy_normalLeft:@"assets/btn_return.png"
                                  selectLeft:@"assets/btn_returnH.png"
                                  leftAction:@selector(clickReturnButton:)
                                leftDelegate:self
                                 normalRight:@"确定"
                                 selectRight:@"确定"
                                 rightAction:@selector(clickSubmit:)
                               rightDelegate:self
                                      center:@"选择地点" isIOS7Lagger:YES];
    [self.view addSubview:topbar];
    [topbar setBackgroundColor:RGB(248, 248, 248)];
    
    //配置用户Key
    [MAMapServices sharedServices].apiKey = @"c8e6333792849de0aaa70532b100a002";
    [AMapSearchServices sharedServices].apiKey = @"c8e6333792849de0aaa70532b100a002";
    
    //初始化检索对象
    _search = [[AMapSearchAPI alloc] init];
    _search.delegate = self;
    
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, nextY(topbar, 0), App_Size.width, SS(440))];
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
//    _mapView.centerCoordinate = _mapView.userLocation.location.coordinate;
    [_mapView setUserTrackingMode: MAUserTrackingModeNone animated:YES];
    
    [self.view addSubview:_mapView];
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, nextY(_mapView, 0), App_Size.width, App_Size.height-_mapView.y-_mapView.height)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[LocationCell class] forCellReuseIdentifier:@"LocationCell"];

}

-(void)startSearch:(CLLocationCoordinate2D)coord{
    
    //构造AMapPOIAroundSearchRequest对象，设置周边请求参数
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location = [AMapGeoPoint locationWithLatitude:coord.latitude longitude:coord.longitude];
//    request.keywords = @"上海";
    // types属性表示限定搜索POI的类别，默认为：餐饮服务|商务住宅|生活服务
    // POI的类型共分为20种大类别，分别为：
    // 汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|
    // 医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|
    // 交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施
//    request.types = @"餐饮服务|生活服务";
    request.sortrule = 0;
    request.requireExtension = YES;
    
    //发起周边搜索
    [_search AMapPOIAroundSearch: request];
}
//实现POI搜索对应的回调函数
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    trace(@"response.pois.count %d",response.pois.count);
    
    self.dataSource = response.pois;
    
    self.tableView.delegate = self;
    [self.tableView reloadData];
//    if(response.pois.count == 0)
//    {
//        return;
//    }
//
//    //通过 AMapPOISearchResponse 对象处理搜索结果
//    NSString *strCount = [NSString stringWithFormat:@"count: %d",response.count];
//    NSString *strSuggestion = [NSString stringWithFormat:@"Suggestion: %@", response.suggestion];
//    NSString *strPoi = @"";
//    for (AMapPOI *p in response.pois) {
//        strPoi = [NSString stringWithFormat:@"%@\nPOI: %@", strPoi, p.description];
//        trace(@"%@ %@ %@",p.name,p.type,p.uid);
//    }
//    NSString *result = [NSString stringWithFormat:@"%@ \n %@ \n %@", strCount, strSuggestion, strPoi];
//    NSLog(@"Place: %@", result);
}

-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        trace(@"-------updatingLocation-------");
        if (pointAnnotation==nil) {
            pointAnnotation = [[MAPointAnnotation alloc] init];
            pointAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
//            pointAnnotation.title = [NSString stringWithFormat:@"%d",arc4random()%100];
//            pointAnnotation.subtitle = @"阜通东大街6号";
            [_mapView addAnnotation:pointAnnotation];
        }else{
            pointAnnotation.title = [NSString stringWithFormat:@"%d",arc4random()%100];
//            pointAnnotation.subtitle = @"阜通东大街6号";
        }
        
        if (!isUserMoveMap) {//一旦用户移动的map，则之后不会自动移动
            _mapView.centerCoordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        }
        
        if (fabs(lastCoordinate2d.latitude-pointAnnotation.coordinate.latitude)>0.5 || fabs(lastCoordinate2d.longitude-pointAnnotation.coordinate.longitude)>0.5) {
            [self startSearch:userLocation.coordinate ];
        }
        
        lastCoordinate2d = pointAnnotation.coordinate;
    }
}
//- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation
//{
//    if ([annotation isKindOfClass:[MAPointAnnotation class]])
//    {
//        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
//        MAPinAnnotationView*annotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
//        if (annotationView == nil)
//        {
//            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
//        }
//        annotationView.canShowCallout= YES;       //设置气泡可以弹出，默认为NO
//        annotationView.animatesDrop = YES;        //设置标注动画显示，默认为NO
//        annotationView.draggable = YES;        //设置标注可以拖动，默认为NO
//        annotationView.pinColor = MAPinAnnotationColorPurple;
//        return annotationView;
//    }
//    return nil;
//}

- (void)mapView:(MAMapView *)mapView mapWillMoveByUser:(BOOL)wasUserAction{
    if (wasUserAction) {
        isUserMoveMap = YES;
    }
}

#pragma mark --tableview delegate 
#pragma mark - TableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return SS(100);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return self.dataSource.count;
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    LocationCell *cell =[tableView dequeueReusableCellWithIdentifier:@"LocationCell" ];
    
    AMapPOI *onePOI=[self.dataSource objectAtIndex:indexPath.row];
    
    [cell refreshTitle:onePOI.name detail:onePOI.address];
    
    
    if (self.currentSelectLocationIndex==indexPath.row)
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType=UITableViewCellAccessoryNone;
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
//    BMKPoiInfo *model=[self.dataSource objectAtIndex:indexPath.row];
//    
//    BMKMapStatus *mapStatus =[self.mapView getMapStatus];
//    
//    mapStatus.targetGeoPt=model.pt;
//    
//    [self.mapView setMapStatus:mapStatus withAnimation:YES];
    
    self.currentSelectLocationIndex=indexPath.row;
    
    [self.tableView reloadData];
    
}


-(void)clickReturnButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        _mapView.delegate = nil;
        [_mapView removeFromSuperview];
        _search = nil;
        self.tableView = nil;
        self.dataSource = nil;
    }];
}

-(void)clickSubmit:(id)sender{
    AMapPOI *onePOI=[self.dataSource objectAtIndex:self.currentSelectLocationIndex];
    
    NSMutableDictionary *pdata = [NSMutableDictionary dictionary];
    
    [Global setObject:pdata value:onePOI.name       key:@"name"];
    [Global setObject:pdata value:onePOI.address    key:@"address"];
    [Global setObject:pdata value:onePOI.tel        key:@"tel"];
    
    NoticePost(KNoticeName_MapSelectPlace, pdata);
    
    
    [self clickReturnButton:nil];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    track();
}
@end
