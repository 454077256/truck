//
//  EETable.m
//  OuRuiDA
//
//  Created by 洪湃 on 15/12/8.
//  Copyright © 2015年 洪湃 454077256. All rights reserved.
//

#import "EETableAuto.h"

@implementation EETableAuto


-(void)initTable{
    curPageNum = 1;
    dataTableView = [[UITableView alloc] initWithFrame:varTableFrame];
    
    dataTableView.delegate = self;
    dataTableView.dataSource = self;
    dataTableView.showsVerticalScrollIndicator = NO;
    dataTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    if ([dataTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [dataTableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    if ([dataTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [dataTableView setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }

    
    [self addSubview:dataTableView];

    [dataTableView setTableFooterView:[Global createViewEmpty]];
//    [dataTableView setBackgroundColor:CRed];
    
    //创建下拉刷新控件
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-dataTableView.height, dataTableView.width, dataTableView.height) dateId:101];
    _refreshHeaderView.delegate = self;
    [dataTableView addSubview:_refreshHeaderView];
    [_refreshHeaderView setBackgroundColor:RGB(239, 237, 233)];
    
}

-(void)initVar{
    varTableFrame = self.bounds;
    varTableCellHeight = 38;
}

-(void)loadData:(NSDictionary *)data size:(CGSize)size{
    
}

-(void)eeTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark ---table delegate ----

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return varTableCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.eeList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *tabelCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    [tabelCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    id cell = [[varCellCLASS alloc] init];
    
    [cell loadData:[self.eeList objectAtIndex:indexPath.row] size:CGSizeMake(tableView.width, varTableCellHeight) indexPath:indexPath];
//    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self eeTableView:tableView didSelectRowAtIndexPath:indexPath];
}



#pragma mark --- 下拉刷新 --

-(void)setFooterView{
    //    UIEdgeInsets test = self.aoView.contentInset;
    // if the footerView is nil, then create it, reset the position of the footer
    CGFloat height = MAX(dataTableView.contentSize.height, dataTableView.frame.size.height);
    if (_refreshFooterView && [_refreshFooterView superview])
    {
        // reset position
        _refreshFooterView.frame = CGRectMake(0.0f,
                                              height,
                                              dataTableView.frame.size.width,
                                              self.bounds.size.height);
    }else
    {
        // create the footerView
        _refreshFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:
                              CGRectMake(0.0f, height,
                                         dataTableView.frame.size.width, self.bounds.size.height) dateId:100];
        _refreshFooterView.delegate = self;
        [dataTableView addSubview:_refreshFooterView];
        [_refreshFooterView setBackgroundColor:RGB(239, 237, 233)];
    }
    
    if (_refreshFooterView)
    {
        [_refreshFooterView refreshLastUpdatedDate];
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods


-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
    track();
    
    _reloading = YES;
    if (aRefreshPos == EGORefreshHeader)
    {
        curPageNum = 1;
        isNewFreshing = YES;
        
    }else if(aRefreshPos == EGORefreshFooter)
    {
        curPageNum ++;
    }
    [self performSelector:@selector(eeTableRequestList) withObject:nil afterDelay:1];
    
}

- (void)doneLoadingTableViewData{
    
    track();
    //  model should call this when its done loading
    _reloading = NO;
    
    if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:dataTableView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:dataTableView];
        [self setFooterView];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (_refreshHeaderView)
    {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
    
    if (_refreshFooterView)
    {
        [_refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (_refreshHeaderView)
    {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    
    if (_refreshFooterView)
    {
        [_refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark  数据请求
-(void)eeTableRequestList{//call children
}


-(void)eeTableRequestListSuccess:(NSArray *)postList{//call children
    //得到网络的数据array
    if (postList==nil){
        traceError(@"获取失败 %@",postList);
        [self doneLoadingTableViewData];
        [self setFooterView];
        return;
    }
    
    if(curPageNum==1){
        self.eeList = [NSMutableArray arrayWithArray:postList];
        //刷新表格
    }else{
        [((NSMutableArray *)self.eeList) addObjectsFromArray:postList];
    }
    
    [dataTableView reloadData];
    
    [self doneLoadingTableViewData];
    [self setFooterView];
    
    if ([self.eeList count]<=0) {
        tipLabel = [Global createLabel:self.bounds label:@"暂无内容" lines:0 fontSize:S(16) fontName:nil textcolor:CGray align:(NSTextAlignmentCenter)];
        [self addSubview:tipLabel];
    }else{
        [tipLabel removeFromSuperview];
        tipLabel = nil;
    }
//    
//    //保存数据
//    [self.newsDataList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        [[self.newsDataList objectAtIndex:idx] setObject:@"" forKey:@"content"];
//    }];
//    [self.newsDataList writeToFile:EE_TodayPlist atomically:YES];
}

//-(void)getWebDataFailed_today:(ASIHTTPRequest *)request{
//    [self doneLoadingTableViewData];
//}



#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

-(BOOL)egoRefreshTableDataSourceIsLoading:(UIView *)view{
    return _reloading; // should return if data source model is reloading
}

-(NSDate *)egoRefreshTableDataSourceLastUpdated:(UIView *)view{
    return [NSDate date];
}

-(void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos{
    [self beginToReloadData:aRefreshPos];
}


@end
