//
//  EETools_System.m
//  Zume100
//
//  Created by 洪湃 on 14-5-24.
//
//

#import "EETools.h"
#include <sys/param.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <dirent.h>

@implementation EETools

#pragma mark ---- 系统空间 -----
+ (long long ) freeDiskSpaceInBytes{
    struct statfs buf;
    long long freespace = -1;
    if(statfs("/var", &buf) >= 0){
        freespace = (long long)(buf.f_bsize * buf.f_bfree);
    }
    return freespace/1024/1024;
    //return [NSString stringWithFormat:@"手机剩余存储空间为：%qi MB" ,freespace/1024/1024];
}
+(float)getTotalDiskSpaceInBytes {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    struct statfs tStats;
    statfs([[paths lastObject] cString], &tStats);
    float totalSpace = (float)(tStats.f_blocks * tStats.f_bsize);

    return totalSpace;
}





#pragma mark ----- 文件操作 -----

+(NSString *)tools_getFilePathInAppBundle_filename:(NSString *)filname ext:(NSString *)type{
    NSString *path = [[NSBundle mainBundle] pathForResource:filname ofType:type];
    return path;
}

// --得到文件的最新修改日期
+(NSString *)tools_getFileLastestUpdateTime:(NSString *)filePath{
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    NSDate *createDate = [NSDate date];
    
    [fileURL getResourceValue:&createDate forKey:@"NSURLAttributeModificationDateKey"error:nil];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *result = [df stringFromDate:createDate];
    
    return  result;
}

+(void)ee_deleteAllFlies_inOneFolder:(NSString *)folder{
    //NSHomeDirectory()
    NSArray *allFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folder error:nil];
    int len = [allFiles count];
    
    for (int i=0; i<len; i++) {
        NSString *filename =  [allFiles objectAtIndex:i];
        NSString *completePath = [folder stringByAppendingPathComponent:filename];
        trace(@"fliepath %@",completePath);
        
        BOOL result = [[NSFileManager defaultManager] removeItemAtPath:completePath error:nil];
        trace(@"---------------del file in Inbox %@",result?@"success":@"failed");
    }
}

+ (long long) ee_fileSizeForPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}

//完全使用unix c函数
+(long long)ee_fileSizeForDir:(NSString*)path{
    return  [EETools folderSize:[path cStringUsingEncoding:NSUTF8StringEncoding]];
}
+ (long long)folderSize:(const char *)folderPath {
    long long folderSize = 0;
    DIR* dir = opendir(folderPath);
    if (dir == NULL) {
        return 0;
    }
    struct dirent* child;
    while ((child = readdir(dir)) != NULL) {
        if (child->d_type == DT_DIR
            && (child->d_name[0] == '.' && child->d_name[1] == 0)) {
            continue;
        }
        
        if (child->d_type == DT_DIR
            && (child->d_name[0] == '.' && child->d_name[1] == '.' && child->d_name[2] == 0)) {
            continue;
        }
        
        int folderPathLength = strlen(folderPath);
        char childPath[1024];
        stpcpy(childPath, folderPath);
        if (folderPath[folderPathLength - 1] != '/'){
            childPath[folderPathLength] = '/';
            folderPathLength++;
        }
        
        stpcpy(childPath + folderPathLength, child->d_name);
        childPath[folderPathLength + child->d_namlen] = 0;
        if (child->d_type == DT_DIR){
            folderSize += [self folderSize:childPath];
            struct stat st;
            if (lstat(childPath, &st) == 0) {
                folderSize += st.st_size;
            }
        } else if (child->d_type == DT_REG || child->d_type == DT_LNK){
            struct stat st;
            if (lstat(childPath, &st) == 0) {
                folderSize += st.st_size;
            }
        }
    }
    
    return folderSize;
}


#pragma mark -- 文字操作 --
+(CGSize)ee_getTextSize:(int)textWidth fontName:(NSString *)fontName fontSize:(CGFloat)fontSize text:(NSString *)text{
    EELabel *lbl = [[EELabel alloc] init];

    lbl.text = text;
    lbl.numberOfLines = 0;
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    
    lbl.font = [UIFont fontWithName:fontName size:fontSize];
    lbl.font = [UIFont systemFontOfSize:fontSize];
    
    lbl.adjustsFontSizeToFitWidth = TRUE;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:(IS_IPAD?10:4)];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    lbl.attributedText = attributedString;
    [lbl sizeToFit];
    
    CGSize resultSize1 = [lbl sizeThatFits:CGSizeMake(textWidth, 0)];
    
    return resultSize1;
}




@end
