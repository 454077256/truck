//
//  NSObject+Extras.m
//  Zume100
//
//  Created by hong pai on 14-3-29.
//
//

#import "NSObject+Extras.h"

@implementation NSObject(Extras)

//block形式  延迟执行
-(void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay{
    block = [block copy];
    [self performSelector:@selector(runPerformBlock:) withObject:block afterDelay:delay];
}

-(void)runPerformBlock:(void(^)(void))block{
    block();
}

@end
