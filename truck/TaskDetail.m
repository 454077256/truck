
//
//  TaskDetail.m
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "TaskDetail.h"

@implementation TaskDetail


-(void)initView{
    trace(@"%@",self.eeData);
    [self setBackgroundColor:CWhite];
    //导航条
    EETopBar *topbar = [EETopBar instanceBy_normalLeft:@"assets/return.png"
                                            selectLeft:nil
                                            leftAction:@selector(clickReturn:)
                                          leftDelegate:self
                                           normalRight:nil
                                           selectRight:nil
                                           rightAction:nil
                                         rightDelegate:self
                                                center:@"更新任务" isIOS7Lagger:YES];
    [topbar setBackgroundColor:CHex(0x1BA2E7)];
    [self addSubview:topbar];
    
    contentDetail = [Global createScroll:R(0, nextY(topbar, 0), App_Size.width, App_Size.height-topbar.height) delegate:self content:nil pageEnable:NO zoom:NO];
    [self addSubview:contentDetail];
    [contentDetail setBackgroundColor:CRed];
    [contentDetail setBackgroundColor:CHex(0xEFEFEF)];
    contentDetail.delaysContentTouches = NO;
    [self createUI];
}


-(void)createUI{
    UIColor *tColor = CHex(0x565656);
    
    UIColor *grayLine = CHex(0xEFEFEF);
    EEView *con1 = [Global createViewByFrame:R(0, 0, self.width, SS(120)) color:CWhite];
    
    processBtn1 = [Global createBtn:R(0, 0, SS(62), SS(62)) normal:@"assets/dfache.png" down:@"assets/du46.png" target:nil action:nil];
    processBtn2 = [Global createBtn:R(0, 0, SS(62), SS(62)) normal:@"assets/dtixiang2.png" down:@"assets/du46.png" target:nil action:nil];
    processBtn3 = [Global createBtn:R(0, 0, SS(62), SS(62)) normal:@"assets/ddaochang.png" down:@"assets/du46.png" target:nil action:nil];
    processBtn4 = [Global createBtn:R(0, 0, SS(62), SS(62)) normal:@"assets/dhuidan.png" down:@"assets/du46.png" target:nil action:nil];
    processBtn5 = [Global createBtn:R(0, 0, SS(62), SS(62)) normal:@"assets/dfangang.png" down:@"assets/du46.png" target:nil action:nil];
    
    
    processBtn1.center = ccp(SS(70), SS(42));
    processBtn2.center = ccp(SS(190), SS(42));
    processBtn3.center = ccp(SS(310), SS(42));
    processBtn4.center = ccp(SS(430), SS(42));
    processBtn5.center = ccp(SS(550), SS(42));
    
    [con1 addSubview:processBtn1];
    [con1 addSubview:processBtn2];
    [con1 addSubview:processBtn3];
    [con1 addSubview:processBtn4];
    [con1 addSubview:processBtn5];
    processBtn1.userInteractionEnabled = NO;
    processBtn2.userInteractionEnabled = NO;
    processBtn3.userInteractionEnabled = NO;
    processBtn4.userInteractionEnabled = NO;
    processBtn5.userInteractionEnabled = NO;
    
    CGRect cRect = R(0, 0, SS(60), SS(48));
    EELabel *lab1 = [Global createLabel:cRect label:@"发车" lines:0 fontSize:S(12) fontName:nil textcolor:CHex(0x555555) align:(NSTextAlignmentCenter)];
    EELabel *lab2 = [Global createLabel:cRect label:@"提箱" lines:0 fontSize:S(12) fontName:nil textcolor:CHex(0x555555) align:(NSTextAlignmentCenter)];
    EELabel *lab3 = [Global createLabel:cRect label:@"到厂" lines:0 fontSize:S(12) fontName:nil textcolor:CHex(0x555555) align:(NSTextAlignmentCenter)];
    EELabel *lab4 = [Global createLabel:cRect label:@"回单" lines:0 fontSize:S(12) fontName:nil textcolor:CHex(0x555555) align:(NSTextAlignmentCenter)];
    EELabel *lab5 = [Global createLabel:cRect label:@"返港" lines:0 fontSize:S(12) fontName:nil textcolor:CHex(0x555555) align:(NSTextAlignmentCenter)];
    
    lab1.center = ccp(SS(70), SS(92));
    lab2.center = ccp(SS(190), SS(92));
    lab3.center = ccp(SS(310), SS(92));
    lab4.center = ccp(SS(430), SS(92));
    lab5.center = ccp(SS(550), SS(92));
    
    [con1 addSubview:lab1];
    [con1 addSubview:lab2];
    [con1 addSubview:lab3];
    [con1 addSubview:lab4];
    [con1 addSubview:lab5];
    
    [con1 addSubview:tmpView =[Global createViewByFrame:R(S(10), processBtn4.center.y,self.width-S(20) ,S(2) ) color:CHex(0x999999)]];
    [con1 sendSubviewToBack:tmpView];
    
    [contentDetail addSubview:con1];
    
    
    NSArray *items = self.eeData[@"workOrderItems"];
    NSDictionary *item = nil;
    if ([items count]>0) {
        item = items[0];
    }
    
    //提单号
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(con1, SS(22)), self.width, SS(80)) color:CWhite]];
    [tmpView addSubview:[Global createViewByFrame:R(0,  tmpView.height-1, self.width, S(0.8)) color:grayLine]];
    [tmpView addSubview:[Global createImage:@"assets/detailico2.png" center:ccp(SS(28), SS(38))]];
    
    NSString *tidanhao = [NSString stringWithFormat:@"提单号      %@",item[@"workOrder"][@"refCde"]];
    [tmpView addSubview:[Global createLabel:R(SS(60), 0, SS(400), SS(78)) label:tidanhao lines:0 fontSize:S(14) fontName:nil textcolor:tColor align:(NSTextAlignmentLeft)]];
    
    //箱号
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(tmpView, SS(0)), self.width, SS(80)) color:CWhite]];
    [tmpView addSubview:[Global createViewByFrame:R(0,  tmpView.height-1, self.width, S(0.8)) color:grayLine]];
    [tmpView addSubview:[Global createImage:@"assets/detailico3.png" center:ccp(SS(28), SS(38))]];
    
    //还箱码头
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(tmpView, SS(12)), self.width, SS(80)) color:CWhite]];
    [tmpView addSubview:[Global createViewByFrame:R(0,  tmpView.height-1, self.width, S(0.8)) color:grayLine]];
    [tmpView addSubview:[Global createImage:@"assets/detailico4.png" center:ccp(SS(28), SS(38))]];
    
    //发货方信息
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(tmpView, SS(0)), self.width, SS(249)) color:CWhite]];
    [tmpView addSubview:[Global createViewByFrame:R(0,  tmpView.height-1, self.width, S(0.8)) color:grayLine]];
    [tmpView addSubview:[Global createImage:@"assets/detailico5.png" center:ccp(SS(28), SS(38))]];
    [tmpView addSubview:[Global createBtn:R(SS(560), SS(8), SS(48), SS(48)) normal:@"assets/icophone.png" target:self action:@selector(clickPhone:)]];
    
    //货物信息
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(tmpView, SS(0)), self.width, SS(80)) color:CWhite]];
    [tmpView addSubview:[Global createViewByFrame:R(0,  tmpView.height-1, self.width, S(0.8)) color:grayLine]];
    [tmpView addSubview:[Global createImage:@"assets/detailico6.png" center:ccp(SS(28), SS(38))]];
    
    //注意事项
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(tmpView, SS(12)), self.width, SS(80)) color:CWhite]];
    [tmpView addSubview:[Global createViewByFrame:R(0,  tmpView.height-1, self.width, S(0.8)) color:grayLine]];
    [tmpView addSubview:[Global createImage:@"assets/detailico7.png" center:ccp(SS(28), SS(38))]];
    
    //menus
    [contentDetail addSubview:tmpView = [Global createViewByFrame:R(0, nextY(tmpView, SS(12)), self.width, SS(160)) color:CWhite]];
    EEButton *btnMenu1 = [Global createBtn:R(SS(4), SS(12), SS(198), SS(64)) normal:@"assets/state1.png" target:self action:@selector(clickMenubtn1:)];
    EEButton *btnMenu2 = [Global createBtn:R(SS(216), SS(12), SS(198), SS(64)) normal:@"assets/state2.png" target:self action:@selector(clickMenubtn2:)];
    EEButton *btnMenu3 = [Global createBtn:R(SS(426), SS(12), SS(198), SS(64)) normal:@"assets/state3.png" target:self action:@selector(clickMenubtn3:)];
    EEButton *btnMenu4 = [Global createBtn:R(SS(4), SS(86), SS(198), SS(64)) normal:@"assets/state4.png" target:self action:@selector(clickMenubtn4:)];
    EEButton *btnMenu5 = [Global createBtn:R(SS(216), SS(86), SS(198), SS(64)) normal:@"assets/state5.png" target:self action:@selector(clickMenubtn5:)];
    EEButton *btnMenu6 = [Global createBtn:R(SS(426), SS(86), SS(198), SS(64)) normal:@"assets/state6.png" target:self action:@selector(clickMenubtn6:)];
    [tmpView addSubview:btnMenu1];
    [tmpView addSubview:btnMenu2];
    [tmpView addSubview:btnMenu3];
    [tmpView addSubview:btnMenu4];
    [tmpView addSubview:btnMenu5];
    [tmpView addSubview:btnMenu6];
}


-(void)clickReturn:(id)sender{
    [self eeRemoveRight];
}


-(void)clickMenubtn1:(id)sender{
    track();
}

-(void)clickMenubtn2:(id)sender{
    track();
}

-(void)clickMenubtn3:(id)sender{
    track();
}

-(void)clickMenubtn4:(id)sender{
    track();
}

-(void)clickMenubtn5:(id)sender{
    track();
}

-(void)clickMenubtn6:(id)sender{
    track();
}


-(void)clickPhone:(id)sender{
    track();
}


-(void)dealloc{
    track();
}

@end
