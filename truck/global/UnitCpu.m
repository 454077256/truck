//
//  UnitCpu.m
//  Lohas
//
//  Created by 洪湃 on 15-3-24.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//
//

#import "UIAlertView+Block.h"
#import <objc/runtime.h>


#import "UnitCpu.h"

static char key1;
static char key2;
static char key3;

@implementation UnitCpu


- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

// 用Block的方式回调，这时候会默认用self作为Delegate
- (void)start:(NSDictionary *)pConfigData  begin:(UnitBegin)blockB complete:(UnitComplete)blockC failed:(UnitFailed)blockD
{
    trace(@"pConfigData %@",pConfigData);
    self.configData = pConfigData;
    int r = 10000 + arc4random() %900000;
    self.uIndex = [NSString stringWithFormat:@"%d",r];
    [[Global sharedGlobal].unitDictionary setObject:self forKey:self.uIndex];
    
    if (blockB) {
        ////移除所有关联
        objc_removeAssociatedObjects(self);
        /**
         1 创建关联（源对象，关键字，关联的对象和一个关联策略。)
         2 关键字是一个void类型的指针。每一个关联的关键字必须是唯一的。通常都是会采用静态变量来作为关键字。
         3 关联策略表明了相关的对象是通过赋值，保留引用还是复制的方式进行关联的；关联是原子的还是非原子的。这里的关联策略和声明属性时的很类似。
         */
        objc_setAssociatedObject(self, &key1, blockB, OBJC_ASSOCIATION_COPY);
        ////设置delegate
//        self.delegate = self;
    }
    if (blockC) {
        objc_setAssociatedObject(self, &key2, blockC, OBJC_ASSOCIATION_COPY);
    }
    if (blockD) {
        objc_setAssociatedObject(self, &key3, blockD, OBJC_ASSOCIATION_COPY);
    }

    //开始准备
    [self doBegin];
    
    //正式启动
    [self fire];
}

//子类集成fire处理开始的逻辑
-(void)fire{
    
}


-(void)doBegin{
    UnitBegin block = objc_getAssociatedObject(self, &key1);
    if (block) {
        block();
    }
    
}
- (void)doComplete:(id)data{
    UnitComplete block = objc_getAssociatedObject(self, &key2);
    if (block) {
        ///block传值
        block(data);
    }
    
    [self performSelector:@selector(disponse) withObject:nil afterDelay:1];
}

- (void)doFailed:(id)data{
    UnitFailed block = objc_getAssociatedObject(self, &key3);
    if (block) {
        ///block传值
        block(data);
    }
    
    [self performSelector:@selector(disponse) withObject:nil afterDelay:1];
}

-(void)disponse{
    [[Global sharedGlobal].unitDictionary removeObjectForKey:self.uIndex];
}

- (void)dealloc
{
    track();
}

/**
 OC中的关联就是在已有类的基础上添加对象参数。来扩展原有的类，需要引入#import <objc/runtime.h>头文件。关联是基于一个key来区分不同的关联。
 常用函数: objc_setAssociatedObject     设置关联
 objc_getAssociatedObject     获取关联
 objc_removeAssociatedObjects 移除关联
 */

@end