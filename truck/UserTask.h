//
//  UserTask.h
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"
#import "EETable.h"
#import "EGORefreshTableHeaderView.h"

@interface UserTask : EEView<UITableViewDelegate,UITableViewDataSource,EGORefreshTableDelegate>{
    EEButton *btnCur;
    EEButton *btnHistory;
    
    
    UIEdgeInsets tableEgeinsets;
    UITableView *dataTableView;
    
    
    NSInteger curPageNum;
    BOOL _reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
    EGORefreshTableFooterView *_refreshFooterView;
    BOOL isNewFreshing;

    float cellHeight;
    
    int taskType;
    
}

@property(nonatomic,strong)NSMutableArray *taskList;

@end
