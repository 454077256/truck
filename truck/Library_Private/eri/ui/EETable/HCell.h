//
//  HCell.h
//  Lohas
//
//  Created by 洪湃 on 16/5/27.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface HCell : EEView<UIScrollViewDelegate>{
    BOOL isLoadedUI;
    UIScrollView *contentScrollView;
    EGOImageView *contentImage;
}

-(void)loadData:(NSDictionary *)data;
-(void)load;
-(void)unload;

@end
