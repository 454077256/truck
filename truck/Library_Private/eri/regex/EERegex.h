//
//  EERegex.h
//  Zume100
//
//  Created by hong pai on 14-2-27.
//
//

#import <Foundation/Foundation.h>

@interface EERegex : NSObject


+(BOOL)validatePhoneCode:(NSString *)str;
+(BOOL) validateEmail:(NSString *)email;
    
+(BOOL)validateString:(NSString *)content reg:(NSString *)regex;
    
@end
