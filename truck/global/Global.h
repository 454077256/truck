//
//  Global.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseClass.h"
#import "EGOImageView.h"



typedef void(^UIControlTouch) (id postResult);

typedef enum {
    MMShadowLeft=0,
    MMShadowRight,
    MMShadowUp,
    MMShadowDown,
    MMShadowALL
}MMShadow;

typedef enum {
    MMNetworkStatusNO=0,
    MMNetworkStatusWiFi,
    MMNetworkStatus3g
}MMNetworkStatus;



typedef enum{
    MMPhoneType4=1,
    MMPhoneType5,
    MMPhoneType6,
    MMPhoneType6p,
    MMPhoneType6p_FS,
    MMPhoneTypePad,
    MMPhoneTypePad1
}MMPhoneType;


extern id tmpObject;
extern EEImageView *tmpImgView;
extern EEButton *tmpButtom;
extern EELabel *tmpLabel;
extern EEView *tmpView;
extern EETextFiled *tmpTextFiled;

@interface Global : NSObject{
    
}

@property (copy) UIControlTouch controlTouchBlock;

@property(nonatomic)MMPhoneType phoneType;

@property(nonatomic,strong)EEView *filebrowser;
@property(nonatomic,strong)EEView *filelistnav;
@property(nonatomic,strong)EEView *filecontent;




@property(nonatomic,strong) NSMutableDictionary *unitDictionary;


@property(nonatomic)CGSize appSize;

@property(nonatomic)CGSize screenSize;

@property(nonatomic)BOOL isDevice4;//是否是4代iphone的尺寸

@property(nonatomic)float version;

@property(nonatomic)BOOL isAnimationWhenShowKey;
@property(nonatomic)BOOL isShowPdfMarkIco;

@property(nonatomic,strong )UIViewController *rootController;

@property(nonatomic,strong) NSString *pageType;

//当前登录用户的关注列表（用来比对的）
//@property(nonatomic,strong) NSArray *myAttentionList;

@property(nonatomic,strong) NSMutableDictionary *reuserContainer;



//设置

@property(nonatomic) BOOL isUserBigFont;
@property(nonatomic) BOOL isSliderAutoScroll;

@property(nonatomic) BOOL isModifyUserShowInformation;


@property(nonatomic) BOOL is_ViewSizeIsEqualToAppSize;

//ipad 的变量
@property(nonatomic,strong) EEView *curContainer;

@property(nonatomic)int lastClickTag;
@property(nonatomic,strong) UIViewController *lastViewController;

+(Global*) sharedGlobal;


+(NSDictionary *)loadplist:(NSString *)path;

+(NSDictionary *)loadxml:(NSString *)path;

+ (UIImage	  *) loadImg:(NSString*)fname;
+(UIImage *)loadImgCache:(NSString *)fname ;

+(EEView *)createViewEmpty;
+(EEView *)createViewByFrame:(CGRect)frame color:(UIColor *)col;
+(EEImageView *)createImage:(CGRect )frame fname:(NSString *)filePath;
+(EEImageView *)createImage:(NSString *)filePath frame:(CGRect)frame;
+(EEImageView *)createImage:(NSString *)filePath point:(CGPoint)point;
+(EEImageView *)createImage:(NSString *)filePath center:(CGPoint)point;
+(EEImageView *)createImage:(NSString *)filePath fitWidth:(int)fwidth point:(CGPoint)point height:(NSNumber *)resultHeight;
+(EEImageView *)createImage:(NSString *)filePath fitSize:(CGSize)fsize center:(CGPoint)point;

+(EGOImageView *)createImageEGO:(NSString *)fileURL frame:(CGRect)_frame;
+(EGOImageView *)createImageEGO:(NSString *)fileURL size:(CGSize)_size center:(CGPoint)point preimage:(UIImage *)_preimage;
+(EGOImageView *)createImageEGO:(NSString *)fileURL size:(CGSize)_size center:(CGPoint)point;

+(UIImageView *)createCircleImage:(NSString *)filename url:(NSString *)url size:(CGSize)_size center:(CGPoint)_center;

    
+ (EELabel  *) createLabel:(CGRect)frame label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)size fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align;
+ (EELabel  *) createLabelFit:(CGSize)size point:(CGPoint)point label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)fontSize fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align resultSize:(CGSize *)resultSize;
+ (EELabel  *) createLabelFitHTML:(CGSize)size point:(CGPoint)point label:(NSString*)text numLine:(int)numline resultSize:(CGSize *)resultSize;

+(EEButton *)createBtnAlpha:(CGRect)frame clearColor:(BOOL)isClearColor delegate:(id)target action:(SEL)act;

+(EEButton *)createBtnColor:(CGRect)frame bgcolor:(UIColor *)bgcolor title:(NSString *)title titleNormal:(UIColor *)_col titleSelect:(UIColor *)_colS size:(int)size delegate:(id)target action:(SEL)act;

+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)dName target:(id)target action:(SEL)act;
+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName target:(id)target action:(SEL)act;
+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)nDown title:(NSString *)_title col:(UIColor *)_col colS:(UIColor *)_colS size:(int)size target:(id)target  action:(SEL)act;
+(EEButton *)createBtn_center:(CGPoint)center name:(NSString *)nName down:(NSString *)nDown  target:(id)target action:(SEL)act;

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector;
+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector;
+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector;




+(EEButton *)createBlockBtnAlpha:(CGRect)frame clearColor:(BOOL)isClearColor action:(eeBlock1)block1;
+(EEButton *)createBlockBtnColor:(CGRect)frame bgcolor:(UIColor *)bgcolor title:(NSString *)title titleNormal:(UIColor *)_col titleSelect:(UIColor *)_colS size:(int)size  action:(eeBlock1)block1;
+(EEButton *)createBlockBtn_center:(CGPoint)center name:(NSString *)nName down:(NSString *)nDown  action:(eeBlock1)block1;
+(EEButton *)createBlockBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)dName  action:(eeBlock1)block1;
+(EEButton *)createBlockBtn:(CGRect)frame normal:(NSString *)nName  action:(eeBlock1)block1;
+(EEButton *)createBlockBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)nDown title:(NSString *)_title col:(UIColor *)_col colS:(UIColor *)_colS size:(int)size  action:(eeBlock1)block1;
+(EEButton *)createBlockFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size  action:(eeBlock1)block1;
+(EEButton *)createBlockFitButton:(UIEdgeInsets)contentEdge original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size  action:(eeBlock1)block1;
+(EEButton *)createBlockFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size  action:(eeBlock1)block1;




+(EETextFiled *)createTextFiled:(CGRect )frame size:(int)size color:(UIColor *)color;
+(EETextView *)createTextView:(CGRect )frame size:(int)fontsize color:(UIColor *)color txt:(NSString *)texts;

+(EEControl *)createControl:(CGRect)frame delegate:(id)delegate action:(SEL)selector color:(UIColor *)color isHighLight:(BOOL)isHigh;
+(EEControl *)createControlBlock:(CGRect)frame action:(eeBlock1)block1 color:(UIColor *)color isHighLight:(BOOL)isHigh;

#pragma mark -- UI UIScrollView
+(EEScrollView *)createScroll:(CGRect)frame delegate:(id)_delegate content:(UIView *)_content pageEnable:(BOOL)pageEnable zoom:(BOOL)isZoom;

#pragma mark -- view 操作
+(void)addShadowToView:(UIView *)pview tag:(MMShadow)tag ;
+(void)addBorder:(UIView *)v borderWidth:(CGFloat)bw color:(UIColor *)col radius:(int)r;
+(void)addDashBorder:(UIView *)instance color:(UIColor *)_color;
-(void)resetViewIntFrame:(UIView *)view;


#pragma mark -- gif
+(UIWebView *)createGif:(CGRect)frame path:(NSString *)filepath;

+(UIActivityIndicatorView *)createHud:(CGSize)size center:(CGPoint)center style:(UIActivityIndicatorViewStyle)style;

-(void) log:(NSString *)s level:(int)lev;


+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize;

+(NSString *)assets:(NSString *)fileName;

#pragma mark -- 系统设定
+(void)setAppWeakupAllTime:(BOOL)isAllWeakup;
#pragma mark -- 文件夹操作 --
+ (long long) fileSizeAtPath:(NSString*) filePath;
+ (float ) folderSizeAtPath:(NSString*) folderPath;
+(NSData *)     converTxtEncodeFromWin:(NSData *)winTxtFileData;//只有在UTF16的编码下才能正确显示中文，因此我们需要在展示文件之前将文件的编码改为UTF16
+(NSString *)   converTxtEncodeFromWin2:(NSData *)winTxtFileData;//只有在UTF16的编码下才能正确显示中文，因此我们需要在展示文件之前将文件的编码改为UTF16

+(NSMutableArray *)getAllFilesInFolder:(NSString *)folderPath;

#pragma mark -- 颜色 color
+(UIColor*)colorWithHex:(uint)value;
+(NSString *)getAppVersion;
+(NSString *)getIdentifier;
#pragma mark - EGOImage

+(NSString *)egoPathConvert:(NSString *)filePath  style:(NSString *)style;
+(BOOL)fileIsInEGOCache:(NSString *)filePath style:(NSString *)style;


#pragma mark - date -
+(NSString *)getDateString:(NSDate *)date formatString:(NSString *)forString;
+(NSDate *)getDateByString:(NSString *)dateString formatString:(NSString *)forString;
+(NSTimeInterval)getTwoDateDistance:(NSDate *)d1 :(NSDate *)d2;
+(NSString *) compareCurrentTime:(NSDate*) compareDate;

#pragma mark  - tools
-(void)alert:(NSString *)content ;
+(void)alertNoWeb;

+(void)arrayRandom:(NSMutableArray *)pArray;
+(void)arraySort:(NSMutableArray *)postArray key:(NSString *)sortKey ascending:(BOOL)isAscending;


+(void)statusBarShow:(BOOL)isShow;

+(CGRect)uisize_isHasStatusSpace:(BOOL)isHasStatusSpace;

+(float)multiHeight:(float)value;
+(float)multiHeightPhone:(float)phone WithPad:(float)pad;
+(float)multiHeightAA:(float)p5 :(float)p6 :(float)p6p :(float)pad;

+(int)uiYstartIOS678;

-(float)pad_phone:(float)x1 :(float)x2;
-(float)p5_6_6p:(float)x1 :(float)x2 :(float)x3;

#pragma mark --  网络
+(MMNetworkStatus)netWorkState;

#pragma mark -- 字符串检测
+(BOOL)isImageFileName:(NSString *)oneString;

#pragma mark -- NSDictionary 操作
+(void)setObject:(NSMutableDictionary *)dic value:(NSString *)_value key:(NSString *)_key;


+(void)filterNullValue:(id)postData;

#pragma mark -- EGO 本地缓存
+(NSString *)egoCacheFolder;
+(NSString *)egoKey:(NSString *)url;
-(void)egoSaveLocalImageData:(UIImage *)localImage url:(NSString *)webpath;

@end


