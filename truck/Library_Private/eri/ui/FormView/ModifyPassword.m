//
//  ModifyPassword.m
//  OuRuiDA
//
//  Created by 洪湃 on 16/6/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "ModifyPassword.h"
#import "SVProgressHUD.h"
#import "Tools_math.h"
#import "URequest.h"


@implementation ModifyPassword


-(void)initView{
    [self setBackgroundColor:[CBlack colorWithAlphaComponent:0.3]];
    
    UIView *container = [[UIView alloc] initWithFrame:R(0, 0, 300, 270)];
    [container setBackgroundColor:CWhite];
    container.layer.cornerRadius = 5;
    container.clipsToBounds = YES;
    [self addSubview:container];
    
    [container addSubview:[Global createLabel:R(10, 50, 80, 32) label:@"原来密码:" lines:0 fontSize:14 fontName:nil textcolor:CGray align:(NSTextAlignmentRight)]];
    [container addSubview:[Global createLabel:R(10, 100, 80, 32) label:@"新密码:" lines:0 fontSize:14 fontName:nil textcolor:CGray align:(NSTextAlignmentRight)]];
    [container addSubview:[Global createLabel:R(10, 150, 80, 32) label:@"重复新密码:" lines:0 fontSize:14 fontName:nil textcolor:CGray align:(NSTextAlignmentRight)]];
    
    
    [container addSubview:tmpTextFiled=[Global createTextFiled:R(100, 50, 170, 32) size:15 color:CGray]];
    [Global addBorder:tmpTextFiled borderWidth:1 color:CGray radius:4];pass1 = tmpTextFiled;pass1.secureTextEntry = YES;
    
    [container addSubview:tmpTextFiled=[Global createTextFiled:R(100, 100, 170, 32) size:15 color:CGray]];
    [Global addBorder:tmpTextFiled borderWidth:1 color:CGray radius:4];pass2 = tmpTextFiled;pass2.secureTextEntry = YES;
    
    [container addSubview:tmpTextFiled=[Global createTextFiled:R(100, 150, 170, 32) size:15 color:CGray]];
    [Global addBorder:tmpTextFiled borderWidth:1 color:CGray radius:4];pass3 = tmpTextFiled;pass3.secureTextEntry = YES;
    
    [container addSubview:[Global createBtn_center:ccp(270, 30) name:@"img/btn_close.png" down:nil target:self action:@selector(clickClose)]];
    
    container.center = center(self);
    
    [container addSubview:[Global createBtnColor:R(50, 205, 200, 32) bgcolor:CRed title:@"提 交" titleNormal:CWhite titleSelect:CGray size:20 delegate:self action:@selector(clickModifyPassWord)]];
    
    
    //键盘升起
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //键盘降下
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)clickModifyPassWord{
    track();
    
    [pass1 resignFirstResponder];
    [pass2 resignFirstResponder];
    [pass3 resignFirstResponder];
    
    if (pass1.text.length<=0) {
        [SVProgressHUD showWithStatus:@"输入原来密码"];
        return;
    }
    
    if (pass2.text.length<=0) {
        [SVProgressHUD showWithStatus:@"输入新密码"];
        return;
    }
    
    if (![pass3.text isEqualToString:pass2.text]) {
        [SVProgressHUD showWithStatus:@"新密码请保持一致"];
        return;
    }
    
//    [[[URequest alloc] init] start:@{@"url":WebHost(@"ModifyPassword"),@"oldpass":[Tools_math md5:pass1.text] ,@"newpass":[Tools_math md5:pass2.text]} begin:^{
//        [SVProgressHUD show];
//    } complete:^(id postResult) {
//        if ([postResult[@"code"] intValue]==1) {
//            [SVProgressHUD showInfoWithStatus:@"修改成功"];
//            [SVProgressHUD dismissWithDelay:1];
//            
//            //关闭
//            [self clickClose];
//        }else{
//            NSString *message = [NSString stringWithFormat:@"修改失败：%@",postResult[@"msg"]];
//            [SVProgressHUD showInfoWithStatus:message];
//            [SVProgressHUD dismissWithDelay:1];
//        }
//    } failed:^(id postResult) {
//        [SVProgressHUD showInfoWithStatus:@"修改失败"];
//        [SVProgressHUD dismissWithDelay:1];
//    }];
}


#pragma mark -- 键盘事件 --

-(void)keyboardWillShow:(NSNotification *)notice{
    [self ee_tween:0.3 to:@"{x:0,y:-200}"];
}
-(void)keyboardWillHide:(NSNotification *)notice{
    [self ee_tween:0.3 to:@"{x:0,y:0}"];
}

-(void)clickClose{
    [self eeRemoveDown];
}

@end
