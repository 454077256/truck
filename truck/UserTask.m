//
//  UserTask.m
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "UserTask.h"
#import "CellTask.h"
#import "URequestJson.h"
#import "SVProgressHUD.h"



@implementation UserTask

-(void)initView{
    [self setBackgroundColor:CHex(0xEFEFEF)];
    //导航条
    EETopBar *topbar = [EETopBar instanceBy_normalLeft:nil
                                            selectLeft:nil
                                            leftAction:nil
                                          leftDelegate:nil
                                           normalRight:nil
                                           selectRight:nil
                                           rightAction:nil
                                         rightDelegate:self
                                                center:@"我的任务" isIOS7Lagger:YES];
    [topbar setBackgroundColor:CHex(0x1BA2E7)];
    [self addSubview:topbar];
    
    curPageNum = 1;
    
    self.taskList = [NSMutableArray array];

    //当前任务
    btnCur = [Global createBtnColor:R(SS(160), SS(134), SS(158), SS(48)) bgcolor:CHex(0xFC9827) title:@"当前任务" titleNormal:CWhite titleSelect:CHex(0xFC9827) size:S(14) delegate:self action:@selector(clickCurTask:)];
    [Global addBorder:btnCur borderWidth:S(3) color:CHex(0xFC9827) radius:0];
    //历史任务
    btnHistory = [Global createBtnColor:R(SS(316), SS(134), SS(158), SS(48)) bgcolor:CHex(0xFC9827) title:@"历史任务" titleNormal:CWhite titleSelect:CHex(0xFC9827) size:S(14) delegate:self action:@selector(clickHistoryTask:)];
    [Global addBorder:btnHistory borderWidth:S(3) color:CHex(0xFC9827) radius:0];
    //
    [self addSubview:[Global createImage:@"assets/icotime.png" center:ccp(SS(30), SS(208))]];
    
    NSString *content = @"<span style='font-size:1.5em;'>待执行任务:共<font style='color:red;'>5</font>件</span>";
    EELabel *maskTask = [Global createLabelFitHTML:CGSizeMake(0, 0) point:ccp(SS(50), SS(190)) label:content numLine:0 resultSize:nil];
    [self addSubview:maskTask];
    
    [self addSubview:btnCur];
    [self addSubview:btnHistory];
    
    [self clickCurTask:btnCur];
    
    
    [self createTable];
}

-(void)eeTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [AppUIFactory open:@"TaskDetail" frame:frameUI data:nil parent:RootView action:(ActionType_fromRight)];
}

-(void)createTable{
    cellHeight = SS(292);
    CGRect varTableFrame = R(0, SS(234), self.width, self.height-SS(234));
    
    //tableEgeinsets = UIEdgeInsetsMake(0, SS(7), 0, SS(7));
    tableEgeinsets = UIEdgeInsetsMake(0,0, 0, 0);
    
    dataTableView = [[UITableView alloc] initWithFrame:varTableFrame];
    
    dataTableView.showsVerticalScrollIndicator = NO;
    //dataTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [dataTableView setSeparatorInset:tableEgeinsets];
    dataTableView.delegate = self;
    dataTableView.dataSource = self;
    
    dataTableView.tableFooterView = [Global createViewByFrame:R(0, 0, self.width, SS(0)) color:CHex(0xdddddd)];
    
    if ([dataTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [dataTableView setSeparatorInset:tableEgeinsets];
    }
    
    if ([dataTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [dataTableView setLayoutMargins:tableEgeinsets];
    }
    
    [dataTableView setBackgroundColor:CClear];
    [dataTableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
    
    [self addSubview:dataTableView];
    
    //创建下拉刷新控件
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-dataTableView.height, dataTableView.width, dataTableView.height) dateId:201];
    _refreshHeaderView.delegate = self;
    [dataTableView addSubview:_refreshHeaderView];
    [_refreshHeaderView setBackgroundColor:CClear];
    
}


-(void)requestTaskList{
    NSString *tagName= @"";
    
    if (taskType==1) {
        tagName = @"JX";
    }else if(taskType==2) {
        tagName = @"WC";
    }
    
    
    NSString *urlstring = WebURL(@"findTask");
    
    NSString *startRow = [NSString stringWithFormat:@"%ld",(curPageNum-1)*10];
    NSString *endRow = [NSString stringWithFormat:@"%ld",(curPageNum)*10];
    
    NSDictionary *postDictionary = @{@"url":urlstring,@"driverTel":[AppData getUser:(UserInfor_username)],@"startRows":startRow,@"endRows":endRow,@"status":tagName};
    
    [[[URequestJson alloc] init] start:postDictionary begin:^{
        [SVProgressHUD show];
    } complete:^(id postResult) {
        trace(@"%@",postResult);
        if([postResult[@"code"] intValue]==200){
            trace(@"%@",postResult);
            
            NSArray *arrays = postResult[@"data"][@"records"];
            if(curPageNum==1){
                self.taskList = [NSMutableArray arrayWithArray:arrays];
            }else{
                [self.taskList addObjectsFromArray:arrays];
            }
            
            [dataTableView reloadData];
            
            [self doneLoadingTableViewData];
            [self performSelector:@selector(setFooterView) withObject:nil afterDelay:1];
            
            [SVProgressHUD dismiss];
        }else{
            
            [SVProgressHUD dismissWithError:postResult[@"message"] afterDelay:1];
        }
    } failed:^(id postResult) {
        trace(@"%@",postResult);
        [SVProgressHUD dismissWithError:postResult[@"message"] afterDelay:1];
    }];

    
}

-(void)clickCurTask:(id)sender{
    track();
    taskType = 1;
    
    curPageNum = 1;
    [btnCur setSelected:NO];
    [btnCur setBackgroundColor:CHex(0xFC9827)];
    [btnHistory setSelected:YES];
    [btnHistory setBackgroundColor:CWhite];
    
    [self requestTaskList];
}

-(void)clickHistoryTask:(id)sender{
    track();
    taskType = 2;
    
    curPageNum = 1;
    [btnCur setSelected:YES];
    [btnCur setBackgroundColor:CWhite];
    [btnHistory setSelected:NO];
    [btnHistory setBackgroundColor:CHex(0xFC9827)];
    
    [self requestTaskList];
}




#pragma mark ---table delegate ----

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:tableEgeinsets];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:tableEgeinsets];
    }
    [cell setLayoutMargins:tableEgeinsets];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.taskList count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellTask *cell = [[CellTask alloc] initWithFrame:R(0, 0, tableView.width, cellHeight)];
    [cell loadData:self.taskList[indexPath.row] size:CGSizeMake(tableView.width, cellHeight) indexPath:indexPath];
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    track();
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CellTask *cellTask = [tableView cellForRowAtIndexPath:indexPath];
    
    //打开文章
    [AppUIFactory open:@"TaskDetail" frame:frameUI data:cellTask.eeData parent:RootView action:1];
}


#pragma mark --- 下拉刷新 --

-(void)setFooterView{
    //    UIEdgeInsets test = self.aoView.contentInset;
    // if the footerView is nil, then create it, reset the position of the footer
    CGFloat height = MAX(dataTableView.contentSize.height, dataTableView.frame.size.height);
    if (_refreshFooterView && [_refreshFooterView superview])
    {
        // reset position
        _refreshFooterView.frame = CGRectMake(0.0f,
                                              height,
                                              dataTableView.frame.size.width,
                                              self.bounds.size.height);
    }else
    {
        // create the footerView
        _refreshFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:
                              CGRectMake(0.0f, height,
                                         dataTableView.frame.size.width, self.bounds.size.height) dateId:100];
        _refreshFooterView.delegate = self;
        [dataTableView addSubview:_refreshFooterView];
        [_refreshFooterView setBackgroundColor:RGB(239, 237, 233)];
    }
    
    if (_refreshFooterView)
    {
        [_refreshFooterView refreshLastUpdatedDate];
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods


-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
    track();
    
    _reloading = YES;
    if (aRefreshPos == EGORefreshHeader)
    {
        curPageNum = 1;
        isNewFreshing = YES;
        
    }else if(aRefreshPos == EGORefreshFooter)
    {
        curPageNum ++;
    }
    
    [self requestTaskList];
    /*
    if([self.newsDataList count]>5){
        [self performSelector:@selector(requestWebData) withObject:nil afterDelay:1];
    }else{
        [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.5];
    }
    */
    
    //[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.5];
}

- (void)doneLoadingTableViewData{
    
    track();
    //  model should call this when its done loading
    _reloading = NO;
    
    if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:dataTableView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:dataTableView];
        [self setFooterView];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (_refreshHeaderView)
    {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
    
    if (_refreshFooterView)
    {
        [_refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
    }
    
    /*
    if([self.newsDataList count]>5){
        
        int curY = scrollView.contentOffset.y;
        
        if (curY<100) {
            [self showWeekViewHeader:YES];
        }else{
            [self showWeekViewHeader:NO];
        }
    }
    */
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (_refreshHeaderView)
    {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    
    if (_refreshFooterView)
    {
        [_refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

-(BOOL)egoRefreshTableDataSourceIsLoading:(UIView *)view{
    return _reloading; // should return if data source model is reloading
}

-(NSDate *)egoRefreshTableDataSourceLastUpdated:(UIView *)view{
    return [NSDate date];
}

-(void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos{
    [self beginToReloadData:aRefreshPos];
}


@end
