//
//  WebRequestProxy.m
//  Zume100
//
//  Created by hong pai on 14-2-28.
//
//

#import "WebRequestProxy.h"
#import "SBJson.h"
#import "Reachability.h"

@implementation WebRequestProxy


/**
 url:请求url
 target:谁发出的请求delegate
 handlerResult:delegate的结果处理方法
 handlerFailed:delegate的异常处理方法
 postError:发现错误的话，是否交给delegate处理
 */

+(WebRequestProxy *)request:(NSURL *)url data:(NSDictionary *)pdata delegate:(id)target success:(SEL)hSuccess failed:(SEL)handlerFailed {
    return [[self alloc] initWithURL:url data:pdata delegate:target success:hSuccess failed:handlerFailed] ;
}


-(WebRequestProxy *)initWithURL:(NSURL *)url data:(NSDictionary *)pdata delegate:(id)target success:(SEL)hSuccess failed:(SEL)hFailed {
    self = [super init];
    if (self) {
        self.url = url;
        self.prameterDictionary = pdata;
        self.delegate = target;
        self.handlerResult = hSuccess;
        self.handlerFailed =  hFailed;
        
        self.isAlertError = YES;
        
        //创建request
        ASIFormDataRequest *requset = [self createRequest];
        
        self.proxyRequest = requset;
    }
    return self;
}


-(ASIFormDataRequest *)createRequest{
    ASIFormDataRequest *requset = [ASIFormDataRequest requestWithURL:self.url];
    [requset setDidFailSelector:@selector(proxyFailedSelector:)];
    [requset setDidFinishSelector:@selector(proxyCompeleteSelector:)];
    [requset setDelegate:self];
    
    if (self.prameterDictionary!=nil ) {
        NSArray *allKeys = [self.prameterDictionary allKeys];
        //复制post参数
        for (NSString *oneKey in allKeys) {
            id value = [self.prameterDictionary objectForKey:oneKey];
            [requset setPostValue:value forKey:oneKey];
        }
    }
//    
//    [requset setPostValue:[ZumeSysConfig getSessionId] forKey:@"sessionId"];
//    [requset setPostValue:[ZumeSysConfig getClientId] forKey:@"clientId"];
//    
//    NSString *version = [ZumeSysConfig getVersion];
//    [requset setPostValue:version forKey:@"clientVersion"];
    
    return requset;
}


-(void)startAsynchronous{
    //查看是否联网
//    
//    Reachability *currentReach=[Reachability reachabilityForInternetConnection];
//    if ([currentReach currentReachabilityStatus]==NotReachable) {
//        
//        [self.delegate performSelector:self.handlerFailed withObject:request];
//        
//        [delegate ]
//        return; d
//    }
//    
    
    
    NSAssert(self.proxyRequest != nil, @"request 不能为空");
    if (self.proxyRequest) {
        [self.proxyRequest startAsynchronous];
    }
//    traceInfor(@"%@",[self.proxyRequest postData]);
}

-(void)startSynchronous{//同步

    NSAssert(self.proxyRequest != nil, @"request 不能为空");
    if (self.proxyRequest) {
        [self.proxyRequest startSynchronous];
        
//        NSError *error = self.proxyRequest.error;
//        if (error==nil) {
//            [self proxyCompeleteSelector:self.proxyRequest];
//        }else{
//            [self proxyFailedSelector:self.proxyRequest];
//        }
    }
}

#pragma mark - 请求回调

-(void)proxyCompeleteSelector:(ASIFormDataRequest *)request{
    if (request.responseStatusCode==200) {
        
        NSString *responseString = request.responseString;
        NSDictionary *responseDictionary = [responseString JSONValue];
        
        if (responseDictionary==nil) {//!!!数据异常了，服务器返回非json字符串
            
            if(self.delegate && [self.delegate respondsToSelector:self.handlerFailed]){
                [self.delegate performSelector:self.handlerFailed withObject:request];
            }
            traceError(@"!!!数据异常了，服务器返回非json字符串%@",responseString);
            
            [self disponse];//可以回收
        }else {//数据正常
            //返回code
            [self.delegate performSelector:self.handlerResult withObject:responseDictionary];
            [self disponse];
            
        }
    }else{
        traceError(@"请求出错: %@",request.responseString);
        if(self.delegate && [self.delegate respondsToSelector:self.handlerFailed]){
            [self.delegate performSelector:self.handlerFailed withObject:request];
        }
        [self disponse];
    }
}


//操作失败
-(void)proxyFailedSelector:(ASIFormDataRequest *)request{
    traceError(@"错误信息code:%ld info：%@",(long)request.error.code,request.error.description);
    if(self.delegate && [self.delegate respondsToSelector:self.handlerFailed]){
        [self.delegate performSelector:self.handlerFailed withObject:request];
    }
    
    [self disponse];
}



#pragma mark - 请求session操作
-(NSString *)getTimestamp{
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
    return  [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
}


#pragma mark - 回收 －
//回收WebRequestProxy对象
-(void)disponse{
    track();
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (self.proxyRequest) {
        self.proxyRequest.delegate = nil;
        [self.proxyRequest cancel];
        self.proxyRequest = nil;
    }
    self.delegate = nil;
    self.url = nil;
    self.prameterDictionary = nil;
//    [self release];
}


-(void)dealloc{
    [self disponse];
    track();
//    [super dealloc];
}


@end
