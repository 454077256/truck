//
//  InAppStore.m
//  OneTap
//
//  Created by xuzhe on 09/12/11.
//  Copyright 2009 Rakuraku Technologies, Inc.. All rights reserved.
//

#import "InAppPurchaseManager.h"

@implementation InAppPurchaseManager {
    InAppPurchaseObserver *_observer;
}

@synthesize delegate = _delegate;

+ (InAppPurchaseManager *)sharedInAppPurchaseManager {
    static InAppPurchaseManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[InAppPurchaseManager alloc] init];
    });
    return manager;
}

- (id)init {
	self = [super init];
	if (self != nil) {
		_observer = [[InAppPurchaseObserver alloc] init];
		_observer.delegate = self;
		[[SKPaymentQueue defaultQueue] addTransactionObserver:_observer];
	}
	return self;
}

- (void)dealloc {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:_observer];
}

+ (BOOL)canMakePayments {
	if ([SKPaymentQueue canMakePayments]) {
		// Display a store to the user.
		return YES;
	} else {
		// Warn the user that purchases are disabled.
		return NO;
	}
}

- (void)requestProductsData:(NSSet *)productIdSet {
	SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdSet];
	request.delegate = self;
	[request start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSArray *products = response.products;
    
    NSLog(@"products: %@, invalid products: %@", products, response.invalidProductIdentifiers);
	
	if (_delegate && [_delegate respondsToSelector:@selector(showProductInfo:invalidProductIdentifiers:)]) {
		[_delegate showProductInfo:products invalidProductIdentifiers:response.invalidProductIdentifiers];
	}
}

- (void)addPayment:(NSString *)productIdentifier {
	SKPayment *payment = [SKPayment paymentWithProductIdentifier:productIdentifier];
	[[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (BOOL)provideContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate {
	if (_delegate && [_delegate respondsToSelector:@selector(provideContent:expireDate:)]) {
		return [_delegate provideContent:transaction expireDate:expireDate];
	}
    return NO;
}

- (BOOL)provideRestoredContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate {
	if (_delegate && [_delegate respondsToSelector:@selector(provideRestoredContent:expireDate:)]) {
		return [_delegate provideRestoredContent:transaction expireDate:expireDate];
    }
    return NO;
}

- (void)verifyReceiptFailed:(SKPaymentTransaction *)transaction withError:(NSError *)error {
    if (_delegate && [_delegate respondsToSelector:@selector(verifyReceiptFailed:withError:)]) {
        [_delegate verifyReceiptFailed:transaction withError:error];
    }
}

- (void)paymentFailed:(SKPaymentTransaction *)transaction {
    NSLog(@"payment failed: %@", transaction.error.localizedDescription);
    if (_delegate && [_delegate respondsToSelector:@selector(paymentFailed:)]) {
		[_delegate paymentFailed:transaction];
	}
}

- (void)paymentCancelled:(SKPaymentTransaction *)transaction {
	if (_delegate && [_delegate respondsToSelector:@selector(paymentCancelled:)]) {
		[_delegate paymentCancelled:transaction];
	}
}

- (void)restoreFinished {
    if (_delegate && [_delegate respondsToSelector:@selector(restoreFinished)]) {
        [_delegate restoreFinished];
    }
}

- (void)restoreFailed:(NSError *)error {
    if (_delegate && [_delegate respondsToSelector:@selector(restoreFailed:)]) {
        [_delegate restoreFailed:error];
    }
}

@end
