//
//  EEOneEvent.m
//  OuRuiDA
//
//  Created by 洪湃 on 16/6/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "EEOneEvent.h"
#import "EEDownloadManager.h"

@implementation EEOneEvent

+(EEOneEvent *)instanceWithWebFilePath:(NSString *)webpath savePath:(NSString *)localSavePath configData:(NSDictionary *)pdata progresView:(LDProgressView *)progressView{
    EEOneEvent *instance = [[EEOneEvent alloc] init];
    instance.webFilePath = webpath;
    instance.localFilePath = localSavePath;
    instance.eeData = pdata;
    instance.progressView = progressView;
    
    return instance;
}

-(void)start{
    if (isStarting) {
        return;
    }
    isStarting = YES;
    
    //创建下载文件的夫文件夹
    [[NSFileManager defaultManager] createDirectoryAtPath:self.localFilePath.stringByDeletingLastPathComponent withIntermediateDirectories:YES attributes:nil error:nil];
    
    trace(@"开始下载文件:%@:",self.webFilePath);
    
    NSURL *url = [NSURL URLWithString:self.webFilePath];
    
    //开始下载
    dataRequest = [ASIFormDataRequest requestWithURL:url];
    
    [dataRequest setDidFinishSelector:@selector(loadFinished:)];
    [dataRequest setDidFailSelector:@selector(loadFailed:)];
//    [dataRequest setTemporaryFileDownloadPath:<#(NSString *)#>]
//    [dataRequest setAllowResumeForFileDownloads:YES];
    [dataRequest setDownloadProgressDelegate:self];
    [dataRequest setDownloadDestinationPath:self.localFilePath];
    [dataRequest setShowAccurateProgress:YES];
    [dataRequest setDelegate:self];
    [dataRequest startAsynchronous];
}
-(void)stop{
    isStarting = NO;
    
    [dataRequest removeTemporaryDownloadFile];
    
    [dataRequest clearDelegatesAndCancel];
    dataRequest.delegate = nil;
    dataRequest = nil;
}


#pragma mark ---下载返回函数

- (void)loadFinished:(ASIHTTPRequest *)request
{
    dataRequest.delegate = nil;
    
    NSData *fileData = request.responseData;
    int resultCode = [request responseStatusCode];
    
    isStarting = NO;
    
    
    if (resultCode == 200) { //下载图片
        trace(@"文件下载完成 %@",self.localFilePath);
        if (self.delegate && [self.delegate respondsToSelector:@selector(EEOneEventDownloadFailed: data:)]) {
            [self.delegate EEOneEventDownloadSuccess:self data:self.eeData];
        }
    }
    
    [EEDownloadManager removeOneDownloadTaskByOneEvent:self];
    [EEDownloadManager startQueue];
    [EEDownloadManager printInfor];
    
    
}

- (void)loadFailed:(ASIHTTPRequest *)request
{
    dataRequest.delegate = nil;
    
    isStarting = NO;
    trace(@"文件下载失败 %@",self.webFilePath);
    if (self.delegate && [self.delegate respondsToSelector:@selector(EEOneEventDownloadFailed: data:)]) {
        [self.delegate EEOneEventDownloadFailed:self data:self.eeData];
    }
    [EEDownloadManager startQueue];

}
-(void)setProgress:(float)newProgress{
    
//    trace(@"%f",newProgress);
    [self.progressView setProgress:newProgress];
}


-(void)disponse{
    [dataRequest clearDelegatesAndCancel];
    dataRequest.delegate = nil;
    dataRequest = nil;
}

-(void)dealloc{
    [self.progressView removeFromSuperview];
    
    self.eeData = nil;
    self.webFilePath = nil;
    self.localFilePath = nil;
    self.progressView = nil;
    
    track();
}




@end
