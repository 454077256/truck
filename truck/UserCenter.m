//
//  UserCenter.m
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "UserCenter.h"
#import "CellUserCeter.h"
#import "SVProgressHUD.h"


@implementation UserCenter

-(void)initView{
    [self setBackgroundColor:CHex(0xEFEFEF)];
    //导航条
    EETopBar *topbar = [EETopBar instanceBy_normalLeft:nil
                                            selectLeft:nil
                                            leftAction:nil
                                          leftDelegate:nil
                                           normalRight:nil
                                           selectRight:nil
                                           rightAction:nil
                                         rightDelegate:self
                                                center:@"个人中心" isIOS7Lagger:YES];
    [topbar setBackgroundColor:CHex(0x1BA2E7)];
    [self addSubview:topbar];
    
    
    
    //表格
    dataTable = [[UITableView alloc] initWithFrame:R(0, nextY(topbar, 0), self.width, self.height-topbar.height) style:(UITableViewStylePlain)];
    [self addSubview:dataTable];
    
    if ([dataTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [dataTable setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    if ([dataTable respondsToSelector:@selector(setLayoutMargins:)]) {
        [dataTable setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    dataList = @[@[@{@"title":@"个人信息"}],@[@{@"title":@"修改密码"},@{@"title":@"退出当前账号"}]];
    //[self refreshData];
    
    dataTable.delegate = self;
    dataTable.dataSource = self;
    
    [dataTable setBackgroundColor:CHex(0xF0F0F0)];
    
    [dataTable setTableFooterView:[Global createViewEmpty]];
    [dataTable reloadData];
}

#pragma mark ---- UITableSourceData delegate ----

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setLayoutMargins:UIEdgeInsetsZero];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [dataList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return SS(144);
    }else{
        return SS(72);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataList[section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary *dictionary = dataList[indexPath.section][indexPath.row];
    CGSize size = CGSizeMake(self.width, SS(72));
    if (indexPath.section==0) {
        size = CGSizeMake(self.width, SS(144));
    }
    CellUserCeter *cell = [[CellUserCeter alloc] init];
    [cell loadData:dictionary size:size indexPath:indexPath];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    track();
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSInteger index = indexPath.row;
    if (index==0) {//
        [AppUIFactory open:@"AppRePassword" frame:frameUI data:nil parent:RootView action:(ActionType_fromRight)];
    }else if (index==1) {//
        
        if ([AppData checkLogin]) {
            
            EEView *container = [Global createViewByFrame:RootView.bounds color:[CBlack colorWithAlphaComponent:0.3]];
            [RootView addSubview:container];
            [Global addShadowToView:container tag:(MMShadowALL)];
            
            [container addSubview:tmpView = [Global createViewByFrame:R(0, 0, SS(460), SS(248)) color:CWhite]];
            [tmpView addSubview:[Global createLabel:R(0, SS(60), tmpView.width, SS(60)) label:@"确认退出账号?" lines:0 fontSize:S(15) fontName:nil
                                          textcolor:CGray align:(NSTextAlignmentCenter)]];
            tmpView.center = center(container);
            
            [tmpView addSubview:[Global createBlockBtnColor:R(SS(48), SS(148), SS(168), SS(48)) bgcolor:CRed title:@"确认" titleNormal:CWhite titleSelect:CGray size:S(14) action:^(id postResult) {
                
                [container removeFromSuperview];
                
                [AppData setData:EE_AppConfig key:@"userIsLogined" value:@"0"];
                
                [SVProgressHUD showSuccessWithStatus:@"注销成功" duration:0.5];
                
                [self performSelector:@selector(loginOutSuccess) withObject:nil afterDelay:0.5];
            }]];
            [tmpView addSubview:[Global createBlockBtnColor:R(SS(240), SS(148), SS(168), SS(48)) bgcolor:CGray title:@"取消" titleNormal:CWhite titleSelect:CBlack size:S(14) action:^(id postResult) {
                [container removeFromSuperview];
            }]];
        }else{
            [SVProgressHUD showErrorWithStatus:@"未登录" duration:1];
        }

    }
}


-(void)loginOutSuccess{
    NoticePost(knotice_userLoginOut, nil);
}

//拥有section 的 回调方法
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return SS(24);
    }else{
        return SS(24);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    EEView *v = [Global createViewByFrame:R(0, 0, self.width, SS(20)) color:CHex(0xF0F0F0)];
    return v;
}

-(void)dealloc{
    track();
}

@end
