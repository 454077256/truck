//
//  EEHTable.h
//  Lohas
//
//  Created by 洪湃 on 16/5/27.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface EEHTable : EEView<UIScrollViewDelegate>{
    UIScrollView *targetScrollView;
}

-(void)gotopage:(int)pageIndex;
+(id)instanceByFrame:(CGRect)frame list:(NSArray *)dataList cellCalss:(Class)_cellClass defalultPage:(int)dPage;

@property(nonatomic,strong)NSArray *dataArrays;
@property(nonatomic,strong)NSMutableArray *pageArrays;
@property(nonatomic)int defalultPage;
@property(nonatomic)Class tableCellClass;

@end
