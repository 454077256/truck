//
//  WebRequestProxy.h
//  Zume100
//
//  Created by hong pai on 14-2-28.
//
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface WebRequestProxy : NSObject{
    int sessionReqeustTimes;//session请求的次数
}

/* 遇到错误是否用UIAlert弹出来, 默认为YES */
@property(nonatomic) BOOL isAlertError;

@property(nonatomic,weak) id delegate;
@property(nonatomic,strong) ASIFormDataRequest *proxyRequest;
@property(nonatomic) SEL handlerResult;
@property(nonatomic) SEL handlerFailed;
@property(nonatomic,strong) NSURL *url;
@property(nonatomic,strong) NSDictionary *prameterDictionary;


+(WebRequestProxy *)request:(NSURL *)url data:(NSDictionary *)pdata delegate:(id)target success:(SEL)hSuccess failed:(SEL)handlerFailed;

-(WebRequestProxy *)initWithURL:(NSURL *)url data:(NSDictionary *)pdata delegate:(id)target success:(SEL)hSuccess failed:(SEL)hFailed ;

-(void)startAsynchronous;
-(void)startSynchronous;
@end
