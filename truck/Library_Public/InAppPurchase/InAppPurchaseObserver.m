//
//  InAppStoreObserver.m
//  OneTap
//
//  Created by xuzhe on 09/12/11.
//  Copyright 2009 Rakuraku Technologies, Inc.. All rights reserved.
//

#import "InAppPurchaseObserver.h"
#import "InAppPurchaseStatus.h"
#import "Extensions.h"

@interface InAppPurchaseObserver (PrivateMethod)

- (void)completeTransaction:(SKPaymentTransaction *)transaction;
- (void)restoreTransaction:(SKPaymentTransaction *)transaction;
- (void)failedTransaction:(SKPaymentTransaction *)transaction;
- (NSInteger)verifyReceipt:(SKPaymentTransaction *)transaction expireDate:(NSDate **)date;
- (BOOL)provideContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate;
- (BOOL)provideRestoredContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate;

@end

@implementation InAppPurchaseObserver {
    NSOperationQueue *_queue;
}
@synthesize delegate = _delegate;

- (NSOperationQueue *)queue {
    if (!_queue) {
        _queue = [[NSOperationQueue alloc] init];
        [_queue setMaxConcurrentOperationCount:2];
        [_queue setSuspended:NO];
    }
    return _queue;
}

// Implement the paymentQueue:updatedTransactions: method on MyStoreObserver.
// The observer’s paymentQueue:updatedTransactions: method is called whenever new transactions are created or updated.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    NSLog(@"paymentQueue, updatedTransactions: %@", transactions);
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:[transactions count]];
    for (SKPaymentTransaction *transaction in transactions) {
        if (transaction.transactionState == SKPaymentTransactionStateFailed) {
            [self failedTransaction:transaction];
        } else if (transaction.transactionState == SKPaymentTransactionStatePurchased ||
                   transaction.transactionState == SKPaymentTransactionStateRestored) {
            NSString *tId = transaction.originalTransaction ? transaction.originalTransaction.transactionIdentifier : transaction.transactionIdentifier;
            NSMutableArray *tArray = [dict objectForKey:tId];
            if (tArray) {
                [tArray addObject:transaction];
            } else {
                tArray = [NSMutableArray arrayWithObject:transaction];
                [dict setObject:tArray forKey:tId];
            }
        } else {
            NSLog(@"TransactionStateUnknown: %d", transaction.transactionState);
        }
    }
    for (NSMutableArray *array in [dict allValues]) {
        [array sortUsingComparator:^NSComparisonResult(SKPaymentTransaction *t1, SKPaymentTransaction *t2) {
            return [t2.transactionDate compare:t1.transactionDate];
        }];
        //NSLog(@"first date: %@, last date: %@", [[array objectAtIndex:0] transactionDate], [[array lastObject] transactionDate]);
        SKPaymentTransaction *t = [array objectAtIndex:0];  // We can also send the whole array to server if needed.
        switch (t.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [[self queue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(completeTransaction:) object:t]];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:t];
                break;
            case SKPaymentTransactionStateRestored:
                [[self queue] addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(restoreTransaction:) object:t]];
                break;
            default:
                NSLog(@"TransactionStateUnknown: %d", t.transactionState);
                break;
        }
        for (NSUInteger i = 1; i < [array count]; i++) {
            [[SKPaymentQueue defaultQueue] finishTransaction:[array objectAtIndex:i]];
        }
    }
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"payment queue restore completed transactions finished.");
    if (_delegate) {
        [_delegate restoreFinished];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"payment queue restore completed transactions failed with error %@.", error);
    if (_delegate) {
        [_delegate restoreFailed:error];
    }
}

- (void)finishTransaction:(SKPaymentTransaction *)transaction withStatus:(NSInteger)status {
    // Do not remove transaction if the status IS server unavailable.
    if (status == kStatusAPIServerUnavailable &&
        status == kStatusReceiptServerUnavailable) {
        return;
    }
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

// Your observer provides the product when the user successfully purchases an item.
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
	// Your application should implement these two methods.
    NSDate *date = nil;
    NSInteger status = [self verifyReceipt:transaction expireDate:&date];
    if (status == kStatusOK) [self provideContent:transaction expireDate:date];
    
	// Remove the transaction from the payment queue.
    [self finishTransaction:transaction withStatus:status];
}

// Finish the transaction for a restored purchase.
- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSDate *date = nil;
    NSInteger status = [self verifyReceipt:transaction expireDate:&date];
    if (status == kStatusOK) [self provideRestoredContent:transaction expireDate:date];
	
    // Remove the transaction from the payment queue.
    [self finishTransaction:transaction withStatus:status];
}

// Finish the transaction for a failed purchase.
- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    if (transaction.error.code != SKErrorPaymentCancelled) {
        // Optionally, display an error here.
		NSLog(@"%@ NG", transaction.payment.productIdentifier);
		if (_delegate) {
			[_delegate paymentFailed:transaction];
		}
    } else {
		NSLog(@"%@ Cancelled", transaction.payment.productIdentifier);
		if (_delegate) {
			[_delegate paymentCancelled:transaction];
		}
	}
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (NSInteger)verifyReceipt:(SKPaymentTransaction *)transaction expireDate:(NSDate **)expireDate {
	// do nothing in non-consumable mode
//    NSInteger status = [InAppPurchaseServerConnector saveTransaction:transaction expireDate:expireDate];
//    if (status != kStatusOK && _delegate && [_delegate respondsToSelector:@selector(verifyReceiptFailed:withError:)]) {
//        NSError *error = [NSError errorWithDomain:kIAPErrorDomain code:status userInfo:nil];
//        SKPaymentTransaction *t = (transaction.transactionState == SKPaymentTransactionStateRestored) ? transaction.originalTransaction : transaction;
//        NSInvocation *invocation = [NSObject invocationWithTarget:_delegate 
//                                                         selector:@selector(verifyReceiptFailed:withError:) 
//                                                  retainArguments:YES 
//                                                        arguments:&t, &error];
//        [invocation performSelectorOnMainThread:@selector(invoke) withObject:nil waitUntilDone:NO];
//    }
    return kStatusOK;
}

- (BOOL)provideContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate {
	NSLog(@"Purchsed %@ OK", transaction.payment.productIdentifier);
	if (_delegate) {
        NSInvocation *invocation = [NSObject invocationWithTarget:_delegate selector:@selector(provideContent:expireDate:) retainArguments:YES arguments:&transaction, &expireDate];
        [invocation performSelectorOnMainThread:@selector(invoke) withObject:nil waitUntilDone:YES];
        BOOL success = NO;
        [invocation getReturnValue:&success];
		return success;
	}
    return NO;
}

- (BOOL)provideRestoredContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate {
	NSLog(@"Restored %@ OK", transaction.originalTransaction.payment.productIdentifier);
	if (_delegate) {
        NSInvocation *invocation = [NSObject invocationWithTarget:_delegate selector:@selector(provideContent:expireDate:) retainArguments:YES arguments:&transaction, &expireDate];
        [invocation performSelectorOnMainThread:@selector(invoke) withObject:nil waitUntilDone:YES];
        BOOL success = NO;
        [invocation getReturnValue:&success];
		return success;
	}
    return NO;
}

@end
