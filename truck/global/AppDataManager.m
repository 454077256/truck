//
//  AppDataManager.m
//  Lohas
//
//  Created by 洪湃 on 15-7-1.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "AppDataManager.h"
#import "SBJson.h"
#import "UIImage+Extras.h"

@implementation AppDataManager

+(void)setNewsstandIconImage{
//    
//    UIApplication *sharedApplication = [UIApplication sharedApplication];
//    if (![sharedApplication respondsToSelector:@selector(setNewsstandIconImage:)]) {
//        return;
//    }
//    
//    NSString *url = @"http://api.ilohas.com/newsstand_image";
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        NSError *error = NULL;
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
//        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
//        
//        NSMutableDictionary *dataDictionary = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] JSONValue];
//        
//        if (dataDictionary==nil) {
//            return;
//        }
//        
//        NSString *imageURL = [dataDictionary objectForKey:@"content"];
//        if (imageURL==nil) {
//            return;
//        }
//        
//        if ([imageURL isEqualToString:[[AppDataConfig appData] objectForKey:@"newstandIcoImage"]]) {
//            return;
//        }
//        
//        [[AppDataConfig appData] setObject:imageURL forKey:@"newstandIcoImage"];
//        [AppDataConfig reSave];
//        
//        NSMutableURLRequest *request2 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
//        
//        NSData *imgdata = [NSURLConnection sendSynchronousRequest:request2 returningResponse:nil error:&error];
//        
//        UIImage *dataImage = [UIImage imageWithData:imgdata];
//        
//        UIImage *smallImage = [UIImage image:dataImage fitInSize:CGSizeMake(512, 512)];
//        
//        [sharedApplication setNewsstandIconImage:smallImage];
//        
//        trace(@"%@",dataImage);
//    });
//    
//    
    
    
    
    
    
    
    
    
    
//    if (!responseData || [responseData length] == 0) {
//        NSLog(@"Check newsstand cover failed.");
//    } else {
//        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//        NSLog(@"Newsstand cover json: %@", jsonString);
//        //        jsonString = @"{\"cover\":\"http://www.rakutec.com/adhoc/Newsstand_512.png\",\"updated\":\"1342779732\"}";  // dummy JSON for test.
//        NSDictionary *dict = [jsonString JSONValue];
//        if (!dict ||
//            ![dict objectForKey:@"updated"] || ![[dict objectForKey:@"updated"] respondsToSelector:@selector(doubleValue)] ||
//            ![dict objectForKey:@"cover"]) {
//            NSLog(@"Bad Json");
//            return;
//        }
//        NSString *lastUpdateDateKey = [NSString stringWithFormat:@"LastNewsstandCoverUpdatedAt_%@", [[AppDefaults defaults] appVersion]];
//        NSTimeInterval updatedTime = [[dict objectForKey:@"updated"] doubleValue];
//        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
//        NSTimeInterval lastCoverUpdatedAt = [defs doubleForKey:lastUpdateDateKey];
//        if (lastCoverUpdatedAt == 0.0) {
//            [[[defs dictionaryRepresentation] allKeys] enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop){
//                if ([key rangeOfString:@"LastNewsstandCoverUpdatedAt" options:NSCaseInsensitiveSearch].location != NSNotFound) {
//                    [defs removeObjectForKey:key];
//                }
//            }];
//        }
//        if (lastCoverUpdatedAt > 0.0 && updatedTime <= lastCoverUpdatedAt) {
//            NSLog(@"No updated cover found.");
//            return;
//        }
//        id coverURL = [dict objectForKey:@"cover"];
//        NSLog(@"Cover URL: %@", coverURL);
//        UIImage *image = nil;
//        if ([coverURL isKindOfClass:[NSString class]]) {
//            responseData = [self performGetRequestWithURL:coverURL];
//            if (!responseData || [responseData length] == 0) {
//                NSLog(@"Bad cover file URL.");
//                return;
//            }
//            image = [UIImage imageWithData:responseData];
//            if (!image) {
//                NSLog(@"Bad image file.");
//                return;
//            }
//        } else {
//            NSLog(@"Back to original cover");
//        }
//        [sharedApplication setNewsstandIconImage:image];
//        [defs setDouble:updatedTime forKey:lastUpdateDateKey];
//        NSLog(@"Successfully updated newsstand cover");
//    }
}

@end
