//
//  InAppStoreObserver.h
//  OneTap
//
//  Created by xuzhe on 09/12/11.
//  Copyright 2009 Rakuraku Technologies, Inc.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol InAppPurchaseObserverDelegate <NSObject>
@required
- (BOOL)provideContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate;
- (BOOL)provideRestoredContent:(SKPaymentTransaction *)transaction expireDate:(NSDate *)expireDate;
- (void)paymentFailed:(SKPaymentTransaction *)transaction;
- (void)paymentCancelled:(SKPaymentTransaction *)transaction;
- (void)restoreFinished;
- (void)restoreFailed:(NSError *)error;
- (void)verifyReceiptFailed:(SKPaymentTransaction *)transaction withError:(NSError *)error;
@end

@interface InAppPurchaseObserver : NSObject <SKPaymentTransactionObserver>

@property (unsafe_unretained, nonatomic) id<InAppPurchaseObserverDelegate> delegate;
@end
