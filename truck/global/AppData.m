//
//  ApData.m
//  OuRuiDA
//
//  Created by 洪湃 on 15/12/17.
//  Copyright © 2015年 洪湃 454077256. All rights reserved.
//

#import "AppData.h"

@implementation AppData


static AppData * appData;
static NSMutableDictionary *eeData;

+(AppData *)shareInstance{
    if (appData == nil) {
        appData = [[AppData alloc] init];
    }
    return appData;
}


+(NSMutableDictionary *)eeData{
    if (eeData==nil) {
        eeData = [NSMutableDictionary dictionaryWithContentsOfFile:EE_Config_Path];
        if (eeData==nil) {
            eeData = [NSMutableDictionary dictionary];
        }
    }
    return eeData;
}


+(void)save{
    if (eeData!=nil) {
        BOOL isOk = [eeData writeToFile:EE_Config_Path atomically:YES];
        if (!isOk) {
            traceError(@"程序配置文件写入失败\n 数据：%@",appData);
        }
    }
}

#pragma mark -- 配置存储操作
+(void)setData:(NSString *)topKey key:(NSString *)keyString value:(id)valueString{
    NSMutableDictionary *topVlue = [AppData eeData][topKey];
    if(topVlue==nil){
        topVlue = [NSMutableDictionary dictionary];
    }
    
    topVlue[keyString] = valueString;
    
    [AppData eeData][topKey] = topVlue;
    [AppData save];
}

+(id)getData:(NSString *)topKey key:(NSString *)keyString{
    id value = [AppData eeData][topKey][keyString];
    return value;
}


#pragma mark -- custom
#pragma mark -- 用户操作
+(Boolean)checkLogin{
    return  [[AppData getData:EE_AppConfig key:@"userIsLogined"] boolValue];
}


+(NSString *)getUser:(UserInfor)tag{
    if(tag==UserInfor_udid){
        return [AppData getData:EE_AppConfig key:@"userinfor"][@"uuid"];
    }else if(tag==UserInfor_username){
        return [AppData getData:EE_AppConfig key:@"userinfor"][@"driverTel"];
    }
    return @"";
}

+(NSDictionary *)getUserInfor{
    return [AppData getData:EE_AppConfig key:@"userinfor"];
}












#pragma mark -- app 专有代码
+(TaskState)taskState:(NSDictionary *)data{
    NSArray *items = data[@"workOrderItems"];
    if(items>0){
        NSDictionary *item = items[0];
        NSString *returnDate = NotNull(item[@"returnDate"]);
        if(returnDate.length>0){
            return  TaskState_compelte;
        }
        
        NSString *startDt = NotNull(item[@"workOrderItemRel"][@"driverDeparture"][@"startDt"]);
        if(startDt.length>0){
            return TaskState_doing;
        }else{
            return TaskState_waiting;
        }
    }
    return TaskState_unknown;
    // returnDate = "2016-11-03 19:21"; 不为空 已完成
    
    //workOrderItemRel driverDeparture startDt 不为空 进行中
    //workOrderItemRel driverDeparture startDt 为空 待执行
}




@end
