//
//  EEDownloadManager.h
//  OuRuiDA
//
//  Created by 洪湃 on 16/6/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EEOneEvent.h"
#import "LDProgressView.h"

@interface EEDownloadManager : NSObject

@property(nonatomic,retain)NSMutableArray *eventsArray;

+(EEDownloadManager*) shareInstance;

+(void)addOneDownloadTask_webpath:(NSString *)webFilePath savePath:(NSString *)savePath delegate:(id)delegate data:(NSDictionary *)data isToFirst:(BOOL)isToFirst progressView:(LDProgressView *)progress;

//删除一个task 
+(void)removeOneDownloadTaskByWebURL:(NSString *)webURL;
+(void)removeOneDownloadTaskByOneEvent:(EEOneEvent *)oneEvent;
    

+(void)printInfor;

+(void)startQueue;
@end
