
//
//  ImageBrowser.m
//  Lohas
//
//  Created by 洪湃 on 16/5/18.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "SBJson.h"
#import "SVProgressHUD.h"
#import "UIImage+Extras.h"


#import "EEImageBrowser.h"

@implementation EEImageBrowser

-(void)initView{
    [self setBackgroundColor:CBlack];
    
    hud = [Global createHud:CGSizeMake(SS(60), SS(60)) center:ccp(self.width*0.5, self.height*0.5) style:(UIActivityIndicatorViewStyleWhiteLarge)];
    [self addSubview:hud];
    
    NSString *imagePath = [self.eeData objectForKey:@"image"];
    contentImageView = [Global createImageEGO:imagePath size:self.bounds.size center:ccp(self.width*0.5, self.height*0.5)];
    contentImageView.delegate = self;
    //
    if (contentImageView.image) {
        [self createScrollViews];
        [hud removeFromSuperview];hud=nil;
    }
    
//    
//    [self addSubview:[Global createBtn:R(P(250,660), P(20,23), P(60,80), P(25,34)) normal:@"assets/btnjumpbg.png" down:nil title:@"关闭" col:CWhite colS:CGray size:P(12,16) target:self action:@selector(eeRemove)]]  ;
    
    
        //轻点效果
        reloadTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickStage:)];
        reloadTapGesture.numberOfTapsRequired = 1;
        reloadTapGesture.numberOfTouchesRequired = 1;
        [self addGestureRecognizer:reloadTapGesture];
}


-(void)clickStage:(id)sender{
    [self eeRemove];
}

-(void)createScrollViews{
    CGSize newsize = [UIImage fitSize:contentImageView.image.size inSize:self.bounds.size];
    [contentImageView setFrame:R(0, 0, newsize.width, newsize.height)];
    
    
    scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    [scrollView addSubview:contentImageView];
    contentImageView.center = ccp(scrollView.width*0.5, scrollView.height*0.5);
    
    scrollView.delegate = self;
    scrollView.multipleTouchEnabled = YES;
    scrollView.minimumZoomScale = 1.0;
    scrollView.maximumZoomScale = 2.0;
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.contentSize = contentImageView.frame.size;
    
    [self addSubview:scrollView];
    [self setBackgroundColor:CBlack];
    
    
    
    
}

- (void)scrollViewDidZoom:(UIScrollView *)aScrollView
{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    contentImageView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                          scrollView.contentSize.height * 0.5 + offsetY);
}


-(void)imageViewLoadedImage:(EGOImageView *)imageView{
    //    UIImage *img = imageView.image;
    
    [self createScrollViews];
    [hud removeFromSuperview];hud=nil;
}
//
//-(void)resetContentSizeByImgData:(UIImage *)img{
//    CGSize imgsize = img.size;
//    CGSize framesize;
//    if (imgsize.width>=imgsize.height) {
//        framesize = CGSizeMake(imgsize.width, imgsize.width*self.height/self.width);
//    }else{
//        framesize = CGSizeMake(imgsize.height, imgsize.height*self.width/self.height);
//    }
////
////    CGFloat maxwidth = (img.size.width>=img.size.height)?img.size.width:img.size.height;
////
////    CGSize newsize = [UIImage fitSize:img.size inSize:CGSizeMake(maxwidth, maxwidth)];
//
//
//    UIGraphicsBeginImageContext(framesize);
//
//    CGRect rect = CGRectMake((framesize.width-img.size.width)*0.5, (framesize.height-img.size.height)*0.5, img.size.width, img.size.height);
//
//    [img drawInRect:rect];
//
//    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    [contentImageView setFrame:self.bounds];
//    contentImageView.image = newimg;
//    [contentImageView setContentMode:(UIViewContentModeScaleAspectFit)];
//
//    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    scrollView.contentSize = contentImageView.frame.size;
//}

//下面的绝对不能少
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    trace(@"%@",[NSValue valueWithCGSize:scrollView.contentSize]);
    
    return contentImageView;
}



//    NSDictionary *data = @{@"id":[self.eeData objectForKey:@"item_id"],@"page_id":[self.eeData objectForKey:@"page_id"]};
//    [AppUIFactory open:@"MagOnePageImageShow" frame:frameUI data:data parent:RootView action:3];
//
//-(void)initView{
//    [self setBackgroundColor: CWhite];
//
//    [self addSubview:[Global createHud:CGSizeMake(PP(25, 30), PP(25, 30)) center:ccp(self.width*0.5, self.height*0.5) style:(UIActivityIndicatorViewStyleGray)]];
//
//    EGOImageView *content = [Global createImageEGO:[self.eeData objectForKey:@"image"] frame:self.bounds];
//    [content setBackgroundColor:CClear];
//    [self addSubview:content];
//
//    [self addSubview:[Global createBtn:R(P(250,660), P(20,23), P(60,80), P(25,34)) normal:@"assets/btnjumpbg.png" down:nil title:@"关闭" col:CWhite colS:CGray size:P(12,16) target:self action:@selector(eeRemove)]]  ;
//
//    
//}




@end
//