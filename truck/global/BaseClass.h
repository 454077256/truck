//
//  BaseClass.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^eeBlock0) ();
typedef void(^eeBlock1) (id postResult);
typedef void(^eeBlock2) (id postResult,id post);




@interface BaseClass : NSObject

@end

typedef enum {
    ActionType_fromLeft,
    ActionType_fromRight,
    ActionType_fromTop,
    ActionType_fromDown,
    ActionType_none
}ActionType;

@interface EEView : UIView{
    BOOL isRemoveAfterAction;
    UIActivityIndicatorView *hud;
}

@property(nonatomic,strong)NSDictionary *eeData;
@property(nonatomic,strong)NSArray *eeList;

-(void)initView;
-(void)initView:(CGRect)frame;
-(void)startDataRequest;

+(id)instance;
+(id)instanceByFrame:(CGRect)frame;

+(id)instanceByData:(id)configData;
+(id)instanceByFrame:(CGRect)frame data:(NSDictionary *)configData;

-(void)eeRemove;
-(void)eeRemoveLeft;
-(void)eeRemoveRight;
-(void)eeRemoveTop;
-(void)eeRemoveDown;

- (void) addTransition:(ActionType)type;

-(void)addAnimation:(int)type;

-(void)addHud;
-(void)removeHud;


@end


@interface EECell : UITableViewCell{
    CGSize eecellSize;
}

@property(nonatomic,strong)id eeData;

@property(nonatomic)float twidth;

-(void)loadData:(NSDictionary *)config size:(CGSize)size indexPath:(NSIndexPath *)indexpath;
-(void)loadData:(NSDictionary *)configData size:(CGSize)size;

-(instancetype)initWithData:(id)data;
- (instancetype)initWithData:(id)data size:(CGSize)size;
-(void)initView;



@end


@interface EEButton : UIButton
@property(nonatomic,strong)id eeData;
@property (nonatomic,strong) eeBlock1 block_1;
@end

@interface EELabel : UILabel
@end

@interface EEScrollView : UIScrollView
@end

@interface EEImageView : UIImageView
@end

@interface EETextFiled : UITextField<UITextFieldDelegate>
@end

@interface EETextView : UITextView;
@end

@interface EEControl : UIControl{
    UIView *maskView;
}
@property(nonatomic)BOOL isShowDarkHighLight;

@property(nonatomic,strong) eeBlock1 block_1;

@end