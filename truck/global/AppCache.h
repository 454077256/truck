//
//  AppCache.h
//  Lohas
//
//  Created by 洪湃 on 15-3-8.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface AppCache : EEView

+(NSMutableDictionary *)EE_LocalData_MagazineData_;
+(NSMutableDictionary *)EE_LocalData_MagazineData_getOneMagContentByKey:(NSString *)key;
+(void)EE_LocalData_MagazineData_update:(NSDictionary *)oneData key:(NSString *)key;


+(BOOL)EE_LocalData_MagazineDownloaded_updateByMagId:(NSString *)magId pageKey:(NSString *)key;


    
    
//下载的图片
+(NSString *)getHashName:(NSString *)imgWebPath;
+(BOOL)isMagWebPathExistInLocalCache:(NSString *)imgWebPath;
+(BOOL)saveMagazineImage:(NSData *)imgWebData imageURL:(NSString *)webPath;

//文章
+(NSDictionary *)getLocalWebArticleData:(NSString *)webPath;
+(BOOL)saveWebArticleToLocal:(NSDictionary *)htmlData webPath:(NSString *)webPath;
+(NSMutableArray *)getTodayList;
+(NSMutableArray *)getSeasonList;

+(NSString *)getOpenImagePath;
+(UIImageView *)getOpenImageView;
+(BOOL)saveNewOpenImage:(NSData *)imgData config:(NSDictionary *)openConfig;

//收藏
+(NSMutableArray *)getFavoriteList;
+(void)updateFavoriteDataToLocal_newArray :(NSMutableArray *)newFavoriteList type:(NSString *)type;
+(void)deleLocalFavoriteById:(NSString *)sid type:(NSString *)type page:(NSString *)page;

+(BOOL)isArticleInLocalFavorite:(NSInteger)artId;
+(BOOL)isMagazineInLocalFavorite:(NSInteger)magId pageKey:(NSInteger)pagekey;
+(void)addArticleInLocalFavorite:(NSString *)articleId title:(NSString *)title;
+(void)addMagazineInLocalFavorite:(NSString *)magId pageid:(NSString *)pid title:(NSString *)title;
+(void)saveFavoriteData:(NSArray *)configList;
@end
