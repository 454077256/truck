//
//  AppTools.h
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"

@interface AppTools : NSObject

+(void)mergerArray:(NSMutableArray *)_oldArray newArray:(NSArray *)_newArray toEnd:(BOOL)_toEnd;

@end
