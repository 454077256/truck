//
//  Extensions.h
//  iMW
//
//  Created by 徐 哲 on 1/6/12.
//  Copyright (c) 2012 Rakuraku Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RETURN_NIL_IF_NSNULL(o) o == [NSNull null] ? nil : o
#define RETURN_DEFAULT_IF_NIL(options, k, d) [options valueForKey:k] ? [options valueForKey:k] : d;
#define kSystemCacheFolderPath  NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)

@interface UIImage (INRoundedCornerShadow)
+ (UIImage *)imageWithRoundedCorners:(UIImage *)inputImage cornerHeight:(float)ch cornerWidth:(float)cw;
+ (UIImage *)roundedCornerImageRect:(CGRect)rect width:(float)cw;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize ratio:(double)r offset:(CGPoint)offset;
+ (UIImage *)imageWithUIView:(UIView *)view;
@end

@interface UIColor (WebHexColor)
+ (UIColor *)convertWebRGBColor:(NSString*)webHexColor;
@end

@interface UIFont (FontSizeAutoSelect)
+ (UIFont *)fontSizeAutoSelect:(NSString *)string
					   useFont:(UIFont *)font
				   maxFontSize:(CGFloat)max
				  miniFontSize:(CGFloat)min
			 constrainedToSize:(CGSize)size
				 lineBreakMode:(UILineBreakMode)lineBreakMode;
@end


#ifdef NS_BLOCKS_AVAILABLE
@interface NSString (UnitConverter)
/*
 ===============HOW TO USE================
 Example:
 =========================================
 
 NSString *(^block)(NSArray *) = ^NSString *(NSArray *array) {
 NSArray *units = [NSArray arrayWithObjects:@"W", @"D", @"H", @"M", @"S", nil];
 NSString *rtnStr = @"";
 NSUInteger i = 0;
 for (NSNumber *number in array) {
 float unitNumber = [number floatValue];
 if (unitNumber < 1.0f) {
 if (i == [units count] - 1 && [rtnStr length] == 0) {
 return [NSString stringWithFormat:@"%d%@", (NSInteger)floorf(unitNumber), [units objectAtIndex:i]];
 }
 i++;
 continue;
 }
 rtnStr = [NSString stringWithFormat:@"%@%d%@", rtnStr, (NSInteger)floorf(unitNumber), [units objectAtIndex:i]];
 i++;
 }
 return rtnStr;
 };
 
 NSArray *array = [NSArray arrayWithObjects:[NSNumber numberWithUnsignedLongLong:7],
 [NSNumber numberWithUnsignedLongLong:24],
 [NSNumber numberWithUnsignedLongLong:60],
 [NSNumber numberWithUnsignedLongLong:60], nil];
 NSLog(@"%@", [NSString stringByConvertUnsignedLongLongNumber:3620 withBlock:block withBehaviors:array]);
 NSLog(@"%@", [NSString stringByConvertUnsignedLongLongNumber:3601 withBlock:block withBehaviors:array]);
 NSLog(@"%@", [NSString stringByConvertUnsignedLongLongNumber:86400 withBlock:block withBehaviors:array]);
 NSLog(@"%@", [NSString stringByConvertUnsignedLongLongNumber:86464 withBlock:block withBehaviors:array]);
 NSLog(@"%@", [NSString stringByConvertUnsignedLongLongNumber:691200 withBlock:block withBehaviors:array]);
 NSLog(@"%@", [NSString stringByConvertUnsignedLongLongNumber:36 withBlock:block withBehaviors:array]);
 
 >>>>>>>>> Output:
 1H20S
 1H1S
 1D
 1D1M4S
 1W1D
 36S
 */

+ (NSString *)stringByConvertUnsignedLongLongNumber:(unsigned long long)number withBlock:(NSString *(^)(NSArray *array))block withBehaviors:(NSArray *)behaviors;
@end
#endif

@interface NSString (MD5)

- (NSString *)md5;

@end

@interface NSDate (relative)

- (NSString *)relativeDescription;
- (NSString *)timeAgo;
- (NSString *)timeAgoInChinese;

@end

@interface NSObject (RTPerform)
- (void)performSelectorOnMainThread:(SEL)aSelector withObject:(id)arg1 withObject:(id)arg2 waitUntilDone:(BOOL)wait;
- (void)performSelectorInBackground:(SEL)aSelector withObject:(id)arg1 withObject:(id)arg2;

- (void)performSelectorOnMainThread:(SEL)aSelector waitUntilDone:(BOOL)wait withObjects:(id)arg, ...;
- (void)performSelectorInBackground:(SEL)aSelector withObjects:(id)arg, ...;

- (void)performSelector:(SEL)aSelector afterDelay:(NSTimeInterval)delay withObject:(id)arg,...;

+ (NSInvocation*)invocationWithTarget:(id)target
                             selector:(SEL)aSelector
                      retainArguments:(BOOL)retainArguments
                            arguments:(void *)args, ...;
@end

@interface NSArray (Shuffle)
- (NSMutableArray *)shuffle;
@end


@interface UIDevice (IdentifierAddition)

- (NSString *)uniqueDeviceIdentifier;
- (NSString *)uniqueGlobalDeviceIdentifier;
- (NSString *)macaddress;
@end
