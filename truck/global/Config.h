
#import "BaseClass.h"
#import "UIView+EEExtras.h"

#define InvalidTrace  1
#define logLevelTrace  1 //infor:1    debug:2    Error:3
#define is_show_data_exception_log YES //设置当程序数据出现错误的时候界面弹出  错误的信息。


//
//
////在真机中是否输出日志YES NO
//#define is_open_log_in_real_machine YES  // YES:在真机上的话，NSLog会保存如文件，不在控制台输出
//
//

#ifdef InvalidTrace //----------------------------

#define trace(fmt, ...) do {                                            \
NSLog((@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__); \
}while(0)
#  define track() NSLog(@"%s", __func__)

#else//--------------------------------------------

#define trace(fmt, ...)
#define track()

#endif//-------------------------------------------




#define traceInfor(fmt, ...) do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__];\
[[Global sharedGlobal] log:discript level:1] ;}while(0)\

#define traceDebug(fmt, ...) do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__] ;\
[[Global sharedGlobal] log:discript level:2] ;}while(0)\

#define traceError(fmt, ...) do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__] ;\
[[Global sharedGlobal] log:discript level:3] ;}while(0)\

#define traceException(fmt, ...) do{ \
NSArray *syms = [NSThread  callStackSymbols];\
NSString* discript = [[NSString alloc] initWithFormat:(@"<%s(%d)> " fmt), __func__, __LINE__, ##__VA_ARGS__] ;\
[[Global sharedGlobal] log:discript level:2] ;\
if ([syms count] > 1) {\
for (int i=0;i<[syms count];i++) {\
NSString *dis = [NSString stringWithFormat:@"<%@ %p> %@ - caller: %@ ", [self class], self, NSStringFromSelector(_cmd),[syms objectAtIndex:i]];\
[[Global sharedGlobal] log:dis level:4] ;\
}\
} else {\
NSString *dis = [NSString stringWithFormat:@"<%@ %p> %@", [self class], self, NSStringFromSelector(_cmd)];\
[[Global sharedGlobal] log:dis level:4] ;\
}\
}while(0)\

#define traceFoot() do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) "), __func__, __LINE__] ;\
[[Global sharedGlobal] log:discript level:3] ;}while(0)\



//是真机还是模拟器
#if TARGET_IPHONE_SIMULATOR
#define SIMULATOR 1
#elif TARGET_OS_IPHONE
#define SIMULATOR 0
#endif


//程序唤醒的时候请求网络，检测更新
//#define CheckUpdateTimeGap 86400*0.5
#define CheckUpdateTimeGap 3600


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define M_PhoneType [Global sharedGlobal].phoneType
#define Pad_Phone(v1,v2) [[Global sharedGlobal] pad_phone:v1 :v2];
#define p5_6_6p(v1,v2,v3) [[Global sharedGlobal] p5_6_6p:v1 :v2 :v3];

//是否是iphone5
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//系统消息提示宏
#define EEAlert(str) [[Global sharedGlobal] alert:str]


#define DeviceW     [[UIScreen mainScreen] currentMode].size.width
#define DeviceH     [[UIScreen mainScreen] currentMode].size.height

#define S(postHeight) [Global multiHeight:postHeight]
#define SS(postHeight) (0.5*[Global multiHeight:(postHeight)])
#define P(phone,pad) ([Global multiHeightPhone:phone WithPad:pad])
#define PP(phone,pad) (0.5*[Global multiHeightPhone:phone WithPad:pad])

#define PPInt(phone,pad) (int)(0.5*[Global multiHeightPhone:phone WithPad:pad])
#define AA(p5,p6,p6p,pad) ([Global multiHeightAA:p5 :p6 :p6p: pad])

#define App_Size    [Global sharedGlobal].appSize

#define Content_Width  [[Global sharedGlobal] getContentWidthPadAndPhone]
#define Content_Height [[Global sharedGlobal] getContentHeightPadAndPhone]

#define IosAppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//CGSizeMake([[UIScreen mainScreen] currentMode].size.width/[[UIScreen mainScreen] scale],[[UIScreen mainScreen] currentMode].size.height/[[UIScreen mainScreen] scale]-StatusHeight)

#define SystemVersion [[[UIDevice currentDevice] systemVersion] floatValue]


#define EE_LocalPlist_Today  [NSString stringWithFormat:@"%@/Library/Caches/EE_LocalData_Today.plist",NSHomeDirectory()]
#define EE_LocalPlist_Season [NSString stringWithFormat:@"%@/Library/Caches/EE_LocalData_Season.plist",NSHomeDirectory()]

#define EE_LocalData_MagazineCoverList [NSString stringWithFormat:@"%@/Library/Caches/EE_LocalData_MagazineCoverList.plist",NSHomeDirectory()]
#define EE_LocalData_MagazineData [NSString stringWithFormat:@"%@/Library/Caches/EE_LocalData_MagazineData.plist",NSHomeDirectory()]

#define EE_LocalData_MagazineDownloaded [NSString stringWithFormat:@"%@/Library/Caches/EE_LocalData_MagazineDownloaded.plist",NSHomeDirectory()]

#define EE_CacheMagazineDictionary [NSString stringWithFormat:@"%@/Library/Caches/magcache",NSHomeDirectory()]
#define EE_CacheHtmlDictionary [NSString stringWithFormat:@"%@/Library/Caches/html",NSHomeDirectory()]

#define EE_CacheOpenImagePlist [NSString stringWithFormat:@"%@/Library/Caches/EE_CacheOpenImagePlist.plist",NSHomeDirectory()]
#define EE_CacheOpenImageData [NSString stringWithFormat:@"%@/Library/Caches/OpenData",NSHomeDirectory()]

#define EE_FavoritePlist [NSString stringWithFormat:@"%@/Library/Caches/EE_FavoritePlist.plist",NSHomeDirectory()]



#define EE_CacheMagazineHistoryPage [NSString stringWithFormat:@"%@/Library/Caches/EE_Magazine_HistoryList.plist",NSHomeDirectory()]


//每日推荐列表缓存
#define EE_TodayPlist [NSString stringWithFormat:@"%@/Library/Caches/EE_TodayPlist.plist",NSHomeDirectory()]
//节气令列表缓存
#define EE_SeasonPlist [NSString stringWithFormat:@"%@/Library/Caches/EE_SeasonPlist.plist",NSHomeDirectory()]








#define EE_SqlitePath [NSString stringWithFormat:@"%@/Library/Caches/appData/APPDATA.db",NSHomeDirectory()]

#define EE_homePath [NSString stringWithFormat:@"%@/Library/Caches/appData",NSHomeDirectory()]



//Documents文件夹
#define EE_DocumentsPath [NSString stringWithFormat:@"%@/Documents",NSHomeDirectory()]


//系统配置信息文件
#define EE_Config_Path [NSString stringWithFormat:@"%@/Library/Caches/Config.plist",NSHomeDirectory()]


#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)




#define ee_AppSize [[UIScreen mainScreen] bounds].size
#pragma mark - 颜色值

#define color_noraml_black         0x121212
#define color_noraml_gray         0x808080
#define color_txt_gray [UIColor colorWithRed:165*1.0/255 green:171*1.0/255 blue:174*1.0/255 alpha:1]
#define RGB(r,g,b) [UIColor colorWithRed:r*1.0/255 green:g*1.0/255 blue:b*1.0/255 alpha:1]


#define color_pageControl_circle 0x4F494C;

#define color_app_black 0x191919

#define CWhite  [UIColor whiteColor]
#define CGray   [UIColor grayColor]
#define CRed    [UIColor redColor]
#define CBlack  [UIColor blackColor]
#define CClear  [UIColor clearColor]
#define CRandom  RGB(arc4random()%255, arc4random()%255, arc4random()%255)
#define CHex(posthex) [Global colorWithHex:posthex]



#define R(x, y, width, height) CGRectMake(x, y, width, height)
#define RH(x, y, width, height) CGRectMake(x*0.5, y*0.5, width*0.5, height*0.5)
#define CGRectMakeH(x, y, width, height) CGRectMake(x*0.5, y*0.5, width*0.5, height*0.5)


#define ccpSS(x,y) ccp(SS(x),SS(y))
#define RSS(x, y, width, height) R(SS(x), SS(y), SS(width), SS(height))


#define nextY(preView,gap) preView.y+preView.height+gap

#define appNotice @"appnoticetag"

#define center(v) CGPointMake(v.width*0.5,v.height*0.5)
#define ccp(x,y) CGPointMake(x,y)
#define ccph(x,y) CGPointMake(x*0.5,y*0.5)
#define size2Point05(size) CGPointMake(size.width*0.5,size.height*0.5)
#define point2Point05(point) CGPointMake(point.x*0.5,point.y*0.5)

#define YesNumber [NSNumber numberWithBool:YES]
#define NoNumber [NSNumber numberWithBool:NO]

#define NullPoint CGPointMake(-1,-1)
#define NotNull(str) ((str==[NSNull null] || str==nil)?@"":str)


#define NewMutableArray [NSMutableArray array]
#define NewMutableDictionary [NSMutableDictionary dictionary]

#define MapNotice @"mapnotice"
#define kBeaconUUID @"00000000-0000-0000-0000-000000000000"

#pragma mark - UI
#define RootView [Global sharedGlobal].rootController.view


#pragma mark --  custom

#define MenuBili 0.81
#define StatusHeight 20
#define AppButtomHeight 88
#define topbarheight 44


#define frameUI R(0,0,App_Size.width,App_Size.height)

#define EE_AppConfig @"EE_AppConfig"

#define color_Orange         CHex(0xFBC145)

//#define WebHost @"http://10.134.18.98:8080/truck-app/"
#define WebHost @"http://www.epanasia.com/truck-app/"
#define WebURL(x)    ([NSString stringWithFormat:@"%@%@",WebHost,x])



