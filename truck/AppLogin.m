//
//  AppLogin.m
//  truck
//
//  Created by 洪湃 on 16/11/25.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "AppLogin.h"
#import "URequest.h"
#import "Tools_math.h"
#import "SVProgressHUD.h"

@implementation AppLogin


-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //导航条
    EETopBar *topbar = [EETopBar instanceBy_normalLeft:nil
                                            selectLeft:nil
                                            leftAction:nil
                                          leftDelegate:nil
                                           normalRight:nil
                                           selectRight:nil
                                           rightAction:nil
                                         rightDelegate:self
                                                center:@"登录" isIOS7Lagger:YES];
    [topbar setBackgroundColor:CHex(0x1BA2E7)];
    [self addSubview:topbar];
    
    [self addSubview:[Global createImage:@"assets/logo1.png" center:ccp(SS(320), SS(212))]];
    
    
    [self addSubview:tmpView = [Global createViewByFrame:R(SS(12), SS(326), SS(616), SS(78)) color:CWhite]];
    [Global addBorder:tmpView borderWidth:1 color:CHex(0xA9A9A9) radius:S(6)];
    tf_username = [Global createTextFiled:R(SS(50), 0, tmpView.width-SS(50), tmpView.height) size:S(14) color:CBlack];
    tf_username.text = @"13918202169";
    [tmpView addSubview:tf_username];
    tf_username.placeholder = @"请输入用户名";
    
    
    [self addSubview:tmpView = [Global createViewByFrame:R(SS(12), SS(424), SS(616), SS(78)) color:CWhite]];
    [Global addBorder:tmpView borderWidth:1 color:CHex(0xA9A9A9) radius:S(6)];
    tf_userpass = [Global createTextFiled:R(SS(50), 0, tmpView.width-SS(50), tmpView.height) size:S(14) color:CBlack];
    [tmpView addSubview:tf_userpass];
    tf_userpass.placeholder = @"请输入密码";
    tf_userpass.text = @"a";
    
    [self addSubview:[Global createImage:@"assets/icouser.png" center:ccp(SS(36), SS(366))]];
    [self addSubview:[Global createImage:@"assets/icolock.png" center:ccp(SS(36), SS(466))]];
    
    checkBtn = [Global createBtn:R(SS(6), SS(520), SS(56), SS(56)) normal:@"assets/check1.png" down:@"assets/check2.png" target:self action:@selector(clickCheckBtn:)];
    [self addSubview:checkBtn];
    
    [self addSubview:[Global createBtn:R(checkBtn.x+checkBtn.width, checkBtn.y, SS(150), checkBtn.height) normal:nil down:nil title:@"记住用户名" col:CBlack colS:CGray size:S(14) target:self action:@selector(clickCheckBtn:)]];
    
    [self addSubview:[Global createBtn:R(SS(504), SS(532), SS(120), SS(30)) normal:nil down:nil title:@"忘记密码" col:CHex(0x1BA2E7) colS:CGray size:S(12) target:self action:@selector(clickForgetPass:)]];
    
    [self addSubview:tmpButtom = [Global createBtnColor:R(SS(12), SS(596), SS(614), SS(68)) bgcolor:CHex(0xFC9827) title:@"登 录" titleNormal:CWhite titleSelect:CGray size:S(16) delegate:self action:@selector(clickLogin:)]];
    [Global addBorder:tmpButtom borderWidth:1 color:CHex(0xFC9827)  radius:S(4)];
}


-(void)clickCheckBtn:(EEButton *)btn{
    track();
}


-(void)clickForgetPass:(EEButton *)btn{
    track();
}


-(void)clickLogin:(EEButton *)btn{
    track();
    NSString *username = tf_username.text;
    NSString *userpass = tf_userpass.text;
    NSString *udid = @"0aergdfgsa0aergdfgsa0aergdfgsa0aergdfgsa";
    NSString *urlstring = WebURL(@"appLogin");
    
    NSDictionary *postDictionary = @{@"url":urlstring,@"driverTel":username,@"device":udid,@"password":userpass};
    
    [[[URequest alloc] init] start:postDictionary begin:^{
        [SVProgressHUD show];
    } complete:^(id postResult) {
        trace(@"%@",postResult);
        if([postResult[@"code"] intValue]==200){
            NSMutableDictionary *newConfig = [NSMutableDictionary dictionaryWithDictionary:postResult[@"data"]];
            [Global filterNullValue:newConfig];
            trace(@"%@",newConfig);
           
            //用户信息储存到本地
            [AppData setData:EE_AppConfig key:@"userinfor" value:newConfig];
            //登陆状态
            [AppData setData:EE_AppConfig key:@"userIsLogined" value:@"1"];
            
            [self performSelector:@selector(noticeLoginSuccess) withObject:nil afterDelay:1];
            
            [SVProgressHUD dismissWithSuccess:@"登录成功" afterDelay:1];
        }else{
            
            [SVProgressHUD dismissWithError:postResult[@"message"] afterDelay:1];
        }
    } failed:^(id postResult) {
        trace(@"%@",postResult);
        [SVProgressHUD dismissWithError:@"登录失败" afterDelay:1];
    }];
}


-(void)noticeLoginSuccess{
    NoticePost(kNotice_userloginsuccess, nil);
}

@end
