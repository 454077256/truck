//
//  AppTools.m
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "AppTools.h"
#import "Reachability.h"

@implementation AppTools


+(void)mergerArray:(NSMutableArray *)_oldArray newArray:(NSArray *)_newArray toEnd:(BOOL)_toEnd{
    NSMutableArray *toAddArray = [NSMutableArray array];
    
    NSMutableArray *mNewArray = [NSMutableArray arrayWithArray:_newArray];
    
    for(int i=0;i<[mNewArray count];i++){
        NSDictionary *oneNew = [mNewArray objectAtIndex:i];
        BOOL isExsit = NO;
        for(int j=0;j<[_oldArray count];j++){
            NSDictionary *oneOld = [_oldArray objectAtIndex:i];
            if ([[oneOld objectForKey:@"id"] integerValue] == [[oneNew objectForKey:@"id"] integerValue]) {
                isExsit = YES;
                continue;
            }
        }
        if (!isExsit) {
            [toAddArray addObject:oneNew];
        }
    }
    if (_toEnd) {
//        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:[_oldArray count]];
//        [toAddArray insertObjects:_oldArray atIndexes:indexSet];
        [_oldArray addObjectsFromArray:toAddArray];
    }else{
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
        [_oldArray insertObjects:toAddArray atIndexes:indexSet];
    }
}



@end
