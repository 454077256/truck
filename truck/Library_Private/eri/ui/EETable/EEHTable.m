//
//  EEHTable.m
//  Lohas
//
//  Created by 洪湃 on 16/5/27.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "HCell.h"
#import "EEHTable.h"


@implementation EEHTable


+(id)instanceByFrame:(CGRect)frame list:(NSArray *)dataList cellCalss:(Class)_cellClass defalultPage:(int)dPage{
    EEHTable *table = [[EEHTable alloc] initWithFrame:frame];
    table.dataArrays = dataList;
    table.defalultPage = dPage;
    
    table.tableCellClass = _cellClass;
    table.pageArrays = [NSMutableArray array];
    [table initView];
    
    return table;
}

-(void)initView{
    if(self.tableCellClass==nil){
        self.tableCellClass = [HCell class];
    }
    
    CGSize selfSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    
    int pagewidth = self.width;
    
    targetScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, selfSize.width, selfSize.height)];
    targetScrollView.pagingEnabled = YES;
    [self addSubview:targetScrollView];
    targetScrollView.delegate = self;
    
    for (int i=0; i<[self.dataArrays count]; i++) {
        HCell *contentPage = [[self.tableCellClass alloc] initWithFrame:R(i*pagewidth, 0, pagewidth, self.height)];
        [contentPage loadData:self.dataArrays[i] ];
        [contentPage setBackgroundColor:CBlack];
        
        [targetScrollView addSubview:contentPage];
        [self.pageArrays addObject:contentPage];
    }
    
    [targetScrollView setContentSize:CGSizeMake([self.dataArrays count]*pagewidth, self.height)];
    
    [self gotopage:self.defalultPage];
}

-(void)gotopage:(int)pageIndex{
    int pagewidth = self.width;
    [targetScrollView setContentOffset:ccp(pageIndex*pagewidth, 0)];
    
    [self scrollViewDidScroll:targetScrollView];
}

#pragma mark -- scroll delegate 


#pragma mark -- scroll delegate --
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //控制搜索框 影藏与显示
    [self.pageArrays enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[HCell class]]) {
            HCell *cell = (HCell *)obj;
            float offset = scrollView.contentOffset.x;
            if (cell.frame.origin.x>=(offset-self.width) && cell.frame.origin.x<=(offset+self.width)) {
                [cell load];
            }else{
                [cell unload];
            }
        }
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    track();
    
}


@end
