//
//  BaseClass.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"

@implementation BaseClass

@end


@implementation EEView

-(void)initView{

}

-(void)initView:(CGRect)frame{
}
+(id)instance{
    Class class = [self class];
    id ins = [[class alloc] init];
    [ins initView];
    return ins;
}

+(id)instanceByFrame:(CGRect)frame{
    Class class = [self class];
    id ins = [[class alloc] initWithFrame:frame];
    [ins initView];
    return ins;
}

+(id)instanceByFrame:(CGRect)frame data:(NSDictionary *)configData{
    Class class = [self class];
    id ins = [[class alloc] initWithFrame:frame];
    ((EEView *)ins).eeData = configData;
    [ins initView];
    return ins;
}
+(id)instanceByData:(id)configData{
    Class class = [self class];
    id ins = [[class alloc] init];
    return ins;
}

-(void)eeRemove{
    [self removeFromSuperview];
}

-(void)eeRemoveLeft{
    isRemoveAfterAction = YES;
    [self addTransition:ActionType_fromRight];
    [self ee_viewToPosition:CGPointMake(App_Size.width, 0)];
}
-(void)eeRemoveRight{
    isRemoveAfterAction = YES;
    [self addTransition:ActionType_fromLeft];
    [self ee_viewToPosition:CGPointMake(App_Size.width, 0)];
}
-(void)eeRemoveTop{
    isRemoveAfterAction = YES;
    [self addTransition:ActionType_fromDown];
    [self ee_viewToPosition:CGPointMake(App_Size.width, 0)];
}

-(void)eeRemoveDown{
    isRemoveAfterAction = YES;
    [self addTransition:ActionType_fromTop];
    [self ee_viewToPosition:CGPointMake(App_Size.width, 0)];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    track();
    if (isRemoveAfterAction) {
        isRemoveAfterAction = NO;
        [self eeRemove];
    }
}
-(void)addAnimation:(int)type{
    CABasicAnimation *animation ;
    if (type==0) {//透明
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.fromValue = [NSNumber numberWithFloat:0];
        animation.toValue = [NSNumber numberWithFloat:1];
    }else if(type==1){
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.fromValue = [NSNumber numberWithFloat:1];
        animation.toValue = [NSNumber numberWithFloat:0];
    }
    
    animation.repeatCount = 1;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.layer addAnimation:animation forKey:nil];
    
}

- (void) addTransition:(ActionType)type;

{
    CATransition *transition = [CATransition animation];
	// Animate over 3/4 of a second
	transition.duration = 0.35f;
	// using the ease in/out timing function
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	
    switch (type) {
        case ActionType_none:
            // Now to set the type of transition. Since we need to choose at random, we'll setup a couple of arrays to help us.
            
            transition.type = kCATransitionFade;
            
            break;
            
        case ActionType_fromRight:
            // Now to set the type of transition. Since we need to choose at random, we'll setup a couple of arrays to help us.
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            
            break;
        case ActionType_fromLeft:
            
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            
            break;
        case ActionType_fromTop:
            
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromTop;
            
            break;
        case ActionType_fromDown:
            
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromBottom;
            
            break;
        default:
            
            break;
    }
    
	// Finally, to avoid overlapping transitions we assign ourselves as the delegate for the animation and wait for the
	// -animationDidStop:finished: message. When it comes in, we will flag that we are no longer transitioning.
	transition.delegate = self;
	
	// Next add it to the containerView's layer. This will perform the transition based on how we change its contents.
	[self.layer addAnimation:transition forKey:nil];
}

-(void)addHud{
    hud = [Global createHud:CGSizeMake(30, 30) center:center(self) style:(UIActivityIndicatorViewStyleGray)];
    [self addSubview:hud];
}
-(void)removeHud{
    [hud removeFromSuperview];
    hud = nil;
}

- (void)dealloc
{
    [hud removeFromSuperview];
    hud = nil;
    self.eeData = nil;
    self.eeList = nil;
}

@end


@implementation EECell

- (instancetype)initWithData:(id)data
{
    self = [super init];
    if (self) {
        self.eeData = data;
        [self initView];
    }
    return self;
}
- (instancetype)initWithData:(id)data size:(CGSize)size{
    {
        self = [super init];
        if (self) {
            eecellSize = size;
            self.eeData = data;
            [self initView];
        }
        return self;
    }
}

-(void)initView{
    
}

-(void)startDataRequest{
    
}

-(void)loadData:(NSDictionary *)configData size:(CGSize)size{
    
}

-(void)loadData:(NSDictionary *)config size:(CGSize)size indexPath:(NSIndexPath *)indexpath{
    
}

-(void)dealloc{
    self.eeData = nil;
}

@end


@implementation EEButton

- (void)dealloc
{
    self.block_1 = nil;
}

@end


@implementation EELabel
@end


@implementation EEScrollView
@end


@implementation EEImageView
@end

@implementation EETextFiled

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , S(10) , 0 );
}

// 控制文本的位置，左右缩 20
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , S(10) , 0 );
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self resignFirstResponder];
    return YES;
}


@end

@implementation EETextView
@end

@implementation EEControl

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addTarget:self action:@selector(touchDown:) forControlEvents:(UIControlEventTouchDown)];
        [self addTarget:self action:@selector(touchCancel:) forControlEvents:(UIControlEventTouchUpInside)];
        [self addTarget:self action:@selector(touchCancel:) forControlEvents:(UIControlEventTouchCancel)];
    }
    return self;
}

-(void)touchDown:(id)sender{
    track();
    if (self.isShowDarkHighLight) {
        if(maskView==nil){
            maskView = [Global createViewByFrame:self.bounds color:[CBlack colorWithAlphaComponent:0.1]];
            maskView.userInteractionEnabled = NO;
        }
        [self addSubview:maskView];
    }
}

-(void)touchCancel:(id)sender{
    
    [maskView removeFromSuperview];
    maskView = nil;
}

- (void)dealloc
{
    track();
    self.block_1 = nil;
}

@end

