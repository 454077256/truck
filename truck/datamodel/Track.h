//
//  Track.h
//  Lohas
//
//  Created by 洪湃 on 15-4-25.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

+(void)postDaily:(NSString *)strContent;
+(void)postSeason:(NSString *)strContent;
+(void)postOpenMagazine:(NSString *)strContent;
+(void)postDownLoadMagazine:(NSString *)strContent;

+(void)postHaoKanVideo;
+(void)postRiZiDian;
+(void)postSlider:(NSString *)title;

//唤醒程序
+(void)postWeakupApp;
@end
