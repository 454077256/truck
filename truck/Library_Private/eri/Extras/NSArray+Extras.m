//
//  NSArray+Extras.m
//  epp
//
//  Created by 洪湃 on 14-9-26.
//  Copyright (c) 2014年 qq:454077256 phone:18621592830. All rights reserved.
//

#import "NSArray+Extras.h"

@implementation NSArray (external)

//是否包含某条string
-(BOOL)isContainerString:(NSString *)searchStr{
    __block BOOL isExist = NO;
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:searchStr]) {
            isExist = YES;
        }
    }];
    return isExist;
}

//某条string 的序号
-(int)getSerchstringIndex:(NSString *)searchStr{
    __block int index=-1;
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:searchStr]) {
            index = idx;
        }
    }];
    return index;
}



@end
