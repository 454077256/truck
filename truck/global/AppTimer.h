//
//  AppTimer.h
//  Lohas
//
//  Created by 洪湃 on 15-3-16.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import <Foundation/Foundation.h>

static int clockNum;

@interface AppTimer : NSObject{
    
    NSTimer *autoPlayerTimer;
}
-(void)createTimer;

+(void)setClockNum:(int)num;
+(int)getClockNum;
+(void)clearClockNum;

@end
