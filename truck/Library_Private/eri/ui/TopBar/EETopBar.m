//
//  EETopBar.m
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "EETopBar.h"

@implementation EETopBar

+(id)instanceBy_normalLeft:(NSString *)normalLeft selectLeft:(NSString *)selectLeft
                leftAction:(SEL)leftAction leftDelegate:(id)leftDelegate
               normalRight:(NSString *)normalRight selectRight:(NSString *)selectRight
               rightAction:(SEL)rightAction rightDelegate:(id)rightDelegate
                    center:(NSString *)centerName
              isIOS7Lagger:(BOOL)isios7lagger{
    return [EETopBar instanceBy_normalLeft:normalLeft selectLeft:selectLeft leftAction:leftAction leftDelegate:leftDelegate normalRight:normalRight selectRight:selectRight rightAction:rightAction rightDelegate:rightDelegate center:centerName isIOS7Lagger:isios7lagger width:App_Size.width];
    
}

+(id)instanceBy_normalLeft:(NSString *)normalLeft selectLeft:(NSString *)selectLeft
                leftAction:(SEL)leftAction leftDelegate:(id)leftDelegate
               normalRight:(NSString *)normalRight selectRight:(NSString *)selectRight
               rightAction:(SEL)rightAction rightDelegate:(id)rightDelegate
                    center:(NSString *)centerName
              isIOS7Lagger:(BOOL)isios7lagger width:(float)fwidth{
    
    float barheight = S(topbarheight);
    CGPoint leftPoint = ccp(S(24), barheight*0.5);
    CGPoint rightPoint = ccp(fwidth - S(24), barheight*0.5);
    if (isios7lagger) {;
        
        leftPoint = ccp(S(24), barheight*0.5+20);
        rightPoint = ccp(fwidth - S(24), barheight*0.5+20);
        barheight = S(topbarheight)+20;
    }
    
    
    EETopBar *topbar = [[EETopBar alloc] initWithFrame:R(0, 0, fwidth, barheight)];
    //26,129,150
    if (normalLeft!=nil && [Global isImageFileName:normalLeft]) {//说明为图片按钮
        topbar.leftButton = [Global createBtn_center:leftPoint name:normalLeft down:selectLeft target:leftDelegate action:leftAction];
    }else if(normalLeft!=nil){
        topbar.leftButton = [Global createBtn:R(0,0,S(70),44) normal:nil down:nil title:normalLeft col:RGB(22, 126, 251) colS:CGray size:S(15) target:leftDelegate action:leftAction];
        topbar.leftButton.center = leftPoint;
    }
    if (normalRight!=nil && [Global isImageFileName:normalRight]) {
        topbar.rightButton = [Global createBtn_center:rightPoint name:normalRight down:selectRight target:rightDelegate action:rightAction];
    }else if(normalRight!=nil){
        topbar.rightButton = [Global createBtn:R(0,0,S(70),44) normal:nil down:nil title:normalRight col:RGB(22, 126, 251) colS:CGray size:S(15) target:rightDelegate action:rightAction];
        topbar.rightButton.center = rightPoint;
    }
    
    
    //中间的是文字，还是图片
    if (centerName!=nil) {
        BOOL isImageFileName = [Global isImageFileName:centerName];
        if (isImageFileName) {//说明是图片
            [topbar addSubview:[Global createImage:centerName center:ccp(fwidth*0.5, leftPoint.y)]];
        }else{
            topbar.centerLabel = [Global createLabel:R(0, 0, fwidth, barheight) label:centerName lines:0 fontSize:S(18) fontName:nil textcolor:CWhite align:NSTextAlignmentCenter];
            [topbar addSubview:topbar.centerLabel];
            topbar.centerLabel.center = ccp(fwidth*0.5, leftPoint.y);
            [topbar.centerLabel setFont:[UIFont boldSystemFontOfSize:S(18)]];
        }
    }
    
    //2个按钮
    [topbar addSubview:topbar.leftButton];
    [topbar addSubview:topbar.rightButton];
//    if (IS_IPAD) {
//        topbar.leftButton.layer.anchorPoint = ccp(0, 0.5);
//        topbar.rightButton.layer.anchorPoint = ccp(1, 0.5);
//        
//        topbar.leftButton.center  = ccp(18, leftPoint.y);
//        topbar.rightButton.center = ccp(topbar.width - 18, rightPoint.y);
//        
//        [topbar.leftButton setBackgroundColor:CRed];
//        [topbar.rightButton setBackgroundColor:CRed];
//    }
    
    return topbar;
}




-(void)setDownLiner:(UIColor *)color{
    [self addSubview:[Global createViewByFrame:R(0, self.height-1, self.width, 0.5) color:color]];
}

-(void)hiddeLeftButonIfIpad{
    if (IS_IPAD) {
        [self.leftButton setHidden:YES];
    }
}

-(void)hiddeRightButonIfIpad{
    if (IS_IPAD) {
        [self.rightButton setHidden:YES];
    }
}

-(void)changedLeftBtnPoint:(CGPoint)point anch:(CGPoint)anc{
    self.leftButton.layer.anchorPoint = anc;
    self.leftButton.center = point;
}

- (void)dealloc
{
    track();
}

@end


////
////  EETopBar.m
////  Lohas
////
////  Created by 洪湃 on 15-1-13.
////  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
////
//
//#import "EETopBar.h"
//
//@implementation EETopBar
//
////44*2+20*2 = 128;
//
//+(id)instanceBy_normalLeft:(NSString *)normalLeft selectLeft:(NSString *)selectLeft
//                          leftAction:(SEL)leftAction leftDelegate:(id)leftDelegate
//                         normalRight:(NSString *)normalRight selectRight:(NSString *)selectRight
//                         rightAction:(SEL)rightAction rightDelegate:(id)rightDelegate
//                              center:(NSString *)centerName
//              isIOS7Lagger:(BOOL)isios7lagger{
//    CGPoint leftPoint = ccp(26, 22);
//    CGPoint rightPoint = ccp(App_Size.width - 26, 22);
//    float barheight = 44;
//    if (isios7lagger) {
//        leftPoint = ccp(26, 22+20);
//        rightPoint = ccp(App_Size.width - 26, 22+20);
//        barheight = 64;
//    }
//    EETopBar *topbar = [[EETopBar alloc] initWithFrame:R(0, 0, App_Size.width, barheight)];
//    //26,129,150
//    EEButton *leftButton;
//    if (normalLeft!=nil && [Global isImageFileName:normalLeft]) {//说明为图片按钮
//        leftButton = [Global createBtn_center:leftPoint name:normalLeft down:selectLeft target:leftDelegate action:leftAction];
//    }else if(normalLeft!=nil){
//        leftButton = [Global createBtn:R(0,0,70,44) normal:nil down:nil title:normalLeft col:RGB(22, 126, 251) colS:CGray size:18 target:leftDelegate action:leftAction];
//        leftButton.center = leftPoint;
//    }
//    if (normalRight!=nil && [Global isImageFileName:normalRight]) {
//        topbar.rightButton = [Global createBtn_center:rightPoint name:normalRight down:selectRight target:rightDelegate action:rightAction];
//    }else if(normalRight!=nil){
//        topbar.rightButton = [Global createBtn:R(0,0,70,44) normal:nil down:nil title:normalRight col:RGB(22, 126, 251) colS:CGray size:18 target:rightDelegate action:rightAction];
//        topbar.rightButton.center = rightPoint;
//    }
//    
//    
//    //中间的是文字，还是图片
//    if (centerName!=nil) {
//        BOOL isImageFileName = [Global isImageFileName:centerName];
//        if (isImageFileName) {//说明是图片
//            [topbar addSubview:[Global createImage:centerName center:ccp(App_Size.width*0.5, leftPoint.y)]];
//        }else{
//            topbar.centerLabel = [Global createLabel:R(0, topbar.height-44, App_Size.width, 44) label:centerName lines:0 fontSize:18 fontName:nil textcolor:CBlack align:NSTextAlignmentCenter];
//            [topbar addSubview:topbar.centerLabel];
//        }
//    }
//    
//    //2个按钮
//    [topbar addSubview:leftButton];
//    [topbar addSubview:topbar.rightButton];
//    
//    return topbar;
//}
//
//
//
//
//-(void)setDownLiner:(UIColor *)color{
//    [self addSubview:[Global createViewByFrame:R(0, self.height-1, self.width, 0.5) color:color]];
//}
//
//- (void)dealloc
//{
//    track();
//}
//
//@end
