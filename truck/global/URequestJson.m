//
//  URequest.m
//  Lohas
//
//  Created by 洪湃 on 15-3-24.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "URequestJson.h"
#import "SBJson.h"

@implementation URequestJson

-(void)fire{
    NSURL *url = [NSURL URLWithString:[self.configData objectForKey:@"url"]];
    
    //
    dataRequest = [ASIHTTPRequest requestWithURL:url];
    dataRequest.delegate = self;
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithDictionary:self.configData];
    [postData removeObjectForKey:@"url"];
    
    NSString *sss = [postData JSONRepresentation];
    NSData *data = [sss dataUsingEncoding:NSUTF8StringEncoding];
    [dataRequest setPostBody:[NSMutableData dataWithData:data]];
    [dataRequest setDidFinishSelector:@selector(reqeustResult:)];
    [dataRequest setDidFailSelector:@selector(reqeustFailed:)];
    
    [dataRequest addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [dataRequest addRequestHeader:@"Accept" value:@"application/json"];
    [dataRequest startAsynchronous];
}



-(void)reqeustResult:(ASIHTTPRequest *)request{
    if (request.responseStatusCode==200) {
        NSString *responseString = request.responseString;
        NSDictionary *responseDictionary = [responseString JSONValue];
        
        if (responseDictionary==nil) {//!!!数据异常了，服务器返回非json字符串
            NSLog(@"解析json错误");
            [self doFailed:@{@"code":@"0",@"message":@"数据结果异常json"}];
        }else {//数据正常
            if([responseDictionary[@"success"] boolValue]){
                [self doComplete:responseDictionary];
            }else{
                [self doFailed:responseDictionary];
            }
        }
    }else{
        [self doFailed:@{@"code":@"0",@"message":request.error.description}];
    }
    
    //
    //    BOOL isFailed = [[postData objectForKey:@"resultCode"] boolValue];
    //    if (isFailed) {
    //        //NSString *sName = [postData objectForKey:@"message"];
    //
    //        [self doFailed:postData];
    //    }else{
    //
    //        [self doComplete:postData];
    //    }
}

-(void)reqeustFailed:(ASIHTTPRequest *)request{
    [self doFailed:@{@"code":@"0",@"message":request.error.description}];
}


#pragma mark --- 回收 ---
-(void)disponse{
    dataRequest.delegate = nil;
    dataRequest = nil;
    [super disponse];
}


- (void)dealloc
{
    track();
}

@end
