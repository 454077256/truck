//
//  LocationCell.h
//  Lohas
//
//  Created by 洪湃 on 16/4/16.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationCell : UITableViewCell{
    EELabel *labelTitle;
    EELabel *labelDetail;
}

-(void)refreshTitle:(NSString *)_title detail:(NSString *)detail;

@end
