//
//  EETable.h
//  OuRuiDA
//
//  Created by 洪湃 on 15/12/8.
//  Copyright © 2015年 洪湃 454077256. All rights reserved.
//

#import "BaseClass.h"
#import "EGORefreshTableHeaderView.h"
#import "EGORefreshTableFooterView.h"

@interface EETable : EEView<UITableViewDataSource,UITableViewDelegate,EGORefreshTableDelegate>{
    CGRect varTableFrame;
    NSInteger varTableCellHeight;
    Class varCellCLASS;
    
    BOOL isNewFreshing;
    
    UITableView *dataTableView;
    
    NSInteger curPageNum;
    BOOL _reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
    EGORefreshTableFooterView *_refreshFooterView;
}

-(void)initVar;
-(void)initTable;

-(void)eeTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;


@end
