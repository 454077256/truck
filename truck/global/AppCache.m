//
//  AppCache.m
//  Lohas
//
//  Created by 洪湃 on 15-3-8.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import "AppCache.h"
#import "EGOImageView.h"

@implementation AppCache


//EE_LocalData_MagazineCoverList
//EE_LocalData_MagazineData
//EE_LocalData_MagazineDownloaded

//管理杂志列表

//管理杂志内容
#pragma mark -- 管理杂志内容 --
+(NSMutableDictionary *)EE_LocalData_MagazineData_{
    return [NSMutableDictionary dictionaryWithContentsOfFile:EE_LocalData_MagazineData];
}

+(NSMutableDictionary *)EE_LocalData_MagazineData_getOneMagContentByKey:(NSString *)key{
    NSMutableDictionary *dictionary = [AppCache EE_LocalData_MagazineData_];
    return [dictionary objectForKey:key];
}

+(void)EE_LocalData_MagazineData_update:(NSDictionary *)oneData key:(NSString *)key{
    NSMutableDictionary *dictionary = [AppCache EE_LocalData_MagazineData_];
    if (dictionary==nil) {
        dictionary = [NSMutableDictionary dictionary];
    }
    [dictionary setObject:oneData forKey:key];
    BOOL isSuccess = [dictionary writeToFile:EE_LocalData_MagazineData atomically:YES];
    if (isSuccess) {
        trace(@"更新 magazineData 成功");
    }
}

//管理杂志内容下载页面
#pragma mark -- 管理杂志内容 --

+(BOOL)EE_LocalData_MagazineDownloaded_updateByMagId:(NSString *)magId pageKey:(NSString *)key{
    //更新本地文件数据库，说明这个文件下载过
    NSMutableDictionary *configDownloadCache = [NSMutableDictionary dictionaryWithContentsOfFile:EE_LocalData_MagazineDownloaded];
    
    if (configDownloadCache==nil) {
        configDownloadCache = [NSMutableDictionary dictionary];
    }
    
    NSMutableDictionary *oneMagConfig = [configDownloadCache objectForKey:magId];
    if (oneMagConfig == nil) {
        oneMagConfig = [NSMutableDictionary dictionary];
        [configDownloadCache setObject:oneMagConfig forKey:magId];
    }
    
    //当前的页码
    [oneMagConfig setObject:YesNumber forKey:key];
    
    trace(@"%@",EE_LocalData_MagazineDownloaded);
    
    BOOL isSccuess = [configDownloadCache writeToFile:EE_LocalData_MagazineDownloaded atomically:YES];
    if (!isSccuess) {
        traceError(@"保存失败");
    }
    return isSccuess;
}

//内容保存
#pragma mark -- 杂志下载图片管理 --

+(NSString *)getHashName:(NSString *)imgWebPath{
    NSString *lohasMagKey = [NSString stringWithFormat:@"LohasMag-%lu", (unsigned long)[imgWebPath hash]];
    return lohasMagKey;
}

+(BOOL)isMagWebPathExistInLocalCache:(NSString *)imgWebPath{
    NSString *lohasMagKey = [AppCache getHashName:imgWebPath];
    NSString *filePath = [EE_CacheMagazineDictionary stringByAppendingPathComponent:lohasMagKey];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return YES;
    }
    return NO;
}

+(BOOL)saveMagazineImage:(NSData *)imgWebData imageURL:(NSString *)webPath{
    if (![[NSFileManager defaultManager] fileExistsAtPath:EE_CacheMagazineDictionary isDirectory:nil]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:EE_CacheMagazineDictionary withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *lohasMagKey = [AppCache getHashName:webPath];
    NSString *filePath = [EE_CacheMagazineDictionary stringByAppendingPathComponent:lohasMagKey];
    BOOL isWriteOk = [imgWebData writeToFile:filePath atomically:YES];
    if (!isWriteOk) {
        traceError(@"图片保存失败 \n %@",webPath);
    }
    return isWriteOk;
}


#pragma mark -- 文章本地缓存管理 --
//得到今日推荐文章列表
+(NSMutableArray *)getTodayList{
    return [NSMutableArray arrayWithContentsOfFile:EE_TodayPlist];
}

//得到节气令文章列表
+(NSMutableArray *)getSeasonList{
    return  [NSMutableArray arrayWithContentsOfFile:EE_SeasonPlist];
}



//得到一篇文章
+(NSDictionary *)getLocalWebArticleData:(NSString *)webPath{
    NSString *hashName = [AppCache getHashName:webPath];
    
    NSString *filePath = [EE_CacheHtmlDictionary stringByAppendingPathComponent:hashName];
    
    NSDictionary *htmlData = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    return htmlData;
}

//保存一篇文章
+(BOOL)saveWebArticleToLocal:(NSDictionary *)htmlData webPath:(NSString *)webPath{
    NSString *hashName = [AppCache getHashName:webPath];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:EE_CacheHtmlDictionary isDirectory:nil]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:EE_CacheHtmlDictionary withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *filePath = [EE_CacheHtmlDictionary stringByAppendingPathComponent:hashName];
    
    BOOL isSuccess = [htmlData writeToFile:filePath atomically:YES];
    
    if (isSuccess) {
        return YES;
    }
    return NO;
}


#pragma mark -- 开始画面处理 --

+(NSString *)getOpenImagePath{
    NSMutableDictionary *openCache = [NSMutableDictionary dictionaryWithContentsOfFile:EE_CacheOpenImagePlist];
    return [openCache objectForKey:@"content"];
}
    
+(UIImageView *)getOpenImageView{
    NSMutableDictionary *openCache = [NSMutableDictionary dictionaryWithContentsOfFile:EE_CacheOpenImagePlist];
    if (openCache==nil) {
        return nil;
    }
    
    NSData *imgData = [NSData dataWithContentsOfFile:EE_CacheOpenImageData];
    UIImage *img = [UIImage imageWithData:imgData];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    return imgView;
}


+(BOOL)saveNewOpenImage:(NSData *)imgData config:(NSDictionary *)openConfig{
    
    //保存plist
    [openConfig writeToFile:EE_CacheOpenImagePlist atomically:YES];
    
    //保存图片
    BOOL isSuccess = [imgData writeToFile:EE_CacheOpenImageData atomically:YES];
    if (!isSuccess) {
        traceError(@"文件保存失败 %@",openConfig);
    }
    return isSuccess;
}



#pragma mark -- 收藏管理 --

+(NSMutableArray *)getFavoriteList{
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:EE_FavoritePlist];
    if (array==nil) {
        array = [NSMutableArray array];
    }
    
    return array;
}


#pragma mark -- 更新本地缓存 --
+(void)updateFavoriteDataToLocal_newArray :(NSMutableArray *)newFavoriteList type:(NSString *)type{
    if (newFavoriteList==nil || [newFavoriteList count]<=0  || type==nil){
        return;
    }
    
    
    NSMutableArray *favoriteCacheList = [AppCache getFavoriteList];
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i=0; i<[favoriteCacheList count]; i++) {
        NSDictionary *oneConfig = [favoriteCacheList objectAtIndex:i];
        NSString *curType = [oneConfig objectForKey:@"type"];
        //        NSString *item_id = [oneConfig objectForKey:@"item_id"];
        //        NSString *page = [oneConfig objectForKey:@"page"];
        if ([type isEqualToString:curType]) {
            [tempArray addObject:oneConfig];
        }
    }
    
    trace(@"------ 删除 %lu 个数据 --------",(unsigned long)[tempArray count]);
    
    //删除此类型的
    [favoriteCacheList removeObjectsInArray:tempArray];
    
    
    trace(@"------ 添加 %lu 个数据 --------",(unsigned long)[newFavoriteList count]);
    
    //添加此类型的
    for(int i=0;i<[newFavoriteList count];i++){
        NSDictionary *configData = [newFavoriteList objectAtIndex:i];
        NSString *sid = [configData objectForKey:@"item_id"];
        NSString *page = [configData objectForKey:@"page"];
        
        NSMutableDictionary *newDiction = [NSMutableDictionary dictionary];
        [newDiction setObject:sid forKey:@"item_id"];
        [newDiction setObject:type forKey:@"type"];
        if (page) {
            [newDiction setObject:page forKey:@"page"];
        }
        [favoriteCacheList addObject:newDiction];
    }
    [AppCache saveFavoriteData:favoriteCacheList];
}

+(void)deleLocalFavoriteById:(NSString *)sid type:(NSString *)type page:(NSString *)page{
    NSMutableArray *favoriteArray = [AppCache getFavoriteList];
    for (int i=0; i<[favoriteArray count]; i++) {
        NSDictionary *oneDictionary = [favoriteArray objectAtIndex:i];
        if ([[oneDictionary objectForKey:@"item_id"] isEqualToString:sid]
            && [[oneDictionary objectForKey:@"type"] isEqualToString:type]) {
            if (page!=nil) {
                if ([[oneDictionary objectForKey:@"page"] isEqualToString:page]) {
                    [favoriteArray removeObjectAtIndex:i];
                    [AppCache saveFavoriteData:favoriteArray];
                    trace(@"-----本地缓存发现一条记录，删除 ------ ");
                    break;
                }
            }else{
                [favoriteArray removeObjectAtIndex:i];
                [AppCache saveFavoriteData:favoriteArray];
                trace(@"-----本地缓存发现一条记录，删除 ------ ");
            }
        }
    }
}




+(BOOL)isArticleInLocalFavorite:(NSInteger)artId{
    NSMutableArray *fArray = [AppCache getFavoriteList];
    
    for (int i=0; i<[fArray count]; i++) {
        NSDictionary *oneDictionary = [fArray objectAtIndex:i];
        NSInteger aid = [[oneDictionary objectForKey:@"item_id"] integerValue];
        NSString *type = [oneDictionary objectForKey:@"type"];
        
        if(aid==artId && [type isEqualToString:[Global sharedGlobal].pageType]){
            return YES;
        }
    }
    
    return NO;
}
+(BOOL)isMagazineInLocalFavorite:(NSInteger)magId pageKey:(NSInteger)pagekey{
    NSMutableArray *fArray = [AppCache getFavoriteList];
    
    for (int i=0; i<[fArray count]; i++) {
        NSDictionary *oneDictionary = [fArray objectAtIndex:i];
        NSInteger mid = [[oneDictionary objectForKey:@"item_id"] integerValue];
        NSString *type = [oneDictionary objectForKey:@"type"];
        NSInteger pkey = [[oneDictionary objectForKey:@"page"] integerValue];
        
        if(magId==mid && pagekey == pkey  && [type isEqualToString:[Global sharedGlobal].pageType]){
            return YES;
        }
    }
    return NO;
}


+(void)addArticleInLocalFavorite:(NSString *)articleId title:(NSString *)title{
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:EE_FavoritePlist];
    if (array==nil) {
        array = [NSMutableArray array];
    }
    
    NSDictionary *newArticleConfig = @{@"type":[Global sharedGlobal].pageType,@"item_id":articleId,@"title":title};
    
    [array addObject:newArticleConfig];
    
    BOOL issuccess = [array writeToFile:EE_FavoritePlist atomically:YES];
    if (!issuccess) {
        traceError(@"保存失败 %@",array);
    }
}

+(void)addMagazineInLocalFavorite:(NSString *)magId pageid:(NSString *)pid title:(NSString *)title{
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:EE_FavoritePlist];
    if (array==nil) {
        array = [NSMutableArray array];
    }
    NSString *catalogTitle = title;
    if (catalogTitle==nil) {
        catalogTitle = @"";
    }
    
    NSDictionary *newArticleConfig = @{@"type":[Global sharedGlobal].pageType,@"item_id":magId,@"page":pid,@"title":title};
    
    [array addObject:newArticleConfig];
    
    BOOL issuccess = [array writeToFile:EE_FavoritePlist atomically:YES];
    if (!issuccess) {
        traceError(@"保存失败 %@",array);
    }
}

+(void)saveFavoriteData:(NSArray *)configList{
    BOOL issuccess = [configList writeToFile:EE_FavoritePlist atomically:YES];
    if (!issuccess) {
        
    }
}






@end
