//
//  AppDelegate.m
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃. All rights reserved.
//
//


/*
                _ooOoo_
               o8888888o
               88" . "88
               (| -_- |)
               O\  =  /O
            ____/`---'\____
          .'  \\|     |//  `.
         /  \\|||  :  |||//  \
         /  _||||| -:- |||||-  \
         |   | \\\  -  /// |   |
         | \_|  ''\---/''  |   |
         \  .-\__  `-`  ___/-. /
        ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
    | | :  `- \`.;`\ _ /`;.`/ - ` : | |
    \  \ `-.   \_ __\ /__ _/   .-` /  /
 ======`-.____`-.___\_____/___.-`____.-'======
 `=---='
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 佛祖保佑       永无BUG      洪某虔俯
 */
#import "AppDelegate.h"
#import "UnitCpu.h"

//#import <ShareSDK/ShareSDK.h>
//#import "WXApi.h"
//#import "WeiboSDK.h"
//#import <TencentOpenAPI/QQApi.h>
//#import <TencentOpenAPI/QQApiInterface.h>
//
//#import <TencentOpenAPI/TencentOAuth.h>
//
//#import <SinaWeiboConnection/SinaWeiboConnection.h>
//
////以下是腾讯QQ和QQ空间
//#import <TencentOpenAPI/QQApi.h>
//#import <TencentOpenAPI/QQApiInterface.h>
//#import <TencentOpenAPI/TencentOAuth.h>
//
////开启QQ和Facebook网页授权需要
//#import <QZoneConnection/ISSQZoneApp.h>

#import "RootControllerPhone.h"
#import "RootControllerIpad.h"
//
//#import "InAppPurchaseManager.h"
//#import "BaiduMobStat.h"
//
//#import "APService.h"
//
//

#import "UMMobClick/MobClick.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self.window makeKeyAndVisible];
    
    [Global sharedGlobal];
    
    //https://i.umeng.com/  454077256@qq.com  h*****7
    //path finder locat to position， cmd  sudo ./umcrashtool  error.csv
    UMConfigInstance.appKey = @"579f174ee0f55ab975000dd6";
    UMConfigInstance.channelId = @"App Store";
    //    UMConfigInstance.eSType = E_UM_GAME; //仅适用于游戏场景，应用统计不用设置
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    [MobClick setCrashReportEnabled:YES];
    
    [self appstart];

    return YES;
}

//appdelegate.m加
//-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    return UIInterfaceOrientationMaskLandscape;
////    if (self.allowRotation) {
////        return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskLandscapeLeft|UIInterfaceOrientationMaskLandscapeRight;
////    }
////    return UIInterfaceOrientationMaskPortrait;
//}


-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    return UIInterfaceOrientationMaskPortrait;
}



-(void)appstart{
//    if(IS_IPAD){
//        RootControllerIpad *rootcontroller = [RootControllerIpad sharedRootViewController];
//        self.window.rootViewController = rootcontroller;
//    }else{
//        //这里创建curContainer 是因为以后很多的uiview的宽高，是根据这个view来获取到的
//        [Global sharedGlobal].curContainer = [[EEView alloc] initWithFrame:R(0, 0, App_Size.width, App_Size.height)];
//        
        RootControllerPhone *rootcontroller = [RootControllerPhone sharedRootViewController];
        self.window.rootViewController = rootcontroller;
//    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"recordHistoryIndexPageIntoLocal" object:nil];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
