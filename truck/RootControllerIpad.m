//
//  RootControllerIpad.m
//  Lohas
//
//  Created by 洪湃 on 15-4-12.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//


#import "RootControllerIpad.h"


static RootControllerIpad *_rootViewController = NULL;

@interface RootControllerIpad ()

@end

@implementation RootControllerIpad


+ (RootControllerIpad*) sharedRootViewController
{
    if (!_rootViewController) {
        _rootViewController = [[RootControllerIpad alloc] init];
        [Global sharedGlobal].rootController = _rootViewController;
    }
    return _rootViewController;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [Global statusBarShow:YES];
    
}



@end
