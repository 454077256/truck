//
//  AppDelegate.h
//  Lohas
//
//  Created by 洪湃 on 15-1-13.
//  Copyright (c) 2015年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
}


@property(nonatomic)BOOL allowRotation;

@property (strong, nonatomic) UIWindow *window;

@end

