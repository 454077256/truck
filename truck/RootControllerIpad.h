//
//  RootControllerIpad.h
//  Lohas
//
//  Created by 洪湃 on 15-4-12.
//  Copyright (c) 2015年 洪湃 454077256. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootControllerIpad : UIViewController{
    
    UIImageView *openImageView;
    EEButton *closeOpenImgBtn;
}


+ (RootControllerIpad*) sharedRootViewController;

@end
