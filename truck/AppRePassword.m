//
//  AppRePassword.m
//  truck
//
//  Created by 洪湃 on 16/11/26.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "AppRePassword.h"
#import "URequest.h"
#import "SVProgressHUD.h"

@implementation AppRePassword


-(void)initView{
    [self setBackgroundColor:CWhite];
    //导航条
    EETopBar *topbar = [EETopBar instanceBy_normalLeft:@"assets/return.png"
                                            selectLeft:nil
                                            leftAction:@selector(clickReturn:)
                                          leftDelegate:self
                                           normalRight:nil
                                           selectRight:nil
                                           rightAction:nil
                                         rightDelegate:self
                                                center:@"修改密码" isIOS7Lagger:YES];
    [topbar setBackgroundColor:CHex(0x1BA2E7)];
    [self addSubview:topbar];
    
    
    [self addSubview:tmpView = [Global createViewByFrame:R(SS(12), SS(132), SS(616), SS(78)) color:CWhite]];
    [Global addBorder:tmpView borderWidth:1 color:CHex(0xA9A9A9) radius:S(6)];
    
    [self addSubview:tmpView = [Global createViewByFrame:R(SS(12), SS(222), SS(616), SS(78)) color:CWhite]];
    [Global addBorder:tmpView borderWidth:1 color:CHex(0xA9A9A9) radius:S(6)];
    
    [self addSubview:tmpView = [Global createViewByFrame:R(SS(12), SS(312), SS(616), SS(78)) color:CWhite]];
    [Global addBorder:tmpView borderWidth:1 color:CHex(0xA9A9A9) radius:S(6)];
    
    [self addSubview:[Global createImage:@"assets/icolocak2.png" center:ccp(SS(64), SS(170))]];
    [self addSubview:[Global createImage:@"assets/icoys.png" center:ccp(SS(64), SS(262))]];
    [self addSubview:[Global createImage:@"assets/icoys.png" center:ccp(SS(64), SS(350))]];
    
    tf_oldPass = [Global createTextFiled:R(SS(90), SS(132), SS(500), SS(78)) size:S(14) color:CBlack];
    [self addSubview:tf_oldPass];
    tf_oldPass.placeholder = @"请输入当前登录密码";
    
    tf_newspass1 = [Global createTextFiled:R(SS(90), SS(224), SS(500), SS(78)) size:S(14) color:CBlack];
    [self addSubview:tf_newspass1];
    tf_newspass1.placeholder = @"请输入新密码";
    
    tf_newspass2 = [Global createTextFiled:R(SS(90), SS(312), SS(500), SS(78)) size:S(14) color:CBlack];
    [self addSubview:tf_newspass2];
    tf_newspass2.placeholder = @"请再次输入新密码";
    
    [self addSubview:tmpButtom = [Global createBtnColor:R(SS(12), SS(414), SS(614), SS(68)) bgcolor:CHex(0xFC9827) title:@"确 认" titleNormal:CWhite titleSelect:CGray size:S(16) delegate:self action:@selector(clickRePassword:)]];
    [Global addBorder:tmpButtom borderWidth:1 color:CHex(0xFC9827)  radius:S(4)];
}

-(void)clickRePassword:(id)sender{
    track();
    NSString *urlstring = WebURL(@"modifyPassword");

    NSString *oldPass = tf_oldPass.text;
    NSString *newspass1 = tf_newspass1.text;
    NSString *newspass2 = tf_newspass2.text;
    
    if(![[newspass1 lowercaseString] isEqualToString:([newspass2 lowercaseString])]){
        [SVProgressHUD showErrorWithStatus:@"2次输入的新密码不一样" duration:1];
        return;
    }
    
    NSDictionary *postDictionary = @{@"url":urlstring,@"password":oldPass,@"newPassword":newspass2};

    
    [[[URequest alloc] init] start:postDictionary begin:^{
        [SVProgressHUD show];
    } complete:^(id postResult) {
        trace(@"%@",postResult);
        [SVProgressHUD dismissWithSuccess:@"修改成功" afterDelay:1];
        [self eeRemoveRight];
    } failed:^(id postResult) {
        [SVProgressHUD dismissWithError:@"修改失败" afterDelay:1];
    }];
}

-(void)clickReturn:(id)sender{
    track();
    [self eeRemoveRight];
}

-(void)dealloc{
    track();
}

@end
