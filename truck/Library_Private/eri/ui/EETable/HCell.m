//
//  HCell.m
//  Lohas
//
//  Created by 洪湃 on 16/5/27.
//  Copyright © 2016年 洪湃 454077256. All rights reserved.
//

#import "HCell.h"

@implementation HCell


-(void)loadData:(NSDictionary *)data{
    self.eeData = data;
}

-(void)createUI{
    NSString *imagePath;
    if ([self.eeData isKindOfClass:[NSString class]]) {
        imagePath = (NSString *)self.eeData;
    }
    
    [self addSubview:[Global createHud:CGSizeMake(30, 30) center:ccp(self.width*0.5, self.height*0.5) style:(UIActivityIndicatorViewStyleWhite)]];

    contentScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    [self addSubview:contentScrollView];
    [contentScrollView setBackgroundColor:CClear];
    contentScrollView.delegate = self;
    [contentScrollView setMinimumZoomScale:1.0];
    [contentScrollView setMaximumZoomScale:2.0];
    
    contentImage = [Global createImageEGO:imagePath frame:self.bounds];
    [contentImage setBackgroundColor:CClear];
    [contentScrollView addSubview:contentImage];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return contentImage ;
}
-(void)load{
//    track();
    if (isLoadedUI) {
        return;
    }
    isLoadedUI = YES;
    
    [self createUI];
}

-(void)unload{
//    track();
    if (!isLoadedUI) {
        return;
    }
    isLoadedUI = NO;
    
    [self ee_removeAllChildren];
}



@end
